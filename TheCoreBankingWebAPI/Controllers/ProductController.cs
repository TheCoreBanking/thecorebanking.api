﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NLog;
using TheCoreBankingWebAPI.Data;
using TheCoreBankingWebAPI.Data.Models;
using TheCoreBankingWebAPI.Data.ViewModels;

namespace TheCoreBankingWebAPI.Controllers
{
    [Route("api/v2/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {

        private readonly IUnitOfWork UnitOfWork;
        private ILogger<ApprovalController> logger;

        public ProductController(IUnitOfWork _unitOfWork, ILogger<ApprovalController> _logger) { this.UnitOfWork = _unitOfWork; logger = _logger; }
        //1
        [HttpGet]
        [Route("LoadCustomers")]
        public async Task<ActionResult<IEnumerable<TblCustomer>>> GetCustomers()
        {

            try
            {

                List<TblCustomer> customer = await UnitOfWork.Customer.GetAllCustomers();

                if (customer.Count() < 1) { return NotFound(new { message = "No Customer Found" }); }
                else
                {

                    return Ok(new { Count = customer.Count(), Data = customer }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

        //2
        [HttpGet]
        [Route("ListProduct")]
        public async Task<ActionResult<IEnumerable<TblProduct>>> GetProduct()
        {

            try
            {

                List<TblProduct> products = await UnitOfWork.Product.LoadProduct();

                if (products.Count() < 1) { return NotFound(new { message = "No Product Found" }); }
                else
                {

                    return Ok(new { Count = products.Count(), Data = products }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }



        //3
        [HttpPost]
        [Route("CreateProduct")]
        public async Task<ActionResult<string>> PostProduct([FromBody] AddProductVm model)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {

                bool Result = await UnitOfWork.Product.CreateProduct(model);

                if (Result == false) { return NotFound(new { message = "Fail to Add New Product" }); }
                else
                {

                    return Ok(new{ Message = "Product Successfully Added" });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

        //4

        [HttpPost]
        [Route("CreateProductGroup")]
        public async Task<ActionResult<string>> PostProductGroup([FromBody] AddProductGroupVm model)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {

                bool Result = await UnitOfWork.Product.CreateProductGroup(model);

                if (Result == false) { return NotFound(new { message = "Fail to Add New Product Gropup" }); }
                else
                {

                    return Ok(new { Message = "Product Group Successfully Added" });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //5

        [HttpPost]
        [Route("CreateProductCatergory")]
        public async Task<ActionResult<string>> PostProductCatergory([FromBody] AddProductCatergoryVm model)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {

                bool Result = await UnitOfWork.Product.CreateProductCatergory(model);

                if (Result == false) { return NotFound(new { message = "Fail to Add New Product Catergory" }); }
                else
                {

                    return Ok(new { Message = "Product Catergory Successfully Added" });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

        //6
        [HttpPut]
        [Route("UpdateProductGroup")]
        public async Task<ActionResult<string>> PutProductGroup([FromBody] UpdateProductGroupVm model, int id)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {

                bool Result = await UnitOfWork.Product.UpdateProductGroup(model , id);

                if (Result == false) { return NotFound(new { message = "Fail to Update Product Gropup" }); }
                else
                {

                    return Ok( new { Message = "Product Group Successfully Updated" });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //7

        [HttpPut]
        [Route("UpdateProductCatergory")]
        public async Task<ActionResult<string>> PutProductCatergory([FromBody] UpdateProductCatergoryVm model, int id)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {

                bool Result = await UnitOfWork.Product.UpdateProductCatergory(model, id);

                if (Result == false) { return NotFound(new { message = "Fail to Update Product Catergory" }); }
                else
                {

                    return Ok( new { Mesahe = "Product Catergory Successfully Added" });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


        //2
        [HttpGet]
        [Route("ListCasaProduct")]
        public async Task<ActionResult<string>> GetCasaProduct()
        {

            try
            {

                List<CasaProductVm> products = await UnitOfWork.Product.ListCasaProduct();

                if (products.Count() < 1) { return NotFound(new { message = "No Product Found" }); }
                else
                {

                    return Ok(new { Count = products.Count(), Data = products }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


        //2
        [HttpGet]
        [Route("GetFeeNameByProductId/{productId}")]
        public async Task<ActionResult<string>> GetFeeNamebyProductId(string productId)
        {

            try
            {

                List<FeeVm> products = await UnitOfWork.Product.GetFeeNameByProductId(productId);

                if (products.Count() < 1) { return NotFound(new { message = "No Product Found" }); }
                else
                {

                    return Ok(new { Count = products.Count(), Data = products }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


        //2
        [HttpGet]
        [Route("GetFeeNameByProductName/{productName}")]
        public async Task<ActionResult<string>> GetFeeName(string productName)
        {

            try
            {

                List<FeeVm> products = await UnitOfWork.Product.GetFeeNameByProductName(productName);

                if (products.Count() < 1) { return NotFound(new { message = "No Product Found" }); }
                else
                {

                    return Ok(new { Count = products.Count(), Data = products }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

    }




}
