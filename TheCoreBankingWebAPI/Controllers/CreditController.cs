﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NLog;
using TheCoreBankingWebAPI.Data;
using TheCoreBankingWebAPI.Data.Models;
using TheCoreBankingWebAPI.Data.ViewModels;

namespace TheCoreBankingWebAPI.Controllers
{
    [Route("api/v2/[controller]")]
    [ApiController]
    public class CreditController : ControllerBase
    {
        private readonly IUnitOfWork UnitOfWork;
        private ILogger<CreditController> logger;

        public CreditController(IUnitOfWork _unitOfWork, ILogger<CreditController> _logger) { this.UnitOfWork = _unitOfWork; logger = _logger; }
        //1
        [HttpGet]
        [Route("LoadCustomers")]
        public async Task<ActionResult<IEnumerable<TblCustomer>>> GetCustomers()
        {

            try
            {

                List<TblCustomer> customer = await UnitOfWork.Credit.GetAllCustomers();

                if (customer.Count() < 1) { return NotFound(new { message = "No Customer Found" }); }
                else
                {

                    return Ok(new { Count = customer.Count(), Data = customer }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


        //2
        [HttpGet]
        [Route("ListOperation")]
        public async Task<ActionResult<string>> Get()
        {

            try
            {

                List<TblBankingOperationSetup> customer = await UnitOfWork.Credit.ListOperation();

                if (customer.Count() < 1) { return NotFound(new { message = "No Customer Found" }); }
                else
                {

                    return Ok(new { Count = customer.Count(), Data = customer }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

        //3
        [HttpGet]
        [Route("LoadLoanProductCatergory")]
        public async Task<ActionResult<IEnumerable<TblProductCategory>>> GetProductCatergory()
        {

            try
            {

                List<TblProductCategory> customer = await UnitOfWork.Credit.LoanProductCatergory();

                if (customer.Count() < 1) { return NotFound(new { message = "No Customer Found" }); }
                else
                {

                    return Ok(new { Count = customer.Count(), Data = customer }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

        //4
        [HttpGet]
        [Route("LoadLoanProductLoanType")]
        public async Task<ActionResult<IEnumerable<TblBankingProductType>>> GetLoanproductLoanType()
        {

            try
            {

                List<TblBankingProductType> customer = await UnitOfWork.Credit.LoanProductType();

                if (customer.Count() < 1) { return NotFound(new { message = "No Customer Found" }); }
                else
                {

                    return Ok(new { Count = customer.Count(), Data = customer }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

        //5
        [HttpGet]
        [Route("LoadCreditProductCatergory")]
        public async Task<ActionResult<IEnumerable<TblProduct>>> GetCreditProduct()
        {

            try
            {

                List<TblProduct> customer = await UnitOfWork.Credit.CreditProduct();

                if (customer.Count() < 1) { return NotFound(new { message = "No Customer Found" }); }
                else
                {

                    return Ok(new { Count = customer.Count(), Data = customer }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

        //6
        [HttpGet]
        [Route("ApprovedLoanApplication")]
        public async Task<ActionResult<IEnumerable<TblBankingLoanApplication>>> GetApprovedLoanApplication()
        {

            try
            {

                List<TblBankingLoanApplication> customer = await UnitOfWork.Credit.ApprovedLoanApplication();

                if (customer.Count() < 1) { return NotFound(new { message = "No Customer Found" }); }
                else
                {

                    return Ok(new { Count = customer.Count(), Data = customer }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


        //7
        [HttpGet]
        [Route("LoadProductSecurity")]
        public async Task<ActionResult<IEnumerable<TblBankingProductSecurity>>> GetProductSecurity()
        {

            try
            {

                List<TblBankingProductSecurity> customer = await UnitOfWork.Credit.ProductSecurity();

                if (customer.Count() < 1) { return NotFound(new { message = "No Customer Found" }); }
                else
                {

                    return Ok(new { Count = customer.Count(), Data = customer }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


        //8
        [HttpPost]
        [Route("GenerateLoanAppSchedule")]

        public async Task<ActionResult<string>> PostLoanAppSchedule([FromBody] LoanApplicationVM model )
        {
            if (ModelState.IsValid)
            {

                try
                {

                    int Result = await UnitOfWork.Credit.GenerateLoanAppSchedule(model);

                    if (Result < 1) { return NotFound(new { message = "Loan Schedule Not Generated" }); }
                    else
                    {

                        return Ok(new { Count = 1, Message = "Loan Application Schedule Successfully Created" }); ;
                    }
                }
                catch (Exception ex)
                {
                    //log Exception
                    Logger logger = LogManager.GetLogger("databaseLogger");

                    // add custom message and pass in the exception
                    logger.Error(ex, "Whoops!");
                    return StatusCode(500);
                }
            }

            return BadRequest(new { Message ="Model Error"});
        }

        //9
        [HttpGet]
        [Route("GetProductAccountNumber/{ProductCode}/{CustomerId}")]
        public async Task<ActionResult<string>> GetProductAccountNumber( string ProductCode, int CustomerId)
        {

            try
            {

                TblCasa AccountNumber = await UnitOfWork.Credit.ProductAccountNumber(ProductCode, CustomerId);

                if (AccountNumber == null) { return NotFound(new { message = "No Account  Found" }); }
                else
                {

                    return Ok(new { Count = 1, Data = AccountNumber.Accountnumber}); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

        ////
        ///

        //10
        [HttpPost]
        [Route("AddLoanApplication")]

        public async Task<ActionResult<string>> PostLoanApplication([FromBody] TblBankingLoanApplication model)
        {
            if (ModelState.IsValid)
            {

                if (string.IsNullOrEmpty((model.AgreeLoan).ToString()))
                { return BadRequest(new { Error = true, Message = " Agree Loan Amount is Required" }); }
                if (string.IsNullOrEmpty((model.AmountPaid).ToString()))
                { return BadRequest(new { Error = true, Message = " Agree Loan Amount is Required" }); }
                if (string.IsNullOrEmpty((model.ApplicationDate).ToString()))
                { return BadRequest(new { Error = true, Message = " Application Date is Required" }); }
                if (string.IsNullOrEmpty((model.ApprovalComment).ToString()))
                { return BadRequest(new { Error = true, Message = " Approval Comment is Required" }); }
                if (string.IsNullOrEmpty((model.ApprovedAmount).ToString()))
                { return BadRequest(new { Error = true, Message = " Approved Amount is Required" }); }
                if (string.IsNullOrEmpty((model.ApprovedBy).ToString()))
                { return BadRequest(new { Error = true, Message = " Approved By is Required" }); }
                if (string.IsNullOrEmpty((model.Booked).ToString()))
                { return BadRequest(new { Error = true, Message = " Booked is Required" }); }
                if (string.IsNullOrEmpty((model.BranchId).ToString()))
                { return BadRequest(new { Error = true, Message = "Branch Id is Required" }); }
                if (string.IsNullOrEmpty((model.Cam).ToString()))
                { return BadRequest(new { Error = true, Message = " Cam is Required" }); }
                if (string.IsNullOrEmpty((model.CanDisburse).ToString()))
                { return BadRequest(new { Error = true, Message = " Can Disburse is Required" }); }
                if (string.IsNullOrEmpty((model.CategoryId).ToString()))
                { return BadRequest(new { Error = true, Message = " Catergory Id is Required" }); }
                if (string.IsNullOrEmpty((model.CoyCode).ToString()))
                { return BadRequest(new { Error = true, Message = " Company Code is Required" }); }
                if (string.IsNullOrEmpty((model.Currency).ToString()))
                { return BadRequest(new { Error = true, Message = " Currency is Required" }); }
                if (string.IsNullOrEmpty((model.CurrentAcct).ToString()))
                { return BadRequest(new { Error = true, Message = " Current Account is Required" }); }
                if (string.IsNullOrEmpty((model.CurrRate).ToString()))
                { return BadRequest(new { Error = true, Message = " Curr Rate is Required" }); }
                if (string.IsNullOrEmpty((model.CustCode).ToString()))
                { return BadRequest(new { Error = true, Message = " Customer Code is Required" }); }
                if (string.IsNullOrEmpty((model.CustName).ToString()))
                { return BadRequest(new { Error = true, Message = " Customer Name is Required" }); }
                if (string.IsNullOrEmpty((model.DateCreated).ToString()))
                { return BadRequest(new { Error = true, Message = " Date Created is Required" }); }
                if (string.IsNullOrEmpty((model.Email).ToString()))
                { return BadRequest(new { Error = true, Message = " Email is Required" }); }
                if (string.IsNullOrEmpty((model.FeeCharge).ToString()))
                { return BadRequest(new { Error = true, Message = "Fee Charge is Required" }); }
                if (string.IsNullOrEmpty((model.FeeFrequency).ToString()))
                { return BadRequest(new { Error = true, Message = " Fee Frequency is Required" }); }
                if (string.IsNullOrEmpty((model.FreqType).ToString()))
                { return BadRequest(new { Error = true, Message = " Frequency Type is Required" }); }
                if (string.IsNullOrEmpty((model.Frequency).ToString()))
                { return BadRequest(new { Error = true, Message = " Frequency is Required" }); }
                if (string.IsNullOrEmpty((model.Installments).ToString()))
                { return BadRequest(new { Error = true, Message = " Insatallment is Required" }); }
                if (string.IsNullOrEmpty((model.InterestAmount).ToString()))
                { return BadRequest(new { Error = true, Message = " Intrest Amount Amount is Required" }); }
                if (string.IsNullOrEmpty((model.InternetBanking).ToString()))
                { return BadRequest(new { Error = true, Message = " Inernet Banking is Required" }); }
                if (string.IsNullOrEmpty((model.LegalReceived).ToString()))
                { return BadRequest(new { Error = true, Message = " Legal Recieved is Required" }); }
                if (string.IsNullOrEmpty((model.MaturityAmount).ToString()))
                { return BadRequest(new { Error = true, Message = " Matuarity Amount is Required" }); }
                if (string.IsNullOrEmpty((model.Miscode).ToString()))
                { return BadRequest(new { Error = true, Message = " MIS Code is Required" }); }
                if (string.IsNullOrEmpty((model.Moratorium).ToString()))
                { return BadRequest(new { Error = true, Message = " Moratorium is Required" }); }
                if (string.IsNullOrEmpty((model.MoraWinthInterest).ToString()))
                { return BadRequest(new { Error = true, Message = " MoraWinthInterest is Required" }); }
                if (string.IsNullOrEmpty((model.OfferLetter).ToString()))
                { return BadRequest(new { Error = true, Message = " Offer Letter is Required" }); }
                if (string.IsNullOrEmpty((model.OpeningComment).ToString()))
                { return BadRequest(new { Error = true, Message = " Opening Comment is Required" }); }
                if (string.IsNullOrEmpty((model.OutstandingPrincipal).ToString()))
                { return BadRequest(new { Error = true, Message = " OutstandingPrincipal is Required" }); }
                if (string.IsNullOrEmpty((model.Paid).ToString()))
                { return BadRequest(new { Error = true, Message = " Paid is Required" }); }
                if (string.IsNullOrEmpty((model.PdTypeId).ToString()))
                { return BadRequest(new { Error = true, Message = " Product Type ID is Required" }); }
                if (string.IsNullOrEmpty((model.Principal).ToString()))
                { return BadRequest(new { Error = true, Message = "Principal is Required" }); }
                if (string.IsNullOrEmpty((model.PrincipalFreqType).ToString()))
                { return BadRequest(new { Error = true, Message = " Principal Frequency Type is Required" }); }
                if (string.IsNullOrEmpty((model.PrincipalFrequency).ToString()))
                { return BadRequest(new { Error = true, Message = " Principal Frequency  is Required" }); }
                if (string.IsNullOrEmpty((model.ProductAcctNo).ToString()))
                { return BadRequest(new { Error = true, Message = " Product Account Number is Required" }); }
                if (string.IsNullOrEmpty((model.ProductCode).ToString()))
                { return BadRequest(new { Error = true, Message = " Produc Code is Required" }); }
                if (string.IsNullOrEmpty((model.ProductName).ToString()))
                { return BadRequest(new { Error = true, Message = " Product Name is Required" }); }
                if (string.IsNullOrEmpty((model.Rate).ToString()))
                { return BadRequest(new { Error = true, Message = " Rate is Required" }); }
                if (string.IsNullOrEmpty((model.Ref).ToString()))
                { return BadRequest(new { Error = true, Message = " Ref is Required" }); }
                if (string.IsNullOrEmpty((model.RelationshipManager).ToString()))
                { return BadRequest(new { Error = true, Message = "RelationshipManager Name is Required" }); }
                if (string.IsNullOrEmpty((model.RelationshipManagerDept).ToString()))
                { return BadRequest(new { Error = true, Message = " RelationshipManager Dept is Required" }); }
                if (string.IsNullOrEmpty((model.RelationshipOfficer).ToString()))
                { return BadRequest(new { Error = true, Message = " Relationship Officer is Required" }); }
                if (string.IsNullOrEmpty((model.RelationshipOfficerDept).ToString()))
                { return BadRequest(new { Error = true, Message = " Relationship Officer Dept  is Required" }); }
                if (string.IsNullOrEmpty((model.Riskass).ToString()))
                { return BadRequest(new { Error = true, Message = " Risk Ass is Required" }); }
                if (string.IsNullOrEmpty((model.RiskReceived).ToString()))
                { return BadRequest(new { Error = true, Message = " Risk Recieved is Required" }); }
                if (string.IsNullOrEmpty((model.SchMethod).ToString()))
                { return BadRequest(new { Error = true, Message = " Schedule Method is Required" }); }
                if (string.IsNullOrEmpty((model.SchPayment).ToString()))
                { return BadRequest(new { Error = true, Message = " Schedule Payment  is Required" }); }
                if (string.IsNullOrEmpty((model.Sms).ToString()))
                { return BadRequest(new { Error = true, Message = "SMS  is Required" }); }
                if (string.IsNullOrEmpty((model.Status).ToString()))
                { return BadRequest(new { Error = true, Message = "Status is Required" }); }
                if (string.IsNullOrEmpty((model.TargetAmount).ToString()))
                { return BadRequest(new { Error = true, Message = "TargetAmount is Required" }); }
                if (string.IsNullOrEmpty((model.TempCam).ToString()))
                { return BadRequest(new { Error = true, Message = "Temp Cam is Required" }); }
                if (string.IsNullOrEmpty((model.TempRisk).ToString()))
                { return BadRequest(new { Error = true, Message = "Temp Risk is Required" }); }
                if (string.IsNullOrEmpty((model.Tenor).ToString()))
                { return BadRequest(new { Error = true, Message = "Tenor is Required" }); }
                if (string.IsNullOrEmpty((model.TenorMode).ToString()))
                { return BadRequest(new { Error = true, Message = "Tenor Mode is Required" }); }
                if (string.IsNullOrEmpty((model.TerminalDate).ToString()))
                { return BadRequest(new { Error = true, Message = "Terminal Date is Required" }); }
                if (string.IsNullOrEmpty((model.UpfrontInterest).ToString()))
                { return BadRequest(new { Error = true, Message = "UpfrontInterest is Required" }); }
                if (string.IsNullOrEmpty((model.Upfrontprincipal).ToString()))
                { return BadRequest(new { Error = true, Message = "UpfrontPrincipal is Required" }); }

                try
                {

                    bool Result = await UnitOfWork.Credit.AddLoanApplication(model);

                    if (Result == false) { return NotFound(new { message = "Loan Not Added" }); }
                    else
                    {

                        return Ok(new { Count = 1, Message = "Loan Application Added Successfully " }); ;
                    }
                }
                catch (Exception ex)
                {
                    //log Exception
                    Logger logger = LogManager.GetLogger("databaseLogger");

                    // add custom message and pass in the exception
                    logger.Error(ex, "Whoops!");
                    return StatusCode(500);
                }
            }
            return BadRequest(new { Message = "Model Error" });
        }
        //11
        [HttpPut]
        [Route("UpdateLoanApplication/{Id}")]

        public async Task<ActionResult<string>> PutUpdateLoanApplication(int Id, [FromBody] TblBankingLoanApplication model)
        {

            if (ModelState.IsValid)
            {

                try
                {

                    var LoantoUpdate = UnitOfWork.Credit.FindLoanApplication(Id);
                    if (LoantoUpdate == null) { return NotFound(new { message = "Invalid Load ID" }); }


                    bool Result = await UnitOfWork.Credit.UpdateLoanApplication(Id, model);

                    if (Result == false) { return NotFound(new { message = "Fail" }); }
                    else
                    {

                        return Ok(new { Count = 1, Message = "Loan Application Successfully Updated" }); ;
                    }
                }
                catch (Exception ex)
                {
                    //log Exception
                    Logger logger = LogManager.GetLogger("databaseLogger");

                    // add custom message and pass in the exception
                    logger.Error(ex, "Whoops!");
                    return StatusCode(500);
                }

            }
            return BadRequest(new { Message = "Model Error" });
        }

        //12
        [HttpDelete]
        [Route("RemoveLoanApplication/{Id}")]

        public async Task<ActionResult<string>> DeleteRemoveLoanApplication(int Id)
        {

            if (ModelState.IsValid)
            {

                try
                {
                    var LoantoUpdate = UnitOfWork.Credit.FindLoanApplication(Id);
                    if (LoantoUpdate == null) { return NotFound(new { message = "Invalid Load ID" }); }

                    bool Result = await UnitOfWork.Credit.RemoveLoanApplication(Id);

                    if (Result == false) { return NotFound(new { message = "Could Not Remove Loan" }); }
                    else
                    {

                        return Ok(new { Count = 1, Message = "Loan Application Successfully Removed" }); ;
                    }
                }
                catch (Exception ex)
                {
                    //log Exception
                    Logger logger = LogManager.GetLogger("databaseLogger");

                    // add custom message and pass in the exception
                    logger.Error(ex, "Whoops!");
                    return StatusCode(500);
                }
            }
            return BadRequest(new { Message = "Model Error" });
        }


        //13
        [HttpPost]
        [Route("AddLoanProduct")]

        public async Task<ActionResult<string>> PosAddLoanProduct([FromBody] LoanProductVM model)
        {

            try
            {

               bool Result = await UnitOfWork.Credit.AddLoanProduct(model);

                if (Result == false ) { return NotFound(new { message = "Fail TO Add Loan" }); }
                else
                {

                    return Ok(new { Count = 1, Message = "Loan Product Successfully Added" }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


        //14
        [HttpPost]
        [Route("AddLoanBooking")]

        public async Task<ActionResult<string>> PostAddLoanBooking([FromBody] TblBankingLoanLease model)
        {

            try
            {

                if (string.IsNullOrEmpty((model.AgreeLoan).ToString()))
                { return BadRequest(new { Error = true, Message = " Agree Loan Amount is Required" }); }
                if (string.IsNullOrEmpty((model.AmountPaid).ToString()))
                { return BadRequest(new { Error = true, Message = " Agree Loan Amount is Required" }); }
                if (string.IsNullOrEmpty((model.Upload).ToString()))
                { return BadRequest(new { Error = true, Message = " Upload is Required" }); }
                if (string.IsNullOrEmpty((model.ApprovalComment).ToString()))
                { return BadRequest(new { Error = true, Message = " Approval Comment is Required" }); }
                if (string.IsNullOrEmpty((model.ApprovedAmount).ToString()))
                { return BadRequest(new { Error = true, Message = " Approved Amount is Required" }); }
                if (string.IsNullOrEmpty((model.ApprovedBy).ToString()))
                { return BadRequest(new { Error = true, Message = " Approved By is Required" }); }
                if (string.IsNullOrEmpty((model.Booked).ToString()))
                { return BadRequest(new { Error = true, Message = " Booked is Required" }); }
                if (string.IsNullOrEmpty((model.BranchId).ToString()))
                { return BadRequest(new { Error = true, Message = "Branch Id is Required" }); }
                if (string.IsNullOrEmpty((model.Cam).ToString()))
                { return BadRequest(new { Error = true, Message = " Cam is Required" }); }
                if (string.IsNullOrEmpty((model.CanDisburse).ToString()))
                { return BadRequest(new { Error = true, Message = " Can Disburse is Required" }); }
                if (string.IsNullOrEmpty((model.CategoryId).ToString()))
                { return BadRequest(new { Error = true, Message = " Catergory Id is Required" }); }
                if (string.IsNullOrEmpty((model.CoyCode).ToString()))
                { return BadRequest(new { Error = true, Message = " Company Code is Required" }); }
                if (string.IsNullOrEmpty((model.Currency).ToString()))
                { return BadRequest(new { Error = true, Message = " Currency is Required" }); }
                if (string.IsNullOrEmpty((model.CurrentAcct).ToString()))
                { return BadRequest(new { Error = true, Message = " Current Account is Required" }); }
                if (string.IsNullOrEmpty((model.CurrRate).ToString()))
                { return BadRequest(new { Error = true, Message = " Curr Rate is Required" }); }
                if (string.IsNullOrEmpty((model.CustCode).ToString()))
                { return BadRequest(new { Error = true, Message = " Customer Code is Required" }); }
                if (string.IsNullOrEmpty((model.CustName).ToString()))
                { return BadRequest(new { Error = true, Message = " Customer Name is Required" }); }
                if (string.IsNullOrEmpty((model.DateCreated).ToString()))
                { return BadRequest(new { Error = true, Message = " Date Created is Required" }); }
                if (string.IsNullOrEmpty((model.Email).ToString()))
                { return BadRequest(new { Error = true, Message = " Email is Required" }); }
                if (string.IsNullOrEmpty((model.Lien).ToString()))
                { return BadRequest(new { Error = true, Message = "Lien is Required" }); }
                if (string.IsNullOrEmpty((model.FeeFrequency).ToString()))
                { return BadRequest(new { Error = true, Message = " Fee Frequency is Required" }); }
                if (string.IsNullOrEmpty((model.FreqType).ToString()))
                { return BadRequest(new { Error = true, Message = " Frequency Type is Required" }); }
                if (string.IsNullOrEmpty((model.Frequency).ToString()))
                { return BadRequest(new { Error = true, Message = " Frequency is Required" }); }
                if (string.IsNullOrEmpty((model.Installments).ToString()))
                { return BadRequest(new { Error = true, Message = " Insatallment is Required" }); }
                if (string.IsNullOrEmpty((model.InterestAmount).ToString()))
                { return BadRequest(new { Error = true, Message = " Intrest Amount Amount is Required" }); }
                if (string.IsNullOrEmpty((model.InternetBanking).ToString()))
                { return BadRequest(new { Error = true, Message = " Inernet Banking is Required" }); }
                if (string.IsNullOrEmpty((model.LegalReceived).ToString()))
                { return BadRequest(new { Error = true, Message = " Legal Res=cieved is Required" }); }
                if (string.IsNullOrEmpty((model.MaturityAmount).ToString()))
                { return BadRequest(new { Error = true, Message = " Matuarity Amount is Required" }); }
                if (string.IsNullOrEmpty((model.Miscode).ToString()))
                { return BadRequest(new { Error = true, Message = " MIS Code is Required" }); }
                if (string.IsNullOrEmpty((model.Moratorium).ToString()))
                { return BadRequest(new { Error = true, Message = " Moratorium is Required" }); }
                if (string.IsNullOrEmpty((model.MoraWinthInterest).ToString()))
                { return BadRequest(new { Error = true, Message = " MoraWinthInterest is Required" }); }
                if (string.IsNullOrEmpty((model.OfferLetter).ToString()))
                { return BadRequest(new { Error = true, Message = " Offer Letter is Required" }); }
                if (string.IsNullOrEmpty((model.OpeningComment).ToString()))
                { return BadRequest(new { Error = true, Message = " Opening Comment is Required" }); }
                if (string.IsNullOrEmpty((model.OutstandingPrincipal).ToString()))
                { return BadRequest(new { Error = true, Message = " OutstandingPrincipal is Required" }); }
                if (string.IsNullOrEmpty((model.Paid).ToString()))
                { return BadRequest(new { Error = true, Message = " Paid is Required" }); }
                if (string.IsNullOrEmpty((model.PdTypeId).ToString()))
                { return BadRequest(new { Error = true, Message = " Product Type ID is Required" }); }
                if (string.IsNullOrEmpty((model.Principal).ToString()))
                { return BadRequest(new { Error = true, Message = "Principal is Required" }); }
                if (string.IsNullOrEmpty((model.PrincipalFreqType).ToString()))
                { return BadRequest(new { Error = true, Message = " Principal Frequency Type is Required" }); }
                if (string.IsNullOrEmpty((model.PrincipalFrequency).ToString()))
                { return BadRequest(new { Error = true, Message = " Principal Frequency  is Required" }); }
                if (string.IsNullOrEmpty((model.ProductAcctNo).ToString()))
                { return BadRequest(new { Error = true, Message = " Product Account Number is Required" }); }
                if (string.IsNullOrEmpty((model.ProductCode).ToString()))
                { return BadRequest(new { Error = true, Message = " Produc Code is Required" }); }
                if (string.IsNullOrEmpty((model.ProductName).ToString()))
                { return BadRequest(new { Error = true, Message = " Product Name is Required" }); }
                if (string.IsNullOrEmpty((model.Rate).ToString()))
                { return BadRequest(new { Error = true, Message = " Rate is Required" }); }
                if (string.IsNullOrEmpty((model.Ref).ToString()))
                { return BadRequest(new { Error = true, Message = " Ref is Required" }); }
                if (string.IsNullOrEmpty((model.RelationshipManager).ToString()))
                { return BadRequest(new { Error = true, Message = "RelationshipManager Name is Required" }); }
                if (string.IsNullOrEmpty((model.RelationshipManagerDept).ToString()))
                { return BadRequest(new { Error = true, Message = " RelationshipManager Dept is Required" }); }
                if (string.IsNullOrEmpty((model.RelationshipOfficer).ToString()))
                { return BadRequest(new { Error = true, Message = " Relationship Officer is Required" }); }
                if (string.IsNullOrEmpty((model.RelationshipOfficerDept).ToString()))
                { return BadRequest(new { Error = true, Message = " Relationship Officer Dept  is Required" }); }
                if (string.IsNullOrEmpty((model.Riskass).ToString()))
                { return BadRequest(new { Error = true, Message = " Risk Ass is Required" }); }
                if (string.IsNullOrEmpty((model.RiskReceived).ToString()))
                { return BadRequest(new { Error = true, Message = " Risk Recieved is Required" }); }
                if (string.IsNullOrEmpty((model.SchMethod).ToString()))
                { return BadRequest(new { Error = true, Message = " Schedule Method is Required" }); }
                if (string.IsNullOrEmpty((model.SchPayment).ToString()))
                { return BadRequest(new { Error = true, Message = " Schedule Payment  is Required" }); }
                if (string.IsNullOrEmpty((model.Sms).ToString()))
                { return BadRequest(new { Error = true, Message = "SMS  is Required" }); }
                if (string.IsNullOrEmpty((model.Status).ToString()))
                { return BadRequest(new { Error = true, Message = "Status is Required" }); }
                if (string.IsNullOrEmpty((model.TargetAmount).ToString()))
                { return BadRequest(new { Error = true, Message = "TargetAmount is Required" }); }
                if (string.IsNullOrEmpty((model.TempCam).ToString()))
                { return BadRequest(new { Error = true, Message = "Temp Cam is Required" }); }
                if (string.IsNullOrEmpty((model.TempRisk).ToString()))
                { return BadRequest(new { Error = true, Message = "Temp Risk is Required" }); }
                if (string.IsNullOrEmpty((model.Tenor).ToString()))
                { return BadRequest(new { Error = true, Message = "Tenor is Required" }); }
                if (string.IsNullOrEmpty((model.TenorMode).ToString()))
                { return BadRequest(new { Error = true, Message = "Tenor Mode is Required" }); }
                if (string.IsNullOrEmpty((model.TerminalDate).ToString()))
                { return BadRequest(new { Error = true, Message = "Terminal Date is Required" }); }
                if (string.IsNullOrEmpty((model.UpfrontInterest).ToString()))
                { return BadRequest(new { Error = true, Message = "UpfrontInterest is Required" }); }
                if (string.IsNullOrEmpty((model.Upfrontprincipal).ToString()))
                { return BadRequest(new { Error = true, Message = "UpfrontPrincipal is Required" }); }


                if (string.IsNullOrEmpty((model.BulletFreq).ToString()))
                { return BadRequest(new { Error = true, Message = " BulletFreqt is Required" }); }
                if (string.IsNullOrEmpty((model.BulletFreqName).ToString()))
                { return BadRequest(new { Error = true, Message = " BulletFreqName is Required" }); }
                if (string.IsNullOrEmpty((model.BulletName).ToString()))
                { return BadRequest(new { Error = true, Message = " BulletName is Required" }); }
                if (string.IsNullOrEmpty((model.BulletType).ToString()))
                { return BadRequest(new { Error = true, Message = " BulletType is Required" }); }
                if (string.IsNullOrEmpty((model.Disburser).ToString()))
                { return BadRequest(new { Error = true, Message = " Disburser is Required" }); }
                if (string.IsNullOrEmpty((model.FeePercent).ToString()))
                { return BadRequest(new { Error = true, Message = " FeePercentis Required" }); }
                if (string.IsNullOrEmpty((model.InstalmentLeft).ToString()))
                { return BadRequest(new { Error = true, Message = " InstalmentLeft is Required" }); }
                if (string.IsNullOrEmpty((model.MyPath).ToString()))
                { return BadRequest(new { Error = true, Message = "MyPath is Required" }); }
                if (string.IsNullOrEmpty((model.Nhf).ToString()))
                { return BadRequest(new { Error = true, Message = "NHF is Required" }); }
                if (string.IsNullOrEmpty((model.NoOfPrincipalAddition).ToString()))
                { return BadRequest(new { Error = true, Message = " NoOfPrincipalAddition is Required" }); }
                if (string.IsNullOrEmpty((model.NoOfPrincipalReduction).ToString()))
                { return BadRequest(new { Error = true, Message = " NoOfPrincipalReduction is Required" }); }
                if (string.IsNullOrEmpty((model.Officer1).ToString()))
                { return BadRequest(new { Error = true, Message = " Officer is Required" }); }
                if (string.IsNullOrEmpty((model.Officer2).ToString()))
                { return BadRequest(new { Error = true, Message = " Officer2 is Required" }); }
                if (string.IsNullOrEmpty((model.OperationId).ToString()))
                { return BadRequest(new { Error = true, Message = " OperationId is Required" }); }
                if (string.IsNullOrEmpty((model.Pccode1).ToString()))
                { return BadRequest(new { Error = true, Message = " Pccode1 is Required" }); }
                if (string.IsNullOrEmpty((model.Pccode2).ToString()))
                { return BadRequest(new { Error = true, Message = " Pccode2 is Required" }); }
                if (string.IsNullOrEmpty((model.PeriodicPay).ToString()))
                { return BadRequest(new { Error = true, Message = "PeriodicPay is Required" }); }
                if (string.IsNullOrEmpty((model.ProfileLoan).ToString()))
                { return BadRequest(new { Error = true, Message = "ProfileLoan is Required" }); }
                if (string.IsNullOrEmpty((model.Sbumis).ToString()))
                { return BadRequest(new { Error = true, Message = "Sbumis is Required" }); }


            
             

                bool Result = await UnitOfWork.Credit.AddLoanBooking(model);

                if (Result  == false) { return NotFound(new { message = "Fail" }); }
                else
                {

                    return Ok(new { Count = 1, Message = "Loan Booking Successfully Added" }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


        //15
        [HttpPut]
        [Route("UpdateLoanBooking/{Id}")]

        public async Task<ActionResult<string>> PutUpdateLoanBooking( int Id, [FromBody] TblBankingLoanLease model)
        {

            try
            {

              bool Result = await UnitOfWork.Credit.UpdateLoanBooking( Id, model);

                if (Result == false) { return NotFound(new { message = "Update Failed" }); }
                else
                {

                    return Ok(new { Count = 1, Message = "Loan Booking  Successfully Updated" }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


        //16
        [HttpDelete]
        [Route("RemoveLoanBooking/{Id}")]

        public async Task<ActionResult<string>> DeleteRemoveLoanBooking(int Id)
        {

            try
            {

               bool Result = await UnitOfWork.Credit.RemoveLoanBooking(Id);

                if (Result == false) { return NotFound(new { message = "Fail" }); }
                else
                {

                    return Ok(new { Count = 1, Message = "Loan Booking Successfully Removed" }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


        //17
        [HttpGet]
        [Route("ListGeneratedSchedule/{ProductAccountNumber}")]

        public async Task<ActionResult<IEnumerable<TblBankingTempSchedule>>> GetListGeneratedSchedule(string ProductAccountNumber)
        {

            try
            {

                var Result = await UnitOfWork.Credit.ListGeneratedSchedule(ProductAccountNumber);

                if (Result.Count() < 0) { return NotFound(new { message = "No Record Found" }); }
                else
                {

                    return Ok(new { Count = Result.Count(), Data = Result }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


        //18
        [HttpPost]
        [Route("AddLoanRestructure")]

        public async Task<ActionResult<string>> PostAddLoanRestructure([FromBody] TblBankingRestructure model)
        {
            if (string.IsNullOrEmpty((model.ApprovedAdvance).ToString()))
            { return BadRequest(new { Error = true, Message = " ApprovedAdvance is Required" }); }
            if (string.IsNullOrEmpty((model.ApprovedLease).ToString()))
            { return BadRequest(new { Error = true, Message = " ApprovedLease is Required" }); }
            if (string.IsNullOrEmpty((model.ApprovedLoan).ToString()))
            { return BadRequest(new { Error = true, Message = " ApprovedLoan is Required" }); }
            if (string.IsNullOrEmpty((model.ArchivedPrincipal).ToString()))
            { return BadRequest(new { Error = true, Message = " ArchivedPrincipal is Required" }); }
            if (string.IsNullOrEmpty((model.BatchRef).ToString()))
            { return BadRequest(new { Error = true, Message = " BatchRef is Required" }); }
            if (string.IsNullOrEmpty((model.ApprovedBy).ToString()))
            { return BadRequest(new { Error = true, Message = " Approved By is Required" }); }
        
            if (string.IsNullOrEmpty((model.BranchId).ToString()))
            { return BadRequest(new { Error = true, Message = "Branch Id is Required" }); }
           
           
            if (string.IsNullOrEmpty((model.CoyCode).ToString()))
            { return BadRequest(new { Error = true, Message = " Company Code is Required" }); }
         
            if (string.IsNullOrEmpty((model.CustCode).ToString()))
         
            if (string.IsNullOrEmpty((model.DateCreated).ToString()))
            { return BadRequest(new { Error = true, Message = " Date Created is Required" }); }
     
            if (string.IsNullOrEmpty((model.FreqType).ToString()))
            { return BadRequest(new { Error = true, Message = " Frequency Type is Required" }); }
            if (string.IsNullOrEmpty((model.Frequency).ToString()))
            { return BadRequest(new { Error = true, Message = " Frequency is Required" }); }
         
            if (string.IsNullOrEmpty((model.Moratorium).ToString()))
            { return BadRequest(new { Error = true, Message = " Moratorium is Required" }); }
           
    
            if (string.IsNullOrEmpty((model.Principal).ToString()))
            { return BadRequest(new { Error = true, Message = "Principal is Required" }); }
  
            if (string.IsNullOrEmpty((model.ProductName).ToString()))
            { return BadRequest(new { Error = true, Message = " Product Name is Required" }); }
          
            if (string.IsNullOrEmpty((model.Ref).ToString()))
            { return BadRequest(new { Error = true, Message = " Ref is Required" }); }
 

            if (string.IsNullOrEmpty((model.InstalmentLeft).ToString()))
            { return BadRequest(new { Error = true, Message = " InstalmentLeft is Required" }); }
            
            if (string.IsNullOrEmpty((model.OperationId).ToString()))
            { return BadRequest(new { Error = true, Message = " OperationId is Required" }); }
            if (string.IsNullOrEmpty((model.ExcessInterest).ToString()))
            { return BadRequest(new { Error = true, Message = "ExcessInterest is Required" }); }
            if (string.IsNullOrEmpty((model.NewRate).ToString()))
            { return BadRequest(new { Error = true, Message = " NewRate is Required" }); }
            if (string.IsNullOrEmpty((model.NewTenor).ToString()))
            { return BadRequest(new { Error = true, Message = " NewTenor is Required" }); }
            if (string.IsNullOrEmpty((model.NewTerminateDate).ToString()))
            { return BadRequest(new { Error = true, Message = " NewTerminateDate is Required" }); }
            if (string.IsNullOrEmpty((model.OldRate).ToString()))
            { return BadRequest(new { Error = true, Message = "OldRate is Required" }); }

            if (string.IsNullOrEmpty((model.PreviousTenor).ToString()))
            { return BadRequest(new { Error = true, Message = " PreviousTenor is Required" }); }
            if (string.IsNullOrEmpty((model.Reason).ToString()))
            { return BadRequest(new { Error = true, Message = "Reason is Required" }); }
            if (string.IsNullOrEmpty((model.Remark).ToString()))
            { return BadRequest(new { Error = true, Message = " Remark is Required" }); }
            if (string.IsNullOrEmpty((model.RestructureDate).ToString()))
            { return BadRequest(new { Error = true, Message = " RestructureDate is Required" }); }
          




            try
            {

                bool Result = await UnitOfWork.Credit.AddLoanRestructure(model);

                if (Result == false) { return NotFound(new { message = "Failed" }); }
                else
                {

                    return Ok(new { Count = 1, Message = "Loan Restructure Successfully Added" }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


        //19
        [HttpPost]
        [Route("AddPrincipalReduction")]

        public async Task<ActionResult<string>> PostAddPrincipalReduction([FromBody]TblBankingPrincipalReduction model)
        {

            if (string.IsNullOrEmpty((model.AccountId).ToString()))
            { return BadRequest(new { Error = true, Message = " AccountId is Required" }); }
            if (string.IsNullOrEmpty((model.AccruedInterest).ToString()))
            { return BadRequest(new { Error = true, Message = " AccruedInterest is Required" }); }
            if (string.IsNullOrEmpty((model.AmountReduced).ToString()))
            { return BadRequest(new { Error = true, Message = "AmountReduced is Required" }); }
            if (string.IsNullOrEmpty((model.ArchivedPrincipal).ToString()))
            { return BadRequest(new { Error = true, Message = " ArchivedPrincipal is Required" }); }
            if (string.IsNullOrEmpty((model.BatchRef).ToString()))
            { return BadRequest(new { Error = true, Message = " BatchRef is Required" }); }
            if (string.IsNullOrEmpty((model.ApprovedBy).ToString()))
            { return BadRequest(new { Error = true, Message = " Approved By is Required" }); }

            if (string.IsNullOrEmpty((model.ApprovalRemark).ToString()))
            { return BadRequest(new { Error = true, Message = "ApprovalRemark is Required" }); }


            if (string.IsNullOrEmpty((model.CoyCode).ToString()))
            { return BadRequest(new { Error = true, Message = " Company Code is Required" }); }

            if (string.IsNullOrEmpty((model.CustCode).ToString()))

                if (string.IsNullOrEmpty((model.DateCreated).ToString()))
                { return BadRequest(new { Error = true, Message = " Date Created is Required" }); }

            if (string.IsNullOrEmpty((model.PastDueInterest).ToString()))
            { return BadRequest(new { Error = true, Message = " PastDueInterest is Required" }); }
            if (string.IsNullOrEmpty((model.PayInterest).ToString()))
            { return BadRequest(new { Error = true, Message = " PayInterest is Required" }); }

            if (string.IsNullOrEmpty((model.InstalmentLeft).ToString()))
            { return BadRequest(new { Error = true, Message = " Moratorium is Required" }); }


            if (string.IsNullOrEmpty((model.InstalmentLeft).ToString()))
            { return BadRequest(new { Error = true, Message = " InstalmentLeft is Required" }); }

            if (string.IsNullOrEmpty((model.OperationId).ToString()))
            { return BadRequest(new { Error = true, Message = " OperationId is Required" }); }
            if (string.IsNullOrEmpty((model.ExcessInterest).ToString()))
            { return BadRequest(new { Error = true, Message = "ExcessInterest is Required" }); }
         

            if (string.IsNullOrEmpty((model.Remark).ToString()))
            { return BadRequest(new { Error = true, Message = " Remark is Required" }); }
            



            try
            {

                bool Result = await UnitOfWork.Credit.AddPrincipalReduction(model);

                if (Result == false) { return NotFound(new { message = "Addition Fail" }); }
                else
                {

                    return Ok(new { Count = 1, Message = "Principal Reduction Successfully Added" }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


        //20
        [HttpPost]
        [Route("AddPrincipalAddition")]

        public async Task<ActionResult<string>> PostAddPrincipalAddition([FromBody]TblBankingPrincipalAddition model)
        {
            if (!ModelState.IsValid) { return BadRequest(new { Count = ModelState.Count(), Message = ModelState }); }
            try
            {

               bool Result = await UnitOfWork.Credit.AddPrincipalAddition(model);

                if (Result == false) { return NotFound(new { message = "Failed " }); }
                else
                {

                    return Ok(new { Count = 1, Message = "Principal Addition Successfully Added" }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

        //21
        [HttpPost]
        [Route("AddInterestSuspension")]

        public async Task<ActionResult<string>> PostAddInterestSuspension([FromBody] TblBankingInterestSuspension model)
        {
            if (!ModelState.IsValid) { return BadRequest(new { Count = ModelState.Count(), Message = ModelState }); }
            try
            {

                bool Result = await UnitOfWork.Credit.AddInterestSuspension(model);

                if (Result == false) { return NotFound(new { message = "failed" }); }
                else
                {

                    return Ok(new { Count = 1, Message = "Intrest Suspension Successfully Added" }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


        //22
        [HttpPost]
        [Route("AddLoanCancellation")]

        public async Task<ActionResult<string>> PostAddLoanCancellation([FromBody] TblBankingLoanCancelation model)
        {

            if (!ModelState.IsValid) { return BadRequest(new { Count = ModelState.Count(), Message = ModelState }); }

            try
            {

                bool Result = await UnitOfWork.Credit.AddLoanCancellation(model);

                if (Result == false) { return NotFound(new { message = "Failed" }); }
                else
                {

                    return Ok(new { Count = 1, Message = "Loan Cancelation Successfully Created" }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

        //23
        [HttpPost]
        [Route("AddLoanTermination")]

        public async Task<ActionResult<string>> PostAddLoanTermination([FromBody] TblBankingTermination model)
        {
            if (!ModelState.IsValid) { return BadRequest(new { Count = ModelState.ErrorCount, Message = ModelState }); }

            try
            {

                bool Result = await UnitOfWork.Credit.AddLoanTermination(model);

                if (Result == false) { return NotFound(new { message = "Failed" }); }
                else
                {

                    return Ok(new { Count = 1, Message = "Loan Termination Successfully Created" }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


    }
}
 