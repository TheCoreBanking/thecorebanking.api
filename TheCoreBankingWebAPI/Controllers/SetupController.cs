﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NLog;
using TheCoreBankingWebAPI.Data;
using TheCoreBankingWebAPI.Data.Models;
using TheCoreBankingWebAPI.Data.ViewModels;

namespace TheCoreBankingWebAPI.Controllers
{
    [Route("api/v2/[controller]")]
    [ApiController]
    public class SetupController : ControllerBase
    {

        private readonly IUnitOfWork UnitOfWork;
        private ILogger<ApprovalController> logger;

        public SetupController(IUnitOfWork _unitOfWork, ILogger<ApprovalController> _logger) { this.UnitOfWork = _unitOfWork; logger = _logger; }
        //1
        [HttpGet]
        [Route("LoadCustomers")]
        public async Task<ActionResult<IEnumerable<TblCustomer>>> GetCustomers()
        {

            try
            {

                List<TblCustomer> customer = await UnitOfWork.Setup.GetAllCustomers();

                if (customer.Count() < 1) { return NotFound(new { message = "No Customer Found" }); }
                else
                {

                    return Ok(new { Count = customer.Count(), Data = customer }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

        
        [HttpGet]
        [Route("listcompany")]
        public async Task<ActionResult<IEnumerable<TblCompanyInformation>>> Getlistcompanys()
        {

            try
            {

                List<TblCompanyInformation> customer = await UnitOfWork.Setup.Listcompanys();

                if (customer.Count() < 1) { return NotFound(new { message = "No Record Found" }); }
                else
                {

                    return Ok(new { Count = customer.Count(), Data = customer }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }



        //3
        [HttpGet]
        [Route("listBranch")]
        public async Task<ActionResult<IEnumerable<TblBranchInformation>>> GetlistBranch()
        {

            try
            {

                List<TblBranchInformation> customer = await UnitOfWork.Setup.ListBranch();

                if (customer.Count() < 1) { return NotFound(new { message = "No Record Found" }); }
                else
                {

                    return Ok(new { Count = customer.Count(), Data = customer }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
            //4
        [HttpGet]
        [Route("listDirector")]
        public async Task<ActionResult<IEnumerable<TblDirectorInformation>>> GetlistDirector()
        {

            try
            {

                List<TblDirectorInformation> customer = await UnitOfWork.Setup.ListDirector();

                if (customer.Count() < 1) { return NotFound(new { message = "No Record Found" }); }
                else
                {

                    return Ok(new { Count = customer.Count(), Data = customer }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


        //5
        [HttpGet]
        [Route("listUnit")]
        public async Task<ActionResult<IEnumerable<TblUnit>>> GetlistUnit()
        {

            try
            {

                List<TblUnit> customer = await UnitOfWork.Setup.ListUnit();

                if (customer.Count() < 1) { return NotFound(new { message = "No Record Found" }); }
                else
                {

                    return Ok(new { Count = customer.Count(), Data = customer }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


        //6
        [HttpGet]
        [Route("listNaration")]
        public async Task<ActionResult<IEnumerable<GeneralSetupTblOperationComment>>> GetlistNaration()
        {

            try
            {

                List<GeneralSetupTblOperationComment> customer = await UnitOfWork.Setup.ListNaration();

                if (customer.Count() < 1) { return NotFound(new { message = "No Record Found" }); }
                else
                {

                    return Ok(new { Count = customer.Count(), Data = customer }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


        //7
        [HttpGet]
        [Route("VerifycompanyCode/{CompanyCode}")]
        public async Task<ActionResult<string>> GetVerifycompanyCode( string CompanyCode)
        {

            try
            {

                bool Result = await UnitOfWork.Setup.VerifycompanyCode(CompanyCode);

                if (Result == false) { return NotFound(new { message = " Invalid Company Code" }); }
                else
                {

                    return Ok(new { Count =1, Message = "Company Code Is Valid"}); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }



        //8
        [HttpGet]
        [Route("VerifyBranchCode/{BranchID}")]
        public async Task<ActionResult<string>> GetVerifyBranchCode( string BranchID)
        {

            try
            {

               bool Result = await UnitOfWork.Setup.VerifyBranchCode(BranchID);

                if (Result == false) { return NotFound(new { message = "Invalid Branch Code" }); }
                else
                {

                    return Ok(new { Count = 1, Message = "Branch Code Is Valid " }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


        //9
        [HttpPost]
        [Route("AddDirector")]
        public async Task<ActionResult<string>> PostDirecor([FromBody] AddDirectorVm model)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {

                bool Result = await UnitOfWork.Setup.AddDirector(model);

                if (Result == false) { return NotFound(new { message = "Fail to Add New Director" }); }
                else
                {

                    return CreatedAtAction("AddDirector","Director Successfully Added") ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

        //10
        [HttpPut]
        [Route("UpdateDirector/{Id}")]
        public async Task<ActionResult<string>> PutDirecor([FromBody] UpdateDirectorVm model, int Id)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {
                TblDirectorInformation directorInformation = await UnitOfWork.Setup.GetDirectorByID(Id);

                if(directorInformation == null) { return BadRequest(new { Message = "Invalid Director ID" }); }

                bool Result = await UnitOfWork.Setup.UpdateDirector(Id, model);

                if (Result == false) { return NotFound(new { message = "Fail to Update Director" }); }
                else
                {

                    return Ok( new { Count = 1 ,Message = "Director Successfully Updated " });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

        //11
        [HttpPost]
        [Route("AddDCompany")]
        public async Task<ActionResult<string>> PostCompany([FromBody] AddCompanyVm model)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {

                bool Result = await UnitOfWork.Setup.Addcompany(model);

                if (Result == false) { return NotFound(new { message = "Fail to Add New Company" }); }
                else
                {

                    return CreatedAtAction("AddDCompany", "Company Successfully Added");
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

        //12
        [HttpPut]
        [Route("UpdateCompany/{Id}")]
        public async Task<ActionResult<string>> PutCompany([FromBody] UpdateCompanyVm model, int Id)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {
                TblCompanyInformation companyInformation = await UnitOfWork.Setup.GetcompanyById(Id);

                if (companyInformation == null) { return BadRequest(new { Message = "Invalid Company ID" }); }

                bool Result = await UnitOfWork.Setup.Updatecompany(Id, model);

                if (Result == false) { return NotFound(new { message = "Fail to Update Director" }); }
                else
                {

                    return Ok(new { Count = 1, Message = "Company Successfully Updated " });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

        //13
        [HttpGet]
        [Route("GetDirectorbyBvn/{Bvn}")]
        public async Task<ActionResult<TblDirectorInformation>> GetDirectorbvn(string Bvn)
        {

            try
            {

               TblDirectorInformation Direcor = await UnitOfWork.Setup.GetDirectorByBvn(Bvn);

                if (Direcor == null) { return NotFound(new { message = "No Record Found" }); }
                else
                {

                    return Ok(new { Count = 1, Data = Direcor }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //14
        [HttpDelete]
        [Route("DeleteNaration/{Id}")]
        public async Task<ActionResult<string>> DeletetNaration( int Id)
        {

            try
            {

              bool  Result  = await UnitOfWork.Setup.DeleteNaration(Id);

                if (Result == false) { return NotFound(new { message = "Could Not Delete Naration" }); }
                else
                {

                    return Ok(new { Count = 1, Message = "Naration Successfully Deleted" }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

        //15
        [HttpPost]
        [Route("CreateNaration")]
        public async Task<ActionResult<string>> PostNaration([FromBody] NarationVm model)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {

                bool Result = await UnitOfWork.Setup.AddNaration(model);

                if (Result == false) { return NotFound(new { message = "Fail to Add New Naration" }); }
                else
                {

                    return CreatedAtAction("AddNaration", "Naration Successfully Added");
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

      

       //16
       [HttpPost]
       [Route("AddDMisInformation")]
       public async Task<ActionResult<string>> PostMis([FromBody] AddMisVm model)
       {
           if (!ModelState.IsValid) { return BadRequest(ModelState); }

           try
           {

               bool Result = await UnitOfWork.Setup.AddMis(model);

               if (Result == false) { return NotFound(new { message = "Fail to Add New Mis Information" }); }
               else
               {

                   return CreatedAtAction("AddDMis", "MIS Information Successfully Added");
               }
           }
           catch (Exception ex)
           {
               //log Exception
               Logger logger = LogManager.GetLogger("databaseLogger");

               // add custom message and pass in the exception
               logger.Error(ex, "Whoops!");
               return StatusCode(500);
           }

       }
       
     //17
     [HttpPut]
     [Route("UpdateMis/{Id}")]
     public async Task<ActionResult<string>> PutMis([FromBody] UpdateMisVm model, int Id)
     {
         if (!ModelState.IsValid) { return BadRequest(ModelState); }

         try
         {
             TblMisinformation Mis = await UnitOfWork.Setup.GetMisById(Id);

             if (Mis == null) { return BadRequest(new { Message = "Invalid Mis  ID" }); }

             bool Result = await UnitOfWork.Setup.UpdateMis(Id, model);

             if (Result == false) { return NotFound(new { message = "Fail to Update Mis" }); }
             else
             {

                 return Ok(new { Count = 1, Message = "Mis Successfully Updated " });
             }
         }
         catch (Exception ex)
         {
             //log Exception
             Logger logger = LogManager.GetLogger("databaseLogger");

             // add custom message and pass in the exception
             logger.Error(ex, "Whoops!");
             return StatusCode(500);
         }

     }
       
    //18
    [HttpPost]
    [Route("AddBranch")]
    public async Task<ActionResult<string>> PostBranch([FromBody] AddbranchVm model)
    {
        if (!ModelState.IsValid) { return BadRequest(ModelState); }

        try
        {

            bool Result = await UnitOfWork.Setup.AddBranch(model);

            if (Result == false) { return NotFound(new { message = "Fail to Add New Branch" }); }
            else
            {

                return CreatedAtAction("AddBranch", "Branch Information Successfully Added");
            }
        }
        catch (Exception ex)
        {
            //log Exception
            Logger logger = LogManager.GetLogger("databaseLogger");

            // add custom message and pass in the exception
            logger.Error(ex, "Whoops!");
            return StatusCode(500);
        }

    }
       
    //19
    [HttpPut]
    [Route("UpdateBranch/{Id}")]
    public async Task<ActionResult<string>> PutBranch([FromBody] UpdatebranchVm model, int Id)
    {
        if (!ModelState.IsValid) { return BadRequest(ModelState); }

        try
        {
        TblBranchInformation Mis = await UnitOfWork.Setup.GetBranchId(Id);

            if (Mis == null) { return BadRequest(new { Message = "Invalid Branch  ID" }); }

            bool Result = await UnitOfWork.Setup.UpdateBranch(Id, model);

            if (Result == false) { return NotFound(new { message = "Fail to Update Branch" }); }
            else
            {

                return Ok(new { Count = 1, Message = "Mis Successfully Updated " });
            }
        }
        catch (Exception ex)
        {
            //log Exception
            Logger logger = LogManager.GetLogger("databaseLogger");

            // add custom message and pass in the exception
            logger.Error(ex, "Whoops!");
            return StatusCode(500);
        }

    }
     
      

        //20
        [HttpPost]
        [Route("AddDepartment")]
        public async Task<ActionResult<string>> PostDepartment([FromBody] DepartmentVm model)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {

                bool Result = await UnitOfWork.Setup.AddDepartment(model);

                if (Result == false) { return NotFound(new { message = "Fail to Add New Department" }); }
                else
                {

                    return CreatedAtAction("AddDepartment", "Department Successfully Added");
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

         
        //21
        [HttpPost]
        [Route("AddDesignation")]
        public async Task<ActionResult<string>> PostDesignation([FromBody] DesignationVm model)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {

                bool Result = await UnitOfWork.Setup.AddDesignation(model);

                if (Result == false) { return NotFound(new { message = "Fail to Add New Designation" }); }
                else
                {

                    return CreatedAtAction("AddDesignation", "Designation Information Successfully Added");
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


        //22
        [HttpPost]
        [Route("AddUnit")]
        public async Task<ActionResult<string>> PostUnit([FromBody] AddUnitVm model)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {

                bool Result = await UnitOfWork.Setup.AddUnit(model);

                if (Result == false) { return NotFound(new { message = "Fail to Add New Unit" }); }
                else
                {

                    return CreatedAtAction("AddUnit", "Unit  Information Successfully Added");
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
              
        //23
        [HttpPut]
        [Route("UpdateUnit/{Id}")]
        public async Task<ActionResult<string>> PutUnit([FromBody] UpdateUnitVm model, int Id)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {
                TblUnit Unit = await UnitOfWork.Setup.GetUnitById(Id);

                if (Unit == null) { return BadRequest(new { Message = "Invalid Unit  ID" }); }

                bool Result = await UnitOfWork.Setup.UpdateUnit(Id, model);

                if (Result == false) { return NotFound(new { message = "Fail to Update Unit" }); }
                else
                {

                    return Ok(new { Count = 1, Message = "Unit Successfully Updated " });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

 

    }
}
