﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NLog;
using TheCoreBankingWebAPI.Data;
using TheCoreBankingWebAPI.Data.Models;

namespace TheCoreBankingWebAPI.Controllers
{
    [Route("api/v2/[controller]")]
    [ApiController]
    public class ApprovalController : ControllerBase
    {

        private readonly IUnitOfWork UnitOfWork;
        private ILogger<ApprovalController> logger;

        public ApprovalController(IUnitOfWork _unitOfWork, ILogger<ApprovalController> _logger) { this.UnitOfWork = _unitOfWork; logger = _logger; }
        //1
        [HttpGet]
        [Route("LoadCustomers")]
        public async Task<ActionResult<IEnumerable<TblCustomer>>> GetCustomers()
        {

            try
            {

                List<TblCustomer> customer = await UnitOfWork.Customer.GetAllCustomers();

                if (customer.Count() < 1) { return NotFound(new { message = "No Customer Found" }); }
                else
                {

                    return Ok(new { Count = customer.Count(), Data = customer }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

    }
}
