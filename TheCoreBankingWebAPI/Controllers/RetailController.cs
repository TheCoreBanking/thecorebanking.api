﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NLog;
using TheCoreBankingWebAPI.Data;
using TheCoreBankingWebAPI.Data.Models;
using TheCoreBankingWebAPI.Data.ViewModels;

namespace TheCoreBankingWebAPI.Controllers
{


    [Route("api/v2/[controller]")]
    [ApiController]
    public class RetailController : ControllerBase
    {
        private readonly IUnitOfWork UnitOfWork;
        private ILogger<RetailController> logger;

        public RetailController(IUnitOfWork _unitOfWork, ILogger<RetailController> _logger) { this.UnitOfWork = _unitOfWork; logger = _logger; }
        //1
        [HttpGet]
        [Route("LoadCustomers")]
        public async Task<ActionResult<IEnumerable<TblCustomer>>> GetCustomers()
        {

            try
            {

                List<TblCustomer> customer = await UnitOfWork.Retail.GetAllCustomers();

                if (customer.Count() < 1) { return NotFound(new { message = "No Customer Found" }); }
                else
                {

                    return Ok(new { Count = customer.Count(), Data = customer }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }

        //2
        [HttpGet]
        [Route("GenerateCustomerCode/{customerId}")]

        public async Task<ActionResult<string>> GenerateCustomerCode(long customerId)
        {
            try
            {
                var Customer = await UnitOfWork.Retail.GetCustomerbyID(Convert.ToInt32(customerId));

                if (Customer == null) { return NotFound(new { Message = "Invalid Customer ID" }); }
                int Result = await UnitOfWork.Customer.GenerateCustomerCode(customerId);

                if (Result < 1) { return NotFound(new { Message = "Could Not Generate Code" }); }
                else
                {

                    return Ok(new { Count = Result, Message = " Customer Code Successfully Generated" }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }
        }
        //3
        [HttpGet]
        [Route("LoadCustomer/{id}")]
        public async Task<ActionResult<TblCustomer>> GetCustomer(int id)
        {

            try
            {

                var customer = await UnitOfWork.Retail.GetCustomerbyID(id);
                if (customer == null) { return NotFound(new { message = "Customer Not Found" }); }
                else
                {
                    return Ok(new { Data = customer });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //4
        [HttpGet]
        [Route("LoadCustomerAccountType")]
        public async Task<ActionResult<IEnumerable<TblCustomeraccounttype>>> Get()
        {

            try
            {

                List<TblCustomeraccounttype> accounttype = await UnitOfWork.Retail.LoadAccountType();
                if (accounttype.Count() < 1) { return NotFound(new { message = "No Account Type Found" }); }
                else
                {
                    return Ok(new { Count = accounttype.Count(), Data = accounttype });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //5
        [HttpGet]
        [Route("LoadAnnualIncomes")]
        public async Task<ActionResult<IEnumerable<TblAnnualincome>>> GetLoadAnnualIncomess()
        {

            try
            {

                List<TblAnnualincome> annualincomes = await UnitOfWork.Retail.LoadAnnualIncome();
                if (annualincomes.Count() < 1) { return NotFound(new { message = "No AnnualIncome Found" }); }
                else
                {
                    return Ok(new { Count = annualincomes.Count(), Data = annualincomes });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //6

        [HttpGet]
        [Route("LoadKYCItem/{id}")]
        public async Task<ActionResult<TblKycitem>> GetLoadKYCItem(int id)
        {

            try
            {

                var kycItem = await UnitOfWork.Retail.LoadKycItem(id);
                if (kycItem == null) { return NotFound(new { message = "No Kycitem  Found" }); }
                else
                {
                    return Ok(new { Data = kycItem });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //7
        [HttpGet]
        [Route("LoadCustomerAddresse/{id}")]
        public async Task<ActionResult<string>> GetLoadCustomerAddresse(int id)
        {

            try
            {

                var customer = await UnitOfWork.Retail.LoadcustomerAddress(id);
                if (customer == null) { return NotFound(new { message = "Not Address Found" }); }
                else
                {

                    return Ok(new
                    {
                        Data = customer
                    });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //8
        [HttpGet]
        [Route("LoadFundSources")]
        public async Task<ActionResult<List<TblSourceoffunds>>> GetLoadFundSources()
        {

            try
            {

                List<TblSourceoffunds> sourceoffunds = await UnitOfWork.Retail.LoadFundSourcs();
                if (sourceoffunds.Count() < 1) { return NotFound(new { Message = "Source of Fund Not Found" }); }
                else
                {
                    return Ok(new { Count = sourceoffunds.Count(), Data = sourceoffunds });

                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //9
        [HttpGet]
        [Route("LoadCustomerPhones/{id}")]
        public async Task<ActionResult<string>> GetLoadCustomerPhones(int id)
        {

            try
            {
                var customer = await UnitOfWork.Retail.LoadcustomerPhone(id);

                if (customer == null) { return NotFound(new { Message = "No Customer Found " }); }
                else
                {
                    return Ok(new { Data = customer });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //10
        [HttpGet]
        [Route("LoadCustomerEmail/{id}")]
        public async Task<ActionResult<string>> GetLoadCustomerEmail(int id)
        {

            try
            {
                var customer = await UnitOfWork.Retail.LoadcustomerEmail(id);

                if (customer == null) { return NotFound(new { Message = "No Customer Found " }); }
                else
                {
                    return Ok(new { Data = customer });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //11

        [HttpGet]
        [Route("LoadAccount")]
        public async Task<ActionResult<List<TblCasa>>> GetLoadAccount()
        {

            try
            {

                List<TblCasa> account = await UnitOfWork.Retail.LoadAccounts();

                if (account.Count() < 1) { return NotFound(new { Message = "No Acount Record Found" }); }

                else
                {
                    return Ok(new { Count = account.Count(), Data = account });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //12
        [HttpGet]
        [Route("LoadAccountsByCasaAccountId/{id}")]
        public async Task<ActionResult<TblCasa>> GetLoadAccountsByCasaAccountId(int id)
        {



            try
            {

                var account = await UnitOfWork.Retail.LoadAccountById(id);

                if (account == null) { return NotFound(new { Message = "No Acount Record Found" }); }

                else
                {
                    return Ok(new { Count = 1, Data = account });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }
        }
        //13

        [HttpGet]
        [Route("LoadDomantAccount")]
        public async Task<ActionResult<TblCasa>> GetLoadDomantAccount()
        {


            try
            {

                List<TblCasa> account = await UnitOfWork.Retail.LoadDomantAccount();

                if (account.Count() < 1) { return NotFound(new { Message = "No Acount Record Found" }); }

                else
                {
                    return Ok(new { Count = account.Count(), Data = account });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }


        }

        //14
        [HttpGet]
        [Route("ProductCasaAccountId/{accountName}")]
        public async Task<ActionResult<int>> GetProductCasaAccountId(string accountName)
        {


            try
            {

                string productId = await UnitOfWork.Retail.GetProductCasaAccountId(accountName);

                if (productId == null) { return NotFound(new { Message = "No Acount Record Found" }); }

                else
                {
                    return Ok(new { Count = 1, Data = productId });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }


        }
        //15
        [HttpGet]
        [Route("LoadFrozenAccountst")]
        //16
        public async Task<ActionResult<List<TblAccountfreeze>>> GetLoadFrozenAccounts()
        {


            try
            {

                var account = await UnitOfWork.Retail.LoadFrozenAccounts();

                if (account.Count() < 1) { return NotFound(new { Message = "No Acount Record Found" }); }

                else
                {
                    return Ok(new { Count = account.Count(), Data = account });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }


        }
        //17
        [HttpGet]
        [Route("LoadAccountCardTypes")]
        public async Task<ActionResult<List<TblAccountcardtype>>> GetLoadAccountCardTypes()
        {


            try
            {

                List<TblAccountcardtype> account = await UnitOfWork.Retail.LoadAccountCardTypes();

                if (account.Count() < 1) { return NotFound(new { Message = "No Account Record Found" }); }

                else
                {
                    return Ok(new { Count = account.Count(), Data = account });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }


        }
        //18
        [HttpGet]
        [Route("ConfirmFreezeStatus/{accountNo}")]
        public async Task<ActionResult<TblAccountfreeze>> GetConfirmFreezeStatus(string accountNo)
        {

            try
            {

                var account = await UnitOfWork.Retail.ConfirmFreezeStatus(accountNo);

                if (account == null) { return NotFound(new { Message = "No Account Record Found" }); }

                else
                {
                    return Ok(new { Count = 1, Data = account.IsFreeze });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //19
        [HttpGet]
        [Route("ConfirmFreezeReversalStatus/{accountNo}")]
        public async Task<ActionResult<TblAccountfreeze>> GetConfirmFreezeReversalStatus(string accountNo)
        {


            try
            {

                var account = await UnitOfWork.Retail.ConfirmFreezeReversalStatus(accountNo);

                if (account == null) { return NotFound(new { Message = "Not Reverse" }); }

                else
                {
                    return Ok(new { Count = 1, Data = account.IsReversed });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //20
        [HttpGet]
        [Route("listClosedAndApprovedAccounts")]
        public async Task<ActionResult<List<TblAccountclosure>>> GetlistClosedAndApprovedAccounts()
        {


            try
            {

                List<TblAccountclosure> account = await UnitOfWork.Retail.ListClosedAndApprovedAccounts();

                if (account.Count() < 1) { return NotFound(new { Message = "No Acount Record Found" }); }

                else
                {
                    return Ok(new { Count = account.Count(), Data = account });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }


        }
        //21
        [HttpGet]
        [Route("LoadGlobalBalance/{customercode}")]
        public async Task<ActionResult<object>> GetGlobalBalance(string customercode)
        {


            try
            {

                object GlobaBalance = await UnitOfWork.Retail.GlobalBalance(customercode);

                if (GlobaBalance == null) { return NotFound(new { Message = "No Acount Record Found" }); }

                else
                {
                    return Ok(new { Count = 1, Data = GlobaBalance });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }


        }
        //22
        [HttpGet]
        [Route("listfreezetransaction")]
        public async Task<ActionResult<List<TblFreezetransactionlist>>> Getfreezetransaction()
        {


            try
            {

                List<TblFreezetransactionlist> freezetransactionlists = await UnitOfWork.Retail.Loadfreezetransaction();

                if (freezetransactionlists.Count() < 1) { return NotFound(new { Message = "No freeze transaction Record Found" }); }

                else
                {
                    return Ok(new { Count = freezetransactionlists.Count(), Data = freezetransactionlists });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }


        }
        //23

        [HttpGet]
        [Route("LoadAccountPostStatuses")]
        public async Task<ActionResult<List<TblCasapostnostatus>>> GetLoadAccountPostStatuses()
        {


            try
            {

                List<TblCasa> account = await UnitOfWork.Retail.LoadAccounts();

                if (account.Count() < 1) { return NotFound(new { Message = "No Acount Record Found" }); }

                else
                {
                    return Ok(new { Count = account.Count(), Data = account });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }


        }

        //24
        [HttpPost]
        [Route("AddCustomer")]
        public async Task<ActionResult<TblCustomer>> Post([FromBody] CustomerViewModel customer)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    bool Result = await UnitOfWork.Retail.AddCustomer(customer);

                    if (Result == false) { return BadRequest(new { Message = "Could Not Add Customer" }); }
                    else
                    {

                        return Ok(new { Message = "Customer Successfully Added " });
                    }


                }
                else { return BadRequest(new { Message = "Model Error" }); }

            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //25
        [HttpPut]
        [Route("UpdateCustomer")]
        public async Task<ActionResult<TblCustomer>> Put([FromBody] CustomerViewModel customer)
        {

            try
            {
                if (ModelState.IsValid)
                {

                    var Customer = await UnitOfWork.Retail.GetCustomerbyID(customer.CustomerId);
                    if (customer == null) { return BadRequest(new { Message = "Customer Dose Not Exist" }); }

                    bool Result = await UnitOfWork.Customer.UpdateCustomer(customer);

                    if (Result == false) { return BadRequest(new { Message = "Could Not Add Customer" }); }
                    else
                    {

                        return Ok(new { Message = "Customer Successfully Added " });
                    }


                }
                else { return BadRequest(new { Message = "Model Error" }); }

            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }


    }
}
