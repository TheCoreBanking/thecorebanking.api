﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TheCoreBankingWebAPI.Data.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using NLog;
using TheCoreBankingWebAPI.Data;
using TheCoreBankingWebAPI.Data.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace TheCoreBankingWebAPI.Controllers
{
    [Route("api/v2/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {

        private readonly IUnitOfWork UnitOfWork;
        private ILogger<CustomerController> logger;
        private readonly TheCoreBankingAzureContext _context;
        Mailer mail = new Mailer();

        public CustomerController(IUnitOfWork _unitOfWork, TheCoreBankingAzureContext context, ILogger<CustomerController> _logger) { this.UnitOfWork = _unitOfWork; logger = _logger; _context = context; }
        //1
        [HttpGet]
        [Route("LoadCustomers")]
        public async Task<ActionResult<IEnumerable<TblCustomer>>> GetCustomers() {

            try {

                List<TblCustomer> customer = await UnitOfWork.Customer.GetAllCustomers();

                if (customer.Count() < 1) { return NotFound(new { message = "No Customer Found" }); }
                else
                {

                    return Ok(new { Count = customer.Count(), Data = customer }); ;
                }
            }
            catch(Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }
           
        }

        //2
        [HttpGet]
        [Route("GenerateCustomerCode/{customerId}")]

        public async Task< ActionResult<string>> GenerateCustomerCode(long customerId)
        {
            try
            {
                var Customer = await UnitOfWork.Customer.GetCustomerbyID(Convert.ToInt32(customerId));

                if(Customer == null) { return NotFound(new { Message = "Invalid Customer ID" }); }
                int Result  = await UnitOfWork.Customer.GenerateCustomerCode(customerId);

                if (Result < 1) { return NotFound(new { Message = "Could Not Generate Code" }); }
                else
                {

                    return Ok(new { Count = Result, Message = " Customer Code Successfully Generated" }); ;
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }
        }
        //3
        [HttpGet]
        [Route("LoadCustomer/{id}")]
        public async Task<ActionResult<TblCustomer>> GetCustomer(int id)
        {

            try
            {

                var customer = await UnitOfWork.Customer.GetCustomerbyID(id);
                if (customer == null) { return NotFound(new { message = "Customer Not Found" }); }
                else
                {
                    return Ok( new { Data = customer });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //4
        [HttpGet]
        [Route("LoadCustomerAccountType")]
        public async Task<ActionResult<IEnumerable<TblCustomeraccounttype>>> Get()
        {

            try
            {

                List<TblCustomeraccounttype> accounttype = await UnitOfWork.Customer.LoadAccountType();
                if (accounttype.Count() < 1) { return NotFound(new { message = "No Account Type Found" }); }
                else
                {
                    return Ok( new { Count = accounttype.Count(), Data = accounttype });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //5
        [HttpGet]
        [Route("LoadAnnualIncomes")]
        public async Task<ActionResult<IEnumerable<TblAnnualincome>>> GetLoadAnnualIncomess()
        {

            try
            {

                List<TblAnnualincome> annualincomes = await UnitOfWork.Customer.LoadAnnualIncome();
                if (annualincomes.Count() < 1) { return NotFound(new { message = "No AnnualIncome Found" }); }
                else
                {
                    return Ok( new { Count = annualincomes.Count(), Data = annualincomes });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //6
     
        [HttpGet]
        [Route("LoadKYCItem/{id}")]
        public async Task<ActionResult<TblKycitem>> GetLoadKYCItem(int id)
        {

            try
            {

                var kycItem = await UnitOfWork.Customer.LoadKycItem(id);
                if (kycItem == null) { return NotFound(new { message = "No Kycitem  Found" }); }
                else
                {
                    return Ok( new { Data = kycItem });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //7
        [HttpGet]
        [Route("LoadCustomerAddresse/{id}")]
        public async Task<ActionResult<string>> GetLoadCustomerAddresse(int id)
        {

            try
            {

                var customer = await UnitOfWork.Customer.LoadcustomerAddress(id);
                if (customer == null) { return NotFound(new { message = "Not Address Found" }); }
                else
                {

                    return Ok(new
                    {
                        Data = customer
                    });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //8
        [HttpGet]
        [Route("LoadFundSources")]
        public async Task<ActionResult<List<TblSourceoffunds>>> GetLoadFundSources()
        {

            try
            {

                List<TblSourceoffunds> sourceoffunds = await UnitOfWork.Customer.LoadFundSourcs();
                if(sourceoffunds.Count()< 1) { return NotFound(new { Message = "Source of Fund Not Found" }); }
                else
                {
                    return Ok(new { Count = sourceoffunds.Count(), Data = sourceoffunds });

                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //9
        [HttpGet]
        [Route("LoadCustomerPhones/{id}")]
        public async Task<ActionResult<string>> GetLoadCustomerPhones( int id)
        {

            try
            {
                var customer = await UnitOfWork.Customer.LoadcustomerPhone(id);

                if (customer == null) { return NotFound(new { Message = "No Customer Found " }); }
                else
                {
                    return Ok( new { Data = customer });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //10
        [HttpGet]
        [Route("LoadCustomerEmail/{id}")]
        public async Task<ActionResult<string>> GetLoadCustomerEmail(int id)
        {

            try
            {
                var customer = await UnitOfWork.Customer.LoadcustomerEmail(id);

                if (customer == null) { return NotFound(new { Message = "No Customer Found " }); }
                else
                {
                    return Ok(new { Data = customer });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //11

        [HttpGet]
        [Route("LoadAccount")]
        public async Task<ActionResult<List<TblCasa>>> GetLoadAccount()
        {

            try
            {

                List<TblCasa> account = await UnitOfWork.Customer.LoadAccounts();

                if (account.Count() < 1) { return NotFound(new { Message = "No Acount Record Found" }); }

                else
                {
                    return Ok(new { Count = account.Count(), Data = account });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //12
        [HttpGet]
        [Route("LoadAccountsByCasaAccountId/{id}")]
        public async Task<ActionResult<TblCasa>> GetLoadAccountsByCasaAccountId(int id)
        {



            try
            {

              var account = await UnitOfWork.Customer.LoadAccountById(id);

                if (account== null) { return NotFound(new { Message = "No Acount Record Found" }); }

                else
                {
                    return Ok(new { Count = 1, Data = account });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }
        }
        //13

        [HttpGet]
        [Route("LoadDomantAccount")]
        public async Task<ActionResult<TblCasa>> GetLoadDomantAccount()
        {


            try
            {

                List<TblCasa> account = await UnitOfWork.Customer.LoadDomantAccount();

                if (account.Count() < 1) { return NotFound(new { Message = "No Acount Record Found" }); }

                else
                {
                    return Ok(new { Count = account.Count(), Data = account });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }


        }

        //14
        [HttpGet]
        [Route("ProductCasaAccountId/{accountName}")]
        public async Task<ActionResult<int>> GetProductCasaAccountId(string accountName)
        {


            try
            {

                string productId = await UnitOfWork.Customer.GetProductCasaAccountId(accountName);

                if (productId == null) { return NotFound(new { Message = "No Acount Record Found" }); }

                else
                {
                    return Ok(new { Count = 1, Data = productId });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }


        }
        //15
        [HttpGet]
        [Route("LoadFrozenAccountst")]
        //16
        public async Task<ActionResult<List<TblAccountfreeze>>> GetLoadFrozenAccounts()
        {


            try
            {

                var account = await UnitOfWork.Customer.LoadFrozenAccounts();

                if (account.Count() < 1) { return NotFound(new { Message = "No Acount Record Found" }); }

                else
                {
                    return Ok(new { Count = account.Count(), Data = account });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }


        }
        //17
        [HttpGet]
        [Route("LoadAccountCardTypes")]
        public async Task<ActionResult<List< TblAccountcardtype>>> GetLoadAccountCardTypes()
        {


            try
            {

                List<TblAccountcardtype> account = await UnitOfWork.Customer.LoadAccountCardTypes();

                if (account.Count() < 1) { return NotFound(new { Message = "No Account Record Found" }); }

                else
                {
                    return Ok(new { Count = account.Count(), Data = account });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }


        }
        //18
        [HttpGet]
        [Route("ConfirmFreezeStatus/{accountNo}")]
        public async Task<ActionResult<TblAccountfreeze>> GetConfirmFreezeStatus(string accountNo)
        {

            try
            {

               var account = await UnitOfWork.Customer.ConfirmFreezeStatus(accountNo);

                if (account == null) { return NotFound(new { Message = "No Account Record Found" }); }

                else
                {
                    return Ok(new { Count =1, Data = account.IsFreeze });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //19
        [HttpGet]
        [Route("ConfirmFreezeReversalStatus/{accountNo}")]
        public async Task<ActionResult<TblAccountfreeze>> GetConfirmFreezeReversalStatus(string accountNo)
        {


            try
            {

                var account = await UnitOfWork.Customer.ConfirmFreezeReversalStatus(accountNo);

                if (account == null) { return NotFound(new { Message = "Not Reverse" }); }

                else
                {
                    return Ok(new { Count = 1, Data = account.IsReversed });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //20
        [HttpGet]
        [Route("listClosedAndApprovedAccounts")]
        public async Task<ActionResult<List< TblAccountclosure>>> GetlistClosedAndApprovedAccounts()
        {


            try
            {

                List<TblAccountclosure> account = await UnitOfWork.Customer.ListClosedAndApprovedAccounts();

                if (account.Count() < 1) { return NotFound(new { Message = "No Acount Record Found" }); }

                else
                {
                    return Ok(new { Count = account.Count(), Data = account });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }


        }
        //21
        [HttpGet]
        [Route("LoadGlobalBalance/{customercode}")]
        public async Task<ActionResult<object>> GetGlobalBalance(string customercode) {


            try
            {

                object GlobaBalance = await UnitOfWork.Customer.GlobalBalance(customercode);

                if (GlobaBalance == null) { return NotFound(new { Message = "No Acount Record Found" }); }

                else
                {
                    return Ok(new { Count = 1, Data = GlobaBalance });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }


        }
        //22
        [HttpGet]
        [Route("listfreezetransaction")]
        public async Task<ActionResult<List<TblFreezetransactionlist>>> Getfreezetransaction() {


            try
            {

                List<TblFreezetransactionlist> freezetransactionlists = await UnitOfWork.Customer.Loadfreezetransaction();

                if (freezetransactionlists.Count() < 1) { return NotFound(new { Message = "No freeze transaction Record Found" }); }

                else
                {
                    return Ok(new { Count = freezetransactionlists.Count(), Data = freezetransactionlists });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }


        }
        //23

        [HttpGet]
        [Route("LoadAccountPostStatuses")]
        public async Task<ActionResult<List< TblCasapostnostatus>>> GetLoadAccountPostStatuses()
        {


            try
            {

                List<TblCasa> account = await UnitOfWork.Customer.LoadAccounts();

                if (account.Count() < 1) { return NotFound(new { Message = "No Acount Record Found" }); }

                else
                {
                    return Ok(new { Count = account.Count(), Data = account });
                }
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }


        }

        //24
        [HttpPost]
        [Route("AddCustomer")]
        public async Task<ActionResult<TblCustomer>> Post([FromBody] CustomerViewModel customer) {

            try
            {
                if (ModelState.IsValid)
                {
                    bool Result = await UnitOfWork.Customer.AddCustomer(customer);

                    if(Result == false) { return BadRequest(new { Message = "Could Not Add Customer"}); }
                    else {

                        return Ok( new { Message = "Customer Successfully Added "});
                    }

                  
                }
                else  { return BadRequest(new { Message = "Model Error" }); }
               
            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //25
        [HttpPut]
        [Route("UpdateCustomer")]
        public async Task<ActionResult<TblCustomer>> Put([FromBody] CustomerViewModel customer)
        {

            try
            {
                if (ModelState.IsValid)
                {

                    var Customer = await UnitOfWork.Customer.GetCustomerbyID(customer.CustomerId);
                    if(customer == null) { return BadRequest(new { Message = "Customer Dose Not Exist" }); }

                    bool Result = await UnitOfWork.Customer.UpdateCustomer(customer);

                    if (Result == false) { return BadRequest(new { Message = "Could Not Add Customer" }); }
                    else
                    {

                        return Ok(new { Message = "Customer Successfully Added " });
                    }


                }
                else { return BadRequest(new { Message = "Model Error" }); }

            }
            catch (Exception ex)
            {
                //log Exception
                Logger logger = LogManager.GetLogger("databaseLogger");

                // add custom message and pass in the exception
                logger.Error(ex, "Whoops!");
                return StatusCode(500);
            }

        }
        //26
        [HttpPost]
        [Route("PostNewAccountInformation")]
        public ActionResult PostNewAccountInformation(CreateAccountRequest request)
        {
            Random random = new Random();
            var getCustomerID = UnitOfWork.Customer.GetAllCustomers().Result.LastOrDefault().Customerid;
            int customerID = getCustomerID + 1;
            var CustCode = UpdateCustomerCode(customerID);
            var phoneContact = new TblCustomerphonecontact()
            {
                Phone = request.phonenumber,
                Customerid = getCustomerID + 1,
                Active = true
            };
            _context.TblCustomerphonecontact.Add(phoneContact);
            _context.SaveChanges();
            var emailContact = new TblCustomeremailcontact()
            {
                Email = request.email,
                Customerid = getCustomerID + 1,
                Active = true
            };
            _context.TblCustomeremailcontact.Add(emailContact);
            _context.SaveChanges();
            var addCustomer = new TblCustomer()
            {
                Bvn = request.bvn,
                Customercode = _context.TblCustomer.Where(i => i.Customerid == customerID).ToList().FirstOrDefault().Customercode
            };
            _context.TblCustomer.Add(addCustomer);
            var customerSave = _context.SaveChanges();
            var accountNo = Convert.ToString(random.Next(1, 1000000000));
            var addAccount = new TblCasa()
            {
                Customercode = _context.TblCustomer.Where(i => i.Customerid == customerID).ToList().FirstOrDefault().Customercode,
                Accountname = request.fullname,
                Accountnumber = accountNo,
                Customerid = customerID
            };
            _context.TblCasa.Add(addAccount);
            var accountSave = _context.SaveChanges();
            if (customerSave == 1 && accountSave == 1)
            {
                mail.SendMailWithTemplate(request.fullname, accountNo, DateTime.Now, request.email, "Fintrak Banking", "Instant Account Opening");
            }
            return CreatedAtAction("GetAllCustomers", new { Id = addAccount.Casaaccountid });
        }
        //27
        [HttpGet]
        [Route("UpdateCustomerCode")]
        public string UpdateCustomerCode(int Id)
        {
            int count = 0;
            if (Id != null || Id != 0)
            {
                using (var dealOperationApproval = new TheCoreBankingAzureContext())
                {
                    SqlParameter Custcode = new SqlParameter("@ID", Id);

                    count = dealOperationApproval.Database.ExecuteSqlCommand("[Customer].[sp_GenerateCustCode] @ID", Custcode);
                }
            }
            return count.ToString();
        }
    }
}
