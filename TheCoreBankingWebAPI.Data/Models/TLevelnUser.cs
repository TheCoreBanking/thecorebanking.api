﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TLevelnUser
    {
        public int LevelUserId { get; set; }
        public int? LevelId { get; set; }
        public string UserId { get; set; }
        public string Branch { get; set; }
        public string CoyId { get; set; }
        public DateTime? LastUpdate { get; set; }
        public bool? ChecklistItem { get; set; }
        public string LevelNo { get; set; }
    }
}
