﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblSmsChargeExemption
    {
        public int Id { get; set; }
        public string PdCode { get; set; }
    }
}
