﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyDispatchOption
    {
        public int Id { get; set; }
        public string Cpid { get; set; }
        public string AcctNo { get; set; }
        public string DispatchOption { get; set; }
        public bool? Dispatched { get; set; }
        public string DispatchOption1 { get; set; }
        public string DispatchOption2 { get; set; }
    }
}
