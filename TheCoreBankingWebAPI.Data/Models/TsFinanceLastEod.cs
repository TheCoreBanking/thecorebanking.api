﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TsFinanceLastEod
    {
        public int Id { get; set; }
        public DateTime LastEod { get; set; }
    }
}
