﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblStockPrice
    {
        public int Id { get; set; }
        public DateTime? SpDayDate { get; set; }
        public string SpSecurity { get; set; }
        public decimal? SpPrice { get; set; }
        public int? SpTrade { get; set; }
        public int? SpVolume { get; set; }
    }
}
