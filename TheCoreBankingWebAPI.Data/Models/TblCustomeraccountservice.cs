﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCustomeraccountservice
    {
        public TblCustomeraccountservice()
        {
            TblCustomeraccountalertmedium = new HashSet<TblCustomeraccountalertmedium>();
            TblCustomeraccountbankingservice = new HashSet<TblCustomeraccountbankingservice>();
            TblCustomeraccountstmntmedium = new HashSet<TblCustomeraccountstmntmedium>();
        }

        public int Id { get; set; }
        public int Casaaccountid { get; set; }
        public int? Cardtypeid { get; set; }
        public string Nameoncard { get; set; }
        public int? Statementfrequencyid { get; set; }
        public decimal? Minbalance { get; set; }

        public virtual TblAccountcardtype Cardtype { get; set; }
        public virtual TblAccountstmntfreq Statementfrequency { get; set; }
        public virtual ICollection<TblCustomeraccountalertmedium> TblCustomeraccountalertmedium { get; set; }
        public virtual ICollection<TblCustomeraccountbankingservice> TblCustomeraccountbankingservice { get; set; }
        public virtual ICollection<TblCustomeraccountstmntmedium> TblCustomeraccountstmntmedium { get; set; }
    }
}
