﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TTransfer
    {
        public int TransferId { get; set; }
        public int? CreditAssessmentId { get; set; }
        public DateTime? TransferOnDate { get; set; }
        public string TranseredBy { get; set; }
        public int? FromTranseredLevelId { get; set; }
        public int? ToTransferLevelId { get; set; }
        public bool? IsTransfered { get; set; }
        public bool? IsNexted { get; set; }
        public string CoyId { get; set; }
        public string BranchId { get; set; }
    }
}
