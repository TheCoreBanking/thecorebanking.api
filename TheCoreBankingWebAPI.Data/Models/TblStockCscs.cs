﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblStockCscs
    {
        public string CsAccNumber { get; set; }
        public string CsAccDescription { get; set; }
    }
}
