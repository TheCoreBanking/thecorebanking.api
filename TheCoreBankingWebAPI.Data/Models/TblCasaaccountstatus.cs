﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCasaaccountstatus
    {
        public int Accountstatusid { get; set; }
        public string Accountstatusname { get; set; }
    }
}
