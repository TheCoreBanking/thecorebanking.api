﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyDealRates
    {
        public int RateId { get; set; }
        public DateTime MmRateDate { get; set; }
        public string DealId { get; set; }
        public decimal OldRate { get; set; }
        public decimal? NewRate { get; set; }
        public int MmEventsId { get; set; }
        public string Remark { get; set; }
        public decimal? Int2DateOldRate { get; set; }
        public decimal? Int2DatenewRate { get; set; }
        public decimal? Int2DateDiff { get; set; }
        public bool? Approved { get; set; }
        public bool? Disapproved { get; set; }
        public string Approvedby { get; set; }
        public DateTime? DateApproved { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string RevStatus { get; set; }
        public decimal? OldMaturityAmt { get; set; }
        public decimal? NewMaturityAmt { get; set; }
        public decimal? OldIntAmt { get; set; }
        public decimal? NewintAmt { get; set; }
        public decimal? Principal { get; set; }
    }
}
