﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblInstitutiontype
    {
        public TblInstitutiontype()
        {
            TblCustomer = new HashSet<TblCustomer>();
        }

        public int Id { get; set; }
        public string Typename { get; set; }

        public virtual ICollection<TblCustomer> TblCustomer { get; set; }
    }
}
