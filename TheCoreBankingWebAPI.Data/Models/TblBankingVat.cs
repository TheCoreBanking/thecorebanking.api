﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingVat
    {
        public int Id { get; set; }
        public string CustCode { get; set; }
        public string ProductAcctNo { get; set; }
        public decimal? Amount { get; set; }
        public int? CustomerType { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateCreated { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public string Source { get; set; }
        public bool? Approved { get; set; }
    }
}
