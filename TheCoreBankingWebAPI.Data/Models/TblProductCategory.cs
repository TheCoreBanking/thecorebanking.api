﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblProductCategory
    {
        public long Id { get; set; }
        public int? Productcategoryid { get; set; }
        public string Productcategoryname { get; set; }
    }
}
