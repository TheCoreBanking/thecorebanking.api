﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwBankingSweepAccount
    {
        public int Customerid { get; set; }
        public string ProductName { get; set; }
        public string Accountnumber { get; set; }
        public string Name { get; set; }
        public bool Iscurrentaccount { get; set; }
        public int Productid { get; set; }
        public decimal Availablebalance { get; set; }
        public string Accountname { get; set; }
        public decimal? Overdraftamount { get; set; }
    }
}
