﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCustomeremailcontact
    {
        public int Emailcontactid { get; set; }
        public string Email { get; set; }
        public int Customerid { get; set; }
        public bool Active { get; set; }
    }
}
