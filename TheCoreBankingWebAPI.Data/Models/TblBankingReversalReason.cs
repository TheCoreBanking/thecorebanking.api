﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingReversalReason
    {
        public int Id { get; set; }
        public string Reason { get; set; }
        public DateTime? DateSet { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
    }
}
