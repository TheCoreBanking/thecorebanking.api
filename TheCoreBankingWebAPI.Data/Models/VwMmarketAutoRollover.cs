﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwMmarketAutoRollover
    {
        public string MmDealId { get; set; }
        public string CpId { get; set; }
        public string DealId { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public decimal? MaturityAmount { get; set; }
        public int? Tenor { get; set; }
        public decimal? InterestRate { get; set; }
        public string CoyCode { get; set; }
        public string BranchCode { get; set; }
        public string DealCreatedby { get; set; }
        public DateTime? NewEffectiveDate { get; set; }
        public string StaffNo { get; set; }
        public string Productcode { get; set; }
        public string Productname { get; set; }
        public int? Producttypeid { get; set; }
        public int? Productcategoryid { get; set; }
        public int? Principalbalancegl { get; set; }
        public int? Interestincomeexpensegl { get; set; }
        public int? Interestreceivablepayablegl { get; set; }
        public short? Productclassid { get; set; }
        public string Customer { get; set; }
        public decimal? NewPrincipalAmount { get; set; }
        public decimal? PrincipalAmount { get; set; }
        public DateTime? NewMaturityDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public string SettlementAccount { get; set; }
        public int? OriginalTenor { get; set; }
        public bool? AutoRollover { get; set; }
        public int AutoRolloverType { get; set; }
        public decimal? InterestAtMaturity { get; set; }
    }
}
