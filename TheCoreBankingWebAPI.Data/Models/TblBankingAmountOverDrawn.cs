﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingAmountOverDrawn
    {
        public int Id { get; set; }
        public string CustCode { get; set; }
        public string ProdCode { get; set; }
        public string ProductAcctNo { get; set; }
        public decimal? PrincipalAmount { get; set; }
        public decimal? AmountOverDrawn { get; set; }
        public DateTime? TransDate { get; set; }
        public int? DaysRun { get; set; }
        public DateTime? NextPayDate { get; set; }
        public int? MonthArrears { get; set; }
    }
}
