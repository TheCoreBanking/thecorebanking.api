﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwGetCustomerDealProducts
    {
        public string DealId { get; set; }
        public decimal? PrincipalAmount { get; set; }
        public decimal Availablebalance { get; set; }
        public string Accountnumber { get; set; }
        public string Productname { get; set; }
    }
}
