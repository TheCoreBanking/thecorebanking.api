﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblIndustry
    {
        public TblIndustry()
        {
            TblCustomer = new HashSet<TblCustomer>();
        }

        public int Industryid { get; set; }
        public string Name { get; set; }
        public int Sectorid { get; set; }
        public bool Isdeleted { get; set; }

        public virtual TblSector Sector { get; set; }
        public virtual ICollection<TblCustomer> TblCustomer { get; set; }
    }
}
