﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TsBankingStandingOrderHeader
    {
        public int Id { get; set; }
        public string PayeeCustCode { get; set; }
        public string PayeeName { get; set; }
        public string PayeeAcctNo { get; set; }
        public string ReceiverCustCode { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverAcctNo { get; set; }
        public decimal? Amount { get; set; }
        public string Remark { get; set; }
        public DateTime SostartDate { get; set; }
        public bool? Recurring { get; set; }
        public int? Frequency { get; set; }
        public string FrequencyDesc { get; set; }
        public DateTime? SoendDate { get; set; }
        public DateTime? NextActionDate { get; set; }
        public int? OperationId { get; set; }
        public bool? Approved { get; set; }
        public bool? Disapproved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? DateApproved { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public string ApprovalRemark { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Reference { get; set; }
        public bool? Saved { get; set; }
        public bool? Suspended { get; set; }
        public string SuspendedBy { get; set; }
    }
}
