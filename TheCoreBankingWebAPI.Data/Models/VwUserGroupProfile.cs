﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwUserGroupProfile
    {
        public int Id { get; set; }
        public int LevelUserId { get; set; }
        public string LevelNo { get; set; }
        public int LevelsId { get; set; }
        public string LevelName { get; set; }
        public string GroupName { get; set; }
        public string UserName { get; set; }
    }
}
