﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCasapostingstatus
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public bool? CurrentAcct { get; set; }
        public string ProductAcctNo { get; set; }
        public string AccountName { get; set; }
        public bool? Pnd { get; set; }
        public bool? Pnc { get; set; }
        public string Status { get; set; }
        public bool? Approved { get; set; }
        public bool? Disapproved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? DateDisapproved { get; set; }
        public DateTime? DateApproved { get; set; }
        public DateTime? DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string Coycode { get; set; }
        public string Brcode { get; set; }
        public int? OperationId { get; set; }
    }
}
