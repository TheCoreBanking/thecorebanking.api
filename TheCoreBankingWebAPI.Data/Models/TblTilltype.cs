﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblTilltype
    {
        public TblTilltype()
        {
            TblTillmapping = new HashSet<TblTillmapping>();
        }

        public int Id { get; set; }
        public string Type { get; set; }

        public virtual ICollection<TblTillmapping> TblTillmapping { get; set; }
    }
}
