﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBondRegistration
    {
        public int BondId { get; set; }
        public string BondName { get; set; }
        public string BondNo { get; set; }
        public string Staffid { get; set; }
        public bool? Foreignbond { get; set; }
        public bool? IsMyLocalBond { get; set; }
        public decimal? TotalBondValue { get; set; }
        public string MmDealId { get; set; }
        public int? SourceId { get; set; }
        public int? IssuerId { get; set; }
        public string CoyCode { get; set; }
        public string BranchCode { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}
