﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblTitle1
    {
        public int Id { get; set; }
        public bool? PepStatus { get; set; }
        public string Title { get; set; }
    }
}
