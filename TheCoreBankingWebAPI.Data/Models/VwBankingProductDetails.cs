﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwBankingProductDetails
    {
        public string ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public int? ProductTypeId { get; set; }
        public string PdType { get; set; }
        public int? ProductCategoryId { get; set; }
        public string ProductCategoryName { get; set; }
        public string AccountType { get; set; }
        public decimal? PdOpeningBalance { get; set; }
        public int? PrincipalBalanceGl { get; set; }
        public int? InterestIncomeExpenseGl { get; set; }
        public int? InterestReceivablePayableGl { get; set; }
        public int? DormantGl { get; set; }
        public int? CompanyId { get; set; }
        public int? CreatedBy { get; set; }
        public bool? Approved { get; set; }
    }
}
