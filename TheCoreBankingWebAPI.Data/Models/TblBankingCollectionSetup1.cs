﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingCollectionSetup1
    {
        public int Id { get; set; }
        public string CollectionGl { get; set; }
        public decimal Amount { get; set; }
        public decimal MinAmount { get; set; }
        public bool IsActive { get; set; }
        public string BrCode { get; set; }
        public string CoyCode { get; set; }
    }
}
