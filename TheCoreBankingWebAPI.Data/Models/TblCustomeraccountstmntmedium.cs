﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCustomeraccountstmntmedium
    {
        public int Id { get; set; }
        public int Statementmediumid { get; set; }
        public int Customeraccountserviceid { get; set; }

        public virtual TblCustomeraccountservice Customeraccountservice { get; set; }
        public virtual TblAccountstmntmedium Statementmedium { get; set; }
    }
}
