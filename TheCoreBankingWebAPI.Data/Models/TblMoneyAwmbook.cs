﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyAwmbook
    {
        public int Id { get; set; }
        public string DealId { get; set; }
        public string CpId { get; set; }
        public decimal? Amount { get; set; }
        public string Operation { get; set; }
        public decimal? Inflow { get; set; }
        public decimal? Outflow { get; set; }
        public DateTime? OpDate { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
    }
}
