﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingRiskAssessmentResult
    {
        public int RiskResultId { get; set; }
        public string CustomerId { get; set; }
        public string ProAccNo { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public string AveSco { get; set; }
        public string Rating { get; set; }
        public string Remarks { get; set; }
        public string InterestRate { get; set; }
        public string ApprovedBy { get; set; }
        public bool? Approved { get; set; }
        public string AppComment { get; set; }
        public string RiskOffComment { get; set; }
        public string RiskOfficer { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public DateTime? SubmitDate { get; set; }
        public bool? Disapproved { get; set; }
        public bool? Submited { get; set; }
        public int? Opid { get; set; }
    }
}
