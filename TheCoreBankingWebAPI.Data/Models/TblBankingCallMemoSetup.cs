﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingCallMemoSetup
    {
        public int Id { get; set; }
        public string CallMemo { get; set; }
        public string CallMemoDate { get; set; }
    }
}
