﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyDiscountWh
    {
        public int WId { get; set; }
        public string DealId { get; set; }
        public DateTime? Date { get; set; }
        public decimal? Dptday { get; set; }
    }
}
