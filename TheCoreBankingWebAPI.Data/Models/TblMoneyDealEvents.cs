﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyDealEvents
    {
        public int Id { get; set; }
        public DateTime MmDate { get; set; }
        public string DealId { get; set; }
        public int MmEventsId { get; set; }
        public int MPreviousStatus { get; set; }
        public int MCurrentStatus { get; set; }
        public string ActionBy { get; set; }
    }
}
