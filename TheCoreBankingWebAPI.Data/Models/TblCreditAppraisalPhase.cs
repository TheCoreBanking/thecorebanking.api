﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCreditAppraisalPhase
    {
        public int PhaseId { get; set; }
        public string Phase { get; set; }
    }
}
