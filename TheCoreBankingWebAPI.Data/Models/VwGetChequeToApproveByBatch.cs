﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwGetChequeToApproveByBatch
    {
        public string AcctNo { get; set; }
        public string Customercode { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? ClearDate { get; set; }
        public string DrLedger { get; set; }
        public string DestBranch { get; set; }
        public string Companycode { get; set; }
        public string Referenceno { get; set; }
        public string Productcode { get; set; }
        public int? CrLedger { get; set; }
        public int? OperationId { get; set; }
        public int? SourceBr { get; set; }
        public string Narration { get; set; }
        public decimal Amount { get; set; }
        public string StaffId { get; set; }
        public string ChequeNo { get; set; }
        public bool Chargestampduty { get; set; }
    }
}
