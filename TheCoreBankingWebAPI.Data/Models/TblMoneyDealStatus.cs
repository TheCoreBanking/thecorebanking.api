﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyDealStatus
    {
        public int MDealStatusId { get; set; }
        public string MDealStatus { get; set; }
    }
}
