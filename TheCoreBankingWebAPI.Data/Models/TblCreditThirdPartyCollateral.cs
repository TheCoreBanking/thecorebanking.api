﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCreditThirdPartyCollateral
    {
        public int Id { get; set; }
        public string ApplicantCustCode { get; set; }
        public string ThirdPartyCustCode { get; set; }
        public string AccountType { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateCreated { get; set; }
        public string CustomerType { get; set; }
    }
}
