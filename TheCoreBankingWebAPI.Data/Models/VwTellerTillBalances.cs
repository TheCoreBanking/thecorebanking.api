﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwTellerTillBalances
    {
        public string AccountName { get; set; }
        public string AccountId { get; set; }
        public decimal EndDateBalance { get; set; }
        public string BrId { get; set; }
        public int AccountTypeId { get; set; }
        public bool? BrSpecific { get; set; }
        public string SourceBranch { get; set; }
        public string DestinationBranch { get; set; }
        public string SCoyCode { get; set; }
        public DateTime? TransactionDate { get; set; }
    }
}
