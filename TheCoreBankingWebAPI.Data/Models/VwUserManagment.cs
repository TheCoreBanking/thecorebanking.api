﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwUserManagment
    {
        public long Id { get; set; }
        public string StaffName { get; set; }
        public string Locked { get; set; }
        public string Status { get; set; }
    }
}
