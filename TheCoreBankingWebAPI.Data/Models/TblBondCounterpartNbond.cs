﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBondCounterpartNbond
    {
        public int CounterPartyBondId { get; set; }
        public string CounterpartyId { get; set; }
        public int? SeriseId { get; set; }
    }
}
