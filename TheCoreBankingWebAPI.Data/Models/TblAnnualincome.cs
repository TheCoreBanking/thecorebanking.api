﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblAnnualincome
    {
        public TblAnnualincome()
        {
            TblCustomer = new HashSet<TblCustomer>();
            TblEditedcustomerAnnualincome = new HashSet<TblEditedcustomer>();
        }

        public int Id { get; set; }
        public string Range { get; set; }
        public string Accounttype { get; set; }

        public virtual TblEditedcustomer TblEditedcustomerCustomer { get; set; }
        public virtual ICollection<TblCustomer> TblCustomer { get; set; }
        public virtual ICollection<TblEditedcustomer> TblEditedcustomerAnnualincome { get; set; }
    }
}
