﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblChequecharges
    {
        public int Id { get; set; }
        public decimal Percentage { get; set; }
        public int Accountledgerid { get; set; }
        public decimal Maxamount { get; set; }
        public bool Isdiscountcharge { get; set; }
        public bool Isreturncharge { get; set; }
        public bool Isothercharge { get; set; }
        public DateTime Datecreated { get; set; }
        public DateTime? Dateupdated { get; set; }
        public string Branchcode { get; set; }
        public string Companycode { get; set; }
        public bool Approved { get; set; }
        public DateTime? Dateapproved { get; set; }
        public bool Disapproved { get; set; }
        public string Comment { get; set; }
        public string Approvalstatus { get; set; }
        public int? Copyfileid { get; set; }
        public bool? Isnewlycreated { get; set; }
        public bool? Deleteflag { get; set; }
    }
}
