﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCreditErrorlog
    {
        public int Id { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string BranchId { get; set; }
        public string ProductName { get; set; }
        public string ProductAcctNo { get; set; }
        public decimal? Principal { get; set; }
        public decimal? Rate { get; set; }
        public int? Tenor { get; set; }
        public decimal? MaturityAmount { get; set; }
        public decimal? InterestAmount { get; set; }
        public decimal? LastPrincipalPaid { get; set; }
        public decimal? Frequency { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public DateTime? CancelledDate { get; set; }
        public string CancelledBy { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? DateApproved { get; set; }
        public string RealReason { get; set; }
        public string Details { get; set; }
    }
}
