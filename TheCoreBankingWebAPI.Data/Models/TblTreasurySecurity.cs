﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblTreasurySecurity
    {
        public int Id { get; set; }
        public string PdSecurity { get; set; }
        public string PdCode { get; set; }
    }
}
