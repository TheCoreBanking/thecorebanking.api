﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyFees
    {
        public int Id { get; set; }
        public string DealId { get; set; }
        public string CpId { get; set; }
        public decimal? Amount { get; set; }
        public int? TransType { get; set; }
        public DateTime? TransDate { get; set; }
        public string FeeType { get; set; }
        public string Comment { get; set; }
        public string ReversedBy { get; set; }
        public DateTime? DateReversed { get; set; }
        public bool? Reversed { get; set; }
    }
}
