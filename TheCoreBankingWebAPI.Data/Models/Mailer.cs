﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace TheCoreBankingWebAPI.Data.Models
{
    public class Mailer
    {
        public string LoginFunction()
        {
            var html = "<html>" +
"<head>" +
"<title>Login : FinTrak Banking Account Opening Application</title>" +
"</head>" +
"<body style='color:black; font-size:15px; width:750px; height:550px; background-color:white; border:2px solid lightblue;'>" +
"<font face='Helvetica, Arial, sans-serif'>" +
"<div style='position:absolute; height:100px; background:url(/images/standardchartered@2x.jpg)'" +
"width:600px; background-image: no-repeat; padding:30px;'>" +
"<img src='http://fintrakbankonline.azurewebsites.net/images/logo3.png' alt='LBIC' />" +
"</div>" +
"<div style='width:750px; height:500px; padding:30px;'>" +
"<h3 style='text-decoration:underline'>FinTrak Banking Account Opening Application</h3>" +
"<p>Dear {0},</p>" +

"<p>Your account has been profiled with FinTrak Banking Account Opening Application \n\n.Thank you for choosing us.</p>" +
"<p>Below are the details:</a></p>" +
"Account Number: {1}<br>" +
"Time of Activity: {2}<br>" +
"<br/>" +

"<p>If none of your actions on our Core Banking Platform warrants this mail, contact your bank.</p>" +
"<br/>" +
"<p>Thank you,</p>" +
"<p>FinTrak Banking Team</p>" +
"</div>" +
"</body>" +
"</html>";
            return html;
        }

        public void SendMailWithTemplate(string customerName, string accountNumber, DateTime moment, string To, string senderName, string subject)
        {
            string body;
            //Uri urlAddress = new Uri("http://fintrakbankonline.azurewebsites.net/Template/" + template + ".txt");
            //using (var sr = new StreamReader(urlAddress.AbsolutePath))
            //{
            //    body = sr.ReadToEnd();
            //}
            body = LoginFunction();
            senderName = "FinTrak Online Banking";
            var mailSettings = ConfigurationManager.AppSettings["MailAuthUser"];

            string customer = HttpUtility.UrlEncode(customerName).Replace("+", " ");
            string sender = "fintrakmobile@gmail.com";
            string content = string.Format(body, customer, accountNumber, moment.ToShortDateString(), moment.ToShortTimeString());
            string emailSubject = subject;
            var MailHelper = new MailHelper
            {
                Sender = sender, //email.Sender,
                SenderName = senderName,
                Recipient = To,
                RecipientCC = null,
                Subject = emailSubject,
                Body = content
            };
            MailHelper.Send();
        }
    }
}
