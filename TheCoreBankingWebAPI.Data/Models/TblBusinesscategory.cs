﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBusinesscategory
    {
        public TblBusinesscategory()
        {
            TblCustomer = new HashSet<TblCustomer>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<TblCustomer> TblCustomer { get; set; }
    }
}
