﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwCustomerAndGlaccount
    {
        public long Id { get; set; }
        public string AccountName { get; set; }
        public string Accountnumber { get; set; }
        public int Iscurrentaccount { get; set; }
        public int IsGl { get; set; }
        public decimal Availablebalance { get; set; }
        public int? Operationid { get; set; }
    }
}
