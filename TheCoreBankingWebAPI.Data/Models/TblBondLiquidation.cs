﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBondLiquidation
    {
        public int BondLiquidetionId { get; set; }
        public int? SeriesId { get; set; }
        public int? BondId { get; set; }
        public int? BondHouseId { get; set; }
        public decimal? AccruedInterest { get; set; }
        public decimal? CashFlow { get; set; }
        public decimal? SoldAt { get; set; }
        public decimal? CleanPrice { get; set; }
        public decimal? UnamotAmt { get; set; }
        public decimal? DirtyPrice { get; set; }
        public decimal? LiquidetedValue { get; set; }
        public decimal? LiquidatePercentage { get; set; }
        public bool? IsDiscount { get; set; }
        public decimal? PremiumOrDiscount { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string DealId { get; set; }
        public int? OperationId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? ActionOn { get; set; }
        public bool? Approved { get; set; }
        public bool? Disapproved { get; set; }
        public string CoyCode { get; set; }
        public string BranchCode { get; set; }
        public string Comments { get; set; }
        public string DealersComment { get; set; }
    }
}
