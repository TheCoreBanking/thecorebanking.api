﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblOperationalReceivable
    {
        public int Id { get; set; }
        public string DealName { get; set; }
        public string DealId { get; set; }
        public string MmDealId { get; set; }
        public string CpId { get; set; }
        public decimal? Amount { get; set; }
        public string SettleAcct { get; set; }
        public bool? Paid { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public decimal? DebtAmt { get; set; }
        public decimal? CredAmt { get; set; }
        public string Description { get; set; }
        public string UserName { get; set; }
        public int? TransType { get; set; }
        public DateTime? TransDate { get; set; }
    }
}
