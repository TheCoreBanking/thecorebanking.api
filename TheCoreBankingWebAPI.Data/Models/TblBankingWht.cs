﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingWht
    {
        public int Id { get; set; }
        public string CustCode { get; set; }
        public string AccountNo { get; set; }
        public decimal? Amount { get; set; }
        public int? CustomerType { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateCreated { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public string Source { get; set; }
        public bool? Approved { get; set; }
        public int? ProductTypeId { get; set; }
        public bool? Matured { get; set; }
    }
}
