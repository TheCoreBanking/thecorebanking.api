﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblTilltfunction
    {
        public TblTilltfunction()
        {
            TblTillimit = new HashSet<TblTillimit>();
        }

        public int Id { get; set; }
        public string Tillfunction { get; set; }
        public int Functioncode { get; set; }

        public virtual ICollection<TblTillimit> TblTillimit { get; set; }
    }
}
