﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblChecklistPosition
    {
        public int Id { get; set; }
        public string Position { get; set; }
    }
}
