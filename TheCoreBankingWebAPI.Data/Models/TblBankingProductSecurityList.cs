﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingProductSecurityList
    {
        public int Id { get; set; }
        public int PdId { get; set; }
        public int PdSecId { get; set; }
    }
}
