﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TsBankingRepaymentStructure
    {
        public int Id { get; set; }
        public bool? FalseDebitAccount { get; set; }
        public string Pdcode { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public string CreatedBy { get; set; }
    }
}
