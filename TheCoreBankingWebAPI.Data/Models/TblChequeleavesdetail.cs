﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblChequeleavesdetail
    {
        public int Id { get; set; }
        public int Chequebookid { get; set; }
        public int Leafstatus { get; set; }
        public int Leafno { get; set; }
        public DateTime Datecreated { get; set; }
        public string Createdby { get; set; }
        public DateTime? Dateupdated { get; set; }
        public string Updatedby { get; set; }
        public bool Isapproved { get; set; }
        public bool Isstopped { get; set; }
        public bool Disapproved { get; set; }
        public string Comment { get; set; }
        public DateTime? Datedisapproved { get; set; }

        public virtual TblChequebookdetail Chequebook { get; set; }
        public virtual TblChequeleafstatus LeafstatusNavigation { get; set; }
    }
}
