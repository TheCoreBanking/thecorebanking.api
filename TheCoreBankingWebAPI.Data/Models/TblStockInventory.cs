﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblStockInventory
    {
        public int SiId { get; set; }
        public DateTime? SiDate { get; set; }
        public string SkSymbol { get; set; }
        public decimal? UnitPrice { get; set; }
        public int? Volume { get; set; }
        public string SiDirection { get; set; }
        public string TradeNo { get; set; }
        public int MkId { get; set; }
        public string MadeBy { get; set; }
        public int? PortfolioId { get; set; }
    }
}
