﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBondPrimaryBidding
    {
        public int BiddingId { get; set; }
        public decimal? FaceValue { get; set; }
        public decimal? BidRate { get; set; }
        public DateTime? TransDate { get; set; }
        public int? SeriesId { get; set; }
        public string CountpartyId { get; set; }
        public string Miscode { get; set; }
        public string DealId { get; set; }
        public int? BidderId { get; set; }
        public int? OpId { get; set; }
        public int? CoyCode { get; set; }
        public int? BrCode { get; set; }
    }
}
