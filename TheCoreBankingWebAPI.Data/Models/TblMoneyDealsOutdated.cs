﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyDealsOutdated
    {
        public int Id { get; set; }
        public string MmDealId { get; set; }
        public string MmDealName { get; set; }
        public int? Id2 { get; set; }
        public int? SortId { get; set; }
        public int InterBankNonBank { get; set; }
    }
}
