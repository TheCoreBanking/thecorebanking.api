﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwChartOfAccount
    {
        public string AccountId { get; set; }
        public string AccountName { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
        public string AccountType { get; set; }
        public string AccountCategory { get; set; }
        public string AccountGroup { get; set; }
        public string Currency { get; set; }
        public string Company { get; set; }
        public bool? BrSpecific { get; set; }
        public int? AccountStatus { get; set; }
        public string CostCentre { get; set; }
        public string CoyId { get; set; }
        public DateTime? DateCreated { get; set; }
        public string FscaptionCode { get; set; }
    }
}
