﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBondCouponDate
    {
        public int BondAmortId { get; set; }
        public decimal? DlyCoup { get; set; }
        public decimal? DlyEffRate { get; set; }
        public decimal? DlyAmort { get; set; }
        public DateTime? CouponDate { get; set; }
        public DateTime? NextCouponDate { get; set; }
        public string DealId { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
    }
}
