﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyInterests9History
    {
        public int Id { get; set; }
        public DateTime? DDate { get; set; }
        public string DealId1 { get; set; }
        public decimal? IPrincipal { get; set; }
        public decimal? InterestRate { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public decimal? Iptday { get; set; }
        public decimal? IAccrualTodate { get; set; }
        public decimal? Iptdate { get; set; }
        public DateTime? LastEod { get; set; }
        public string EmpId { get; set; }
        public decimal? InterestAmount { get; set; }
        public DateTime? BackupDate { get; set; }
        public string Batchref { get; set; }
    }
}
