﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblLoanProvision
    {
        public int Id { get; set; }
        public string CustCode { get; set; }
        public string AccountNo { get; set; }
        public DateTime? LastRepaymentDate { get; set; }
        public string RelatedAccount { get; set; }
        public decimal? LoanBal { get; set; }
        public decimal? RelatedAccountBal { get; set; }
        public DateTime? LastCreditDate { get; set; }
        public int? DaysOverDue { get; set; }
        public decimal? MonthlyInstallment { get; set; }
        public int? AgeOfDefault { get; set; }
        public decimal? Minval { get; set; }
        public decimal? Maxval { get; set; }
        public string Classification { get; set; }
        public string SubClassification { get; set; }
        public decimal? ProvisionRate { get; set; }
        public decimal? ProvisionAmount { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string CustomerName { get; set; }
        public string Frequency { get; set; }
        public string Schedule { get; set; }
        public DateTime? CurrentDate { get; set; }
        public string RelationshipOfficer1 { get; set; }
        public string RelationshipOfficer2 { get; set; }
        public string BranchId { get; set; }
        public decimal? MaturityAmount { get; set; }
        public decimal? InterestAmount { get; set; }
        public decimal? ApprovedAmount { get; set; }
        public decimal? PrincipalRepayment { get; set; }
        public decimal? InterestPayment { get; set; }
        public decimal? TotalRepayment { get; set; }
        public DateTime? EffectiveDate { get; set; }
    }
}
