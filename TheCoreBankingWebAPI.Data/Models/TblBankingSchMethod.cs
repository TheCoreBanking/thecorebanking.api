﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingSchMethod
    {
        public int Id { get; set; }
        public string SchMethod { get; set; }
        public string Description { get; set; }
        public bool? Schactive { get; set; }
    }
}
