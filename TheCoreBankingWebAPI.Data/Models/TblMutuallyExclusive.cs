﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMutuallyExclusive
    {
        public int Id { get; set; }
        public bool Endofday { get; set; }
        public bool Startofday { get; set; }
    }
}
