﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblOperationModule
    {
        public int ModuleId { get; set; }
        public string ModuleOperation { get; set; }
        public int? OperationId { get; set; }
    }
}
