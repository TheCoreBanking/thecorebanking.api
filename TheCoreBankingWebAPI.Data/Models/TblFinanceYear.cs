﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblFinanceYear
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public DateTime Startyear { get; set; }
        public DateTime Endyear { get; set; }
    }
}
