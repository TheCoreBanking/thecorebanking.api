﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblTreasuryErrorlog
    {
        public int Id { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string BranchId { get; set; }
        public string ProductName { get; set; }
        public string ProductAcctNo { get; set; }
        public decimal? Principal { get; set; }
        public decimal? Rate { get; set; }
        public int? Tenor { get; set; }
        public decimal? DiscountedValue { get; set; }
        public decimal? Discount { get; set; }
        public decimal? EffectiveYield { get; set; }
        public DateTime? PostingDate { get; set; }
        public DateTime? ValueDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public string CancelledBy { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? DateApproved { get; set; }
        public string Reason { get; set; }
        public string Details { get; set; }
    }
}
