﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingMemberActivity
    {
        public int Id { get; set; }
        public string StaffNo { get; set; }
        public string GroupReference { get; set; }
        public string CreateDate { get; set; }
        public string CheckList { get; set; }
        public string Details { get; set; }
        public string Actionpoint { get; set; }
        public string ActionDueDate { get; set; }
        public string ActivityRef { get; set; }
        public string CustCode { get; set; }
        public bool? Approved { get; set; }
        public bool? Disapproved { get; set; }
        public DateTime? DateApproved { get; set; }
        public string ApprovedBy { get; set; }
        public string Approvalcomment { get; set; }
        public bool? SupervisorApproved { get; set; }
        public bool? SupervisorDisapproved { get; set; }
        public DateTime? SupervisorDateApproved { get; set; }
        public string Supervisor { get; set; }
        public string SupervisorComment { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public string Feedback { get; set; }
        public DateTime? FeedbackDate { get; set; }
        public string Customer { get; set; }
        public string Email { get; set; }
        public string TelephoneNo { get; set; }
        public string Occupation { get; set; }
        public string Address { get; set; }
        public string ApprovalStatus { get; set; }
        public bool? IsExistingCustomer { get; set; }
    }
}
