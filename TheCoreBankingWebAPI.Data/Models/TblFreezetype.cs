﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblFreezetype
    {
        public TblFreezetype()
        {
            TblAccountfreeze = new HashSet<TblAccountfreeze>();
        }

        public int Id { get; set; }
        public string FreezeType { get; set; }
        public bool? Active { get; set; }

        public virtual ICollection<TblAccountfreeze> TblAccountfreeze { get; set; }
    }
}
