﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VmListrestructureOld
    {
        public int Id { get; set; }
        public string CustName { get; set; }
        public string CustCode { get; set; }
        public string ProductName { get; set; }
        public string ProdNo { get; set; }
        public string NewMaturitydate { get; set; }
        public string NewTenor { get; set; }
        public string DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string OutstandingPrincipal { get; set; }
        public string Operation { get; set; }
    }
}
