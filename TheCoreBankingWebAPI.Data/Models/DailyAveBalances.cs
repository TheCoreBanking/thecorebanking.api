﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class DailyAveBalances
    {
        public int Id { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public bool? IsCurrent { get; set; }
        public DateTime? DateAsAt { get; set; }
        public int? Days { get; set; }
        public string BrCode { get; set; }
        public string ProductGroup { get; set; }
        public decimal? AveBalance { get; set; }
        public decimal? AveDebitBalance { get; set; }
        public decimal? AveCreditBalance { get; set; }
        public string DayOfWeek { get; set; }
        public string PdCode { get; set; }
        public string CustCode { get; set; }
        public string PrincipalGl { get; set; }
        public string CurrencyCode { get; set; }
        public string MisCode { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public string AccountOfficer { get; set; }
        public string ProductName { get; set; }
        public decimal? Rate { get; set; }
        public decimal? DailyInterest { get; set; }
        public string MonthCount { get; set; }
        public decimal? AccountBalance { get; set; }
        public decimal? ActualInterest { get; set; }
        public bool IsDelete { get; set; }
        public string OldAccountNo { get; set; }
    }
}
