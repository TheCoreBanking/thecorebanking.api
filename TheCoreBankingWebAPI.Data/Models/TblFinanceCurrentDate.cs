﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblFinanceCurrentDate
    {
        public DateTime CurrentDate { get; set; }
        public int Id { get; set; }
    }
}
