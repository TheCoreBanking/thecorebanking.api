﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblState
    {
        public TblState()
        {
            TblBranch = new HashSet<TblBranch>();
            TblCustomerStateoforigin = new HashSet<TblCustomer>();
            TblCustomerWorkstateNavigation = new HashSet<TblCustomer>();
            TblCustomeraddress = new HashSet<TblCustomeraddress>();
            TblInsertcustomerprofile = new HashSet<TblInsertcustomerprofile>();
        }

        public int Stateid { get; set; }
        public string Statename { get; set; }
        public int? Lgaid { get; set; }
        public int? Countryid { get; set; }
        public int? Regionid { get; set; }
        public string Createdby { get; set; }
        public DateTime? Datetimecreated { get; set; }

        public virtual TblCountry Country { get; set; }
        public virtual TblStateandlga Lga { get; set; }
        public virtual TblRegion Region { get; set; }
        public virtual ICollection<TblBranch> TblBranch { get; set; }
        public virtual ICollection<TblCustomer> TblCustomerStateoforigin { get; set; }
        public virtual ICollection<TblCustomer> TblCustomerWorkstateNavigation { get; set; }
        public virtual ICollection<TblCustomeraddress> TblCustomeraddress { get; set; }
        public virtual ICollection<TblInsertcustomerprofile> TblInsertcustomerprofile { get; set; }
    }
}
