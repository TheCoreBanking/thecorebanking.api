﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyDiscounts9
    {
        public DateTime? DDate { get; set; }
        public string DealId { get; set; }
        public decimal? Dptday { get; set; }
        public decimal? Dptdate { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Tenor { get; set; }
        public DateTime? MaturityDate { get; set; }
        public decimal? EarnedDiscount { get; set; }
        public decimal? Uediscount { get; set; }
        public decimal? Days2maturity { get; set; }
        public DateTime? CurrentDate { get; set; }
        public string EmpId { get; set; }
        public bool? CollectDiscount { get; set; }
        public bool? Matured { get; set; }
    }
}
