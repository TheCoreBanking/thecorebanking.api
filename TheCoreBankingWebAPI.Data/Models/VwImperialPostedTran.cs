﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwImperialPostedTran
    {
        public string Description { get; set; }
        public decimal? DebitAmt { get; set; }
        public decimal? CreditAmt { get; set; }
        public string AccountId { get; set; }
        public string PostedBy { get; set; }
        public string PostingTime { get; set; }
        public string ApprovedBy { get; set; }
        public string SourceBranch { get; set; }
        public string DestinationBranch { get; set; }
        public string BatchRef { get; set; }
        public int Id { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string Legtype { get; set; }
        public DateTime? ValueDate { get; set; }
        public string Accountname { get; set; }
        public string Oldproductaccountnumber1 { get; set; }
        public string Oldproductaccountnumber2 { get; set; }
        public string Oldproductaccountnumber3 { get; set; }
    }
}
