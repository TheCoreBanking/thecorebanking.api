﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblDirectorInformation
    {
        public int Id { get; set; }
        public string Bvn { get; set; }
        public decimal? PercentageShare { get; set; }
        public string Position { get; set; }
        public string CompanyId { get; set; }
        public string FullName { get; set; }
    }
}
