﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyOperationalDeals
    {
        public int Id { get; set; }
        public string DealId { get; set; }
        public string MmDealId { get; set; }
        public string CpId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public decimal? DiscountedValue { get; set; }
        public decimal? MaturityAmount { get; set; }
        public decimal? PrincipalAmount { get; set; }
        public decimal? NewPrincipal { get; set; }
        public decimal? InterestAmount { get; set; }
        public decimal? Discount { get; set; }
        public decimal? AmtCollected { get; set; }
        public decimal? PenalAmount { get; set; }
        public decimal? AmtReInvested { get; set; }
        public int? Tenor { get; set; }
        public decimal? InterestRate { get; set; }
        public decimal? NewInterestRate { get; set; }
        public decimal? EffectiveYield { get; set; }
        public string SettlementAccount { get; set; }
        public int? MDealStatusId { get; set; }
        public int? MDealEventId { get; set; }
        public string NewDealId { get; set; }
        public string CoyCode { get; set; }
        public string BranchCode { get; set; }
        public string DealCreatedby { get; set; }
        public bool? DealApproved { get; set; }
        public string DealApprovedby { get; set; }
        public bool? DealDisapproved { get; set; }
        public DateTime? DateApproved { get; set; }
        public string Remark { get; set; }
        public int? OperationId { get; set; }
        public bool? DealSlip { get; set; }
        public bool? Cert { get; set; }
        public bool? TransactLetter { get; set; }
        public string Comment { get; set; }
        public string LetterUpload { get; set; }
        public string Upload { get; set; }
        public bool? PendingDeals { get; set; }
        public bool? InterBank { get; set; }
    }
}
