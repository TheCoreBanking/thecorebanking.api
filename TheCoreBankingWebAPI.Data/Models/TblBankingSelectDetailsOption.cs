﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingSelectDetailsOption
    {
        public int DetailsOptionId { get; set; }
        public int DetailsTypeId { get; set; }
        public int DetailsId { get; set; }
        public int OptionId { get; set; }
        public string OptionName { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }
    }
}
