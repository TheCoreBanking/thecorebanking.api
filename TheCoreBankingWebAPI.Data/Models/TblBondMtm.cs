﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBondMtm
    {
        public int Mtmid { get; set; }
        public string DealId { get; set; }
        public string ProductName { get; set; }
        public bool? IsBond { get; set; }
        public decimal? BoughtAt { get; set; }
        public decimal? FaceValue { get; set; }
        public decimal? BidPrice { get; set; }
        public decimal? OfferPrice { get; set; }
        public decimal? MidPrice { get; set; }
        public decimal? FairAmount { get; set; }
        public DateTime? MarkDate { get; set; }
        public bool? IsApproved { get; set; }
        public bool? IsDisapproved { get; set; }
        public string ActionBy { get; set; }
        public DateTime? ActionOn { get; set; }
        public int? ProductdTypeId { get; set; }
        public bool? IsCurrentlyActive { get; set; }
        public DateTime? TransactionDate { get; set; }
        public int? OperationId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public decimal? PostedValue { get; set; }
        public bool? GainOrLoss { get; set; }
    }
}
