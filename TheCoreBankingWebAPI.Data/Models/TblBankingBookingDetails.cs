﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingBookingDetails
    {
        public int Id { get; set; }
        public string CustCode { get; set; }
        public string ProductAcctNo { get; set; }
        public string ProdCode { get; set; }
        public int? OperationId { get; set; }
        public bool? Approved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? DateApproved { get; set; }
        public bool? Disapproved { get; set; }
        public decimal? Amount { get; set; }
        public string AmountInWords { get; set; }
        public string AccountNo { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public string ChequeNo { get; set; }
        public string Bank { get; set; }
        public string CreatedBy { get; set; }
        public string StaffNo { get; set; }
        public string Ref { get; set; }
    }
}
