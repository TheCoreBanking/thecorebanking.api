﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCottransaction
    {
        public int Id { get; set; }
        public string Customercode { get; set; }
        public string Accountnumber { get; set; }
        public int? OperationId { get; set; }
        public decimal? Cotamount { get; set; }
        public string Companycode { get; set; }
        public string Branchcode { get; set; }
        public bool? SameCustomer { get; set; }
        public bool? SameAccount { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string Narration { get; set; }
        public string PostedBy { get; set; }
        public bool? IsCharged { get; set; }
        public int? Productid { get; set; }
        public int? CutOffDay { get; set; }
        public string ReferenceId { get; set; }
        public bool? Internal { get; set; }
    }
}
