﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class UsertoBranch
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string BrName { get; set; }
    }
}
