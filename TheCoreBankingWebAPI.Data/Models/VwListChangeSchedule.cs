﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwListChangeSchedule
    {
        public int Id { get; set; }
        public string CustName { get; set; }
        public string CustCode { get; set; }
        public string ProductName { get; set; }
        public string ProdNo { get; set; }
        public DateTime? DateCreated { get; set; }
        public decimal? OutstandingPrincipal { get; set; }
    }
}
