﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblClearingoptions
    {
        public TblClearingoptions()
        {
            TblOutwardbankcheque = new HashSet<TblOutwardbankcheque>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<TblOutwardbankcheque> TblOutwardbankcheque { get; set; }
    }
}
