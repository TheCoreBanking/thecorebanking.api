﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwOfficerComment
    {
        public int Id { get; set; }
        public int? CreditAssessmentId { get; set; }
        public int? LevelId { get; set; }
        public string UserId { get; set; }
        public string Comment { get; set; }
        public string AdditionalInformation { get; set; }
        public DateTime? CommontOn { get; set; }
        public string UserName { get; set; }
    }
}
