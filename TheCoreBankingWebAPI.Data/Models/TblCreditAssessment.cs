﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCreditAssessment
    {
        public int CreditAssessmentId { get; set; }
        public int? FactorId { get; set; }
        public int? FactorIndexId { get; set; }
        public int? IndexHeadId { get; set; }
        public int? IndexId { get; set; }
        public decimal? IndexScore { get; set; }
        public string CustomerId { get; set; }
        public string AccountNo { get; set; }
        public bool? IsDone { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
