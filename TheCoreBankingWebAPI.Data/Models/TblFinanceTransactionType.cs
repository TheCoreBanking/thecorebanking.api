﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblFinanceTransactionType
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
