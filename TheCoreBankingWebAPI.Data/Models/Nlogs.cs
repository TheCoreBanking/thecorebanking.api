﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class Nlogs
    {
        public int Id { get; set; }
        public DateTime Datetime { get; set; }
        public string Message { get; set; }
        public string Lvl { get; set; }
    }
}
