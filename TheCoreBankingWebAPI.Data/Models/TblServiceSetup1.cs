﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblServiceSetup1
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public bool Status { get; set; }
    }
}
