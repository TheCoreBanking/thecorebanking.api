﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingSameDayReversal
    {
        public int Id { get; set; }
        public string CustCode { get; set; }
        public string ProdNo { get; set; }
        public int? OperationId { get; set; }
        public string Remark { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateCreated { get; set; }
        public bool? Approved { get; set; }
        public bool? Disapproved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? DateApproved { get; set; }
        public int? TransId { get; set; }
        public string ApprovalRemark { get; set; }
        public string BatchRef { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
    }
}
