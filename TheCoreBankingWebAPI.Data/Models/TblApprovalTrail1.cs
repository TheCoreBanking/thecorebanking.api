﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblApprovalTrail1
    {
        public int Id { get; set; }
        public int? UniqueOpId { get; set; }
        public string TransactionTypeId { get; set; }
        public string ApprovingStaffId { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public string Comment { get; set; }
        public string ApprovingStaffEmail { get; set; }
    }
}
