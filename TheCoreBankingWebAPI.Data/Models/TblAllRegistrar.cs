﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblAllRegistrar
    {
        public int RegistrarId { get; set; }
        public string RegistrarName { get; set; }
        public string RegistrarAddress { get; set; }
    }
}
