﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class CoaType
    {
        public string AccountId { get; set; }
        public string AccountName { get; set; }
        public int AccountTypeId { get; set; }
        public int AccountCategoryId { get; set; }
        public int AccountGroupId { get; set; }
        public string CoyId { get; set; }
        public string BrId { get; set; }
        public int? CurrCode { get; set; }
        public int? Costcode { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
    }
}
