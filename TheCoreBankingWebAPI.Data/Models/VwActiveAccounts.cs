﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwActiveAccounts
    {
        public string Accountnumber { get; set; }
        public string AccountName { get; set; }
        public int? BranchId { get; set; }
        public string BranchName { get; set; }
        public string Customercode { get; set; }
        public string ProductCode { get; set; }
        public string Productname { get; set; }
        public DateTime? Datetimecreated { get; set; }
        public DateTime? Effectivedate { get; set; }
        public string LinkedAcct { get; set; }
        public string Name { get; set; }
    }
}
