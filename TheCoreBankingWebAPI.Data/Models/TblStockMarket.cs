﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblStockMarket
    {
        public int MkId { get; set; }
        public string MkName { get; set; }
        public int CurrCode { get; set; }
    }
}
