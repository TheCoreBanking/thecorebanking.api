﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMaturitySetup
    {
        public int Id { get; set; }
        public int? MaturityMin { get; set; }
        public int? MaturityMax { get; set; }
        public string BucketName { get; set; }
        public string Coycode { get; set; }
        public string Brcode { get; set; }
    }
}
