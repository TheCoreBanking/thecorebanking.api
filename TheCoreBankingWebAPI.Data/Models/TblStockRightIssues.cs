﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblStockRightIssues
    {
        public int Id { get; set; }
        public decimal Ratio { get; set; }
        public decimal? Minimum { get; set; }
        public string Symbol { get; set; }
        public DateTime Openingdate { get; set; }
        public string Tradingwindow { get; set; }
        public string Markettype { get; set; }
        public DateTime Effectivedate { get; set; }
        public DateTime Closuredate { get; set; }
        public string Enteredby { get; set; }
        public bool? Approved { get; set; }
        public string Approvedby { get; set; }
    }
}
