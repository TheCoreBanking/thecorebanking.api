﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwRiskRatingOld
    {
        public string Name { get; set; }
        public string ProductName { get; set; }
        public string ProductAcctNo { get; set; }
        public string CurrentAcct { get; set; }
        public DateTime? EffectDate { get; set; }
        public DateTime? TerminalDate { get; set; }
        public string CustCode { get; set; }
        public string BranchId { get; set; }
        public string Miscode { get; set; }
        public string CoyCode { get; set; }
        public int? Producttypeid { get; set; }
        public string Officer1 { get; set; }
        public string Officer2 { get; set; }
        public string BrAddress { get; set; }
        public string BrLocation { get; set; }
        public string BrState { get; set; }
        public string BrManager { get; set; }
        public int? OperationId { get; set; }
        public int Id { get; set; }
        public decimal? Principal { get; set; }
        public string PdType { get; set; }
        public bool? TempRisk { get; set; }
    }
}
