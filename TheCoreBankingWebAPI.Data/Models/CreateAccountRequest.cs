﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheCoreBankingWebAPI.Data.Models
{
    public class CreateAccountRequest
    {
        public string bvn { get; set; }
        public string email { get; set; }
        public string fullname { get; set; }
        public string phonenumber { get; set; }

    }
}
