﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwYearWarehousing
    {
        public DateTime TransactionDate { get; set; }
        public string AccountId { get; set; }
        public string Description { get; set; }
        public decimal? DebitAmt { get; set; }
        public decimal? CreditAmt { get; set; }
        public decimal? Balance { get; set; }
        public int AccountTypeId { get; set; }
        public int AccountGroupId { get; set; }
    }
}
