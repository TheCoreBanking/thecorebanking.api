﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingCurrencyList
    {
        public int Id { get; set; }
        public int? PdId { get; set; }
        public int? CurrCode { get; set; }
        public string CurrName { get; set; }
    }
}
