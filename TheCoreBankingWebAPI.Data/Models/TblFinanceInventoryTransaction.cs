﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblFinanceInventoryTransaction
    {
        public int Id { get; set; }
        public string Symbol { get; set; }
        public int? Qty { get; set; }
        public int? Price { get; set; }
        public string Description { get; set; }
        public DateTime DateInsert { get; set; }
    }
}
