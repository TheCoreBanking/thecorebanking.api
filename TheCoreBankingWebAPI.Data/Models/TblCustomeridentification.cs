﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCustomeridentification
    {
        public int Identificationid { get; set; }
        public int Customerid { get; set; }
        public string Identificationno { get; set; }
        public int? Identificationmodeid { get; set; }
        public string Issueplace { get; set; }
        public string Issueauthority { get; set; }

        public virtual TblCustomeridentimodetype Identificationmode { get; set; }
    }
}
