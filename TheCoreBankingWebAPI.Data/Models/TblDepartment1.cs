﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblDepartment1
    {
        public long Id { get; set; }
        public string CoyId { get; set; }
        public string Department { get; set; }
        public string Remark { get; set; }
        public string DeptCode { get; set; }
    }
}
