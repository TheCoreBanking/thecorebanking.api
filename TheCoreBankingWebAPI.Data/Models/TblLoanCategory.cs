﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblLoanCategory
    {
        public int Id { get; set; }
        public string MainCategory { get; set; }
        public string Category { get; set; }
        public int? MinRange { get; set; }
        public int? MaxRange { get; set; }
        public decimal? MinRecovery { get; set; }
        public decimal? MaxRecovery { get; set; }
        public string Classification { get; set; }
        public string SubClassification { get; set; }
        public decimal? LoanProvisionRate { get; set; }
        public int? CreditRating { get; set; }
        public bool? AllProduct { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public int? ClassId { get; set; }
    }
}
