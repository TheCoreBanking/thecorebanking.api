﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwLoanAttachment
    {
        public int Id { get; set; }
        public int? Tenor { get; set; }
        public decimal? Rate { get; set; }
        public int? Moratorium { get; set; }
        public bool? Approved { get; set; }
        public int? TenorMode { get; set; }
        public string CurrentAcct { get; set; }
        public decimal? FeeCharge { get; set; }
        public int? FeeFrequency { get; set; }
        public decimal? PrincipalFrequency { get; set; }
        public string PrincipalFreqType { get; set; }
        public string RelationshipManager { get; set; }
        public string RelationshipManagerDept { get; set; }
        public string RelationshipOfficer { get; set; }
        public string CreditAccountNo { get; set; }
        public string AppraisalAttachmentRemark { get; set; }
        public string AttachedBy { get; set; }
        public string AttachedDate { get; set; }
    }
}
