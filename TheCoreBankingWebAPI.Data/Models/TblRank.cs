﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblRank
    {
        public int Id { get; set; }
        public string Rank { get; set; }
        public string BrId { get; set; }
    }
}
