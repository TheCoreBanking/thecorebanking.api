﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwMmarketCustomerBalance
    {
        public string MmDealId { get; set; }
        public string DealId { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public decimal? DiscountedValue { get; set; }
        public decimal? MaturityAmount { get; set; }
        public decimal? PrincipalAmount { get; set; }
        public int? Tenor { get; set; }
        public decimal? InterestRate { get; set; }
        public string CoyCode { get; set; }
        public string BranchCode { get; set; }
        public string CpName { get; set; }
        public DateTime? MaturityDate { get; set; }
        public int? Productcategoryid { get; set; }
        public string CpId { get; set; }
        public string MmDealName { get; set; }
        public decimal? TaxCharge { get; set; }
        public decimal? InterestAmount { get; set; }
        public string UnitCode { get; set; }
        public int? Producttypeid { get; set; }
        public short? ProductBehaviourid { get; set; }
    }
}
