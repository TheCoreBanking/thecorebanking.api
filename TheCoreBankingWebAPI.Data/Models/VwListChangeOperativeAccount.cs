﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwListChangeOperativeAccount
    {
        public int Id { get; set; }
        public string CustName { get; set; }
        public string CustCode { get; set; }
        public string ProductName { get; set; }
        public string ProductAccountNo { get; set; }
        public string OldOperativeAccount { get; set; }
        public decimal? Amount { get; set; }
        public string NewOperativeAccount { get; set; }
        public string Comment { get; set; }
    }
}
