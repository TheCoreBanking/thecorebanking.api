﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingMarketingGroup
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
        public string GroupHeadNo { get; set; }
        public string GroupReference { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public string Class { get; set; }
        public string SupStaffNo { get; set; }
        public bool? Approved { get; set; }
        public bool? Disapproved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? DateApproved { get; set; }
    }
}
