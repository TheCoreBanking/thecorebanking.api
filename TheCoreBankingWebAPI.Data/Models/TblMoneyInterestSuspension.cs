﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyInterestSuspension
    {
        public int Id { get; set; }
        public string DealId { get; set; }
        public string MmDealId { get; set; }
        public string DealName { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public decimal? Rate { get; set; }
        public int? Tenor { get; set; }
        public decimal? PrincipalAamt { get; set; }
        public decimal? MaturityAmt { get; set; }
        public decimal? InterestAmt { get; set; }
        public decimal? InterestAccrued2Date { get; set; }
        public string Upload { get; set; }
        public string Reason { get; set; }
        public DateTime? SuspensionDate { get; set; }
        public string CpId { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public int? OperationId { get; set; }
        public bool? Approved { get; set; }
        public bool? DisApproved { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public string ApprovalComment { get; set; }
        public string SettlAcct { get; set; }
        public string ApprovedBy { get; set; }
        public int? NewTenor { get; set; }
        public DateTime? NewMaturityDate { get; set; }
        public int? DaysRun { get; set; }
    }
}
