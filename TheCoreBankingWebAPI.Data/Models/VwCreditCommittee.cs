﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwCreditCommittee
    {
        public int Id { get; set; }
        public int LoanId { get; set; }
        public string ProductAcctNo { get; set; }
        public string ProductName { get; set; }
        public string Name { get; set; }
        public decimal? Principal { get; set; }
        public int? Producttypeid { get; set; }
        public string BranchId { get; set; }
        public string CurrentAcct { get; set; }
        public string CustCode { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? TerminalDate { get; set; }
        public string AppraisalAttachmentRemark { get; set; }
        public string AppraisalAttachmentTitle { get; set; }
        public string AttachedBy { get; set; }
    }
}
