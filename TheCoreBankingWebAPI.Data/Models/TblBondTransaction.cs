﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBondTransaction
    {
        public int BondtransId { get; set; }
        public int? BondId { get; set; }
        public int? BSeriseId { get; set; }
        public string BOriginalHolder { get; set; }
        public string BNewHolder { get; set; }
        public bool? IsSource { get; set; }
        public DateTime? BTransDate { get; set; }
        public string Transactionby { get; set; }
        public DateTime? BMaturityDate { get; set; }
        public int? BondVolumBought { get; set; }
        public int? BondVolumSold { get; set; }
        public decimal? BAmount { get; set; }
        public bool? Bond { get; set; }
        public bool? IsFunded { get; set; }
        public string BApprovedby { get; set; }
        public DateTime? BApprovedon { get; set; }
        public bool? Approved { get; set; }
        public bool? Disapproved { get; set; }
        public bool? Pending { get; set; }
        public string CpId { get; set; }
        public string DealId { get; set; }
        public string RefNo { get; set; }
    }
}
