﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBondTransactionCheck
    {
        public int TransactionCheckId { get; set; }
        public DateTime? TransactionDate { get; set; }
        public DateTime? SettlementDate { get; set; }
        public string DealId { get; set; }
        public bool? IsConsidered { get; set; }
    }
}
