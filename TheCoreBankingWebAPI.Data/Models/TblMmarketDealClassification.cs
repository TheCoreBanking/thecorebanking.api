﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMmarketDealClassification
    {
        public int Id { get; set; }
        public string DealClassification { get; set; }
        public string Details { get; set; }
    }
}
