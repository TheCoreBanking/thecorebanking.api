﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblOperationApprovalCommittee
    {
        public int Id { get; set; }
        public int? OperationId { get; set; }
        public string ApprovingAuthority { get; set; }
        public string StaffId { get; set; }
        public decimal? CreditMinMgt { get; set; }
        public decimal? CreditMinBoard { get; set; }
        public int? Department { get; set; }
        public int? ApprovingLevel { get; set; }
        public string CompanyCode { get; set; }
        public string BranchCode { get; set; }
        public string Miscode { get; set; }
        public string CommitteeType { get; set; }
        public decimal? CummulativeAmt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? BoardNo { get; set; }
        public int? MgtNo { get; set; }
        public decimal? CreditMaxComteeAmt { get; set; }
        public decimal? CreditMaxBoardAmt { get; set; }
        public int? MgtNoApproval { get; set; }
        public int? BoardNoApproval { get; set; }
        public bool? Approved { get; set; }
        public bool? Disapproved { get; set; }
        public string ApprovedBy { get; set; }
        public string Comment { get; set; }
        public decimal? MinAmt { get; set; }
        public decimal? MaxAmt { get; set; }
    }
}
