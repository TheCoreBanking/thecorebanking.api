﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwSmsCharges
    {
        public string AccountNumber { get; set; }
        public string ProductCode { get; set; }
        public int? BranchId { get; set; }
        public int? Companyid { get; set; }
        public int? Drledger { get; set; }
        public DateTime DateCreated { get; set; }
        public string Customercode { get; set; }
        public decimal? EndBalance { get; set; }
    }
}
