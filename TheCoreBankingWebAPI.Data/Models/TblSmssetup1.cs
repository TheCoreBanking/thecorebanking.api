﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblSmssetup1
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public decimal SmsAmount { get; set; }
        public string SmsCrLedger { get; set; }
        public string SmsCrLedgerIncome { get; set; }
        public bool IsActive { get; set; }
        public int Apiid { get; set; }
    }
}
