﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class IndexOfRiskAssessment
    {
        public int Iraid { get; set; }
        public decimal? IraavScoMax { get; set; }
        public decimal? IraavScoMin { get; set; }
        public string Iraration { get; set; }
        public string Iraremarks { get; set; }
        public string IrainterestRate { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public int? CreditTypeId { get; set; }
        public string CreditGrades { get; set; }
        public string CreditGradeDefinitions { get; set; }
        public string PdCode { get; set; }
        public string CreditGradeDesc { get; set; }
    }
}
