﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblFreezedatetype
    {
        public TblFreezedatetype()
        {
            TblAccountfreeze = new HashSet<TblAccountfreeze>();
        }

        public int Id { get; set; }
        public string DateType { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public virtual ICollection<TblAccountfreeze> TblAccountfreeze { get; set; }
    }
}
