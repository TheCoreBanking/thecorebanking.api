﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCustomeraddresstype
    {
        public TblCustomeraddresstype()
        {
            TblCustomeraddress = new HashSet<TblCustomeraddress>();
        }

        public short Addresstypeid { get; set; }
        public string AddressTypeName { get; set; }

        public virtual ICollection<TblCustomeraddress> TblCustomeraddress { get; set; }
    }
}
