﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblDesignation
    {
        public long Id { get; set; }
        public string Designation { get; set; }
        public string DesignationCode { get; set; }
    }
}
