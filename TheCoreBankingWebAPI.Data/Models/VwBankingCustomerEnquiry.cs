﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwBankingCustomerEnquiry
    {
        public string Customercode { get; set; }
        public string Ctname { get; set; }
        public DateTime? Datetimecreated { get; set; }
        public string CustomerName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string CustAddress { get; set; }
        public int? Branchid { get; set; }
        public int? Companyid { get; set; }
        public string StaffName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int AccountType { get; set; }
    }
}
