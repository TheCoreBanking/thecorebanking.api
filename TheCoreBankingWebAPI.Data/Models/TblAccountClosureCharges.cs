﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblAccountClosureCharges
    {
        public int Id { get; set; }
        public decimal? Amount { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateCreated { get; set; }
        public string CoyCode { get; set; }
        public string CrLedger { get; set; }
    }
}
