﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TsStandingOrderFtt
    {
        public int Id { get; set; }
        public DateTime? TransactionDate { get; set; }
        public int? TransactionType { get; set; }
        public string Description { get; set; }
        public string Ref { get; set; }
        public double? DebitAmt { get; set; }
        public double? CreditAmt { get; set; }
        public string AccountId { get; set; }
        public string PostedBy { get; set; }
        public bool? Approved { get; set; }
        public string ApprovedBy { get; set; }
        public bool? IsMoved { get; set; }
        public DateTime ValueDate { get; set; }
        public string Legtype { get; set; }
        public string BatchRef { get; set; }
        public string CoyCode { get; set; }
        public string BranchCode { get; set; }
        public string ApplicationId { get; set; }
        public bool? IsSuspended { get; set; }
        public int? OperationId { get; set; }
        public int? TransactionRef { get; set; }
    }
}
