﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingProductType
    {
        public int PdTypeId { get; set; }
        public string PdType { get; set; }
        public int? PdGroupId { get; set; }
        public int? PdPayDay { get; set; }
        public int? PdTypecode { get; set; }
    }
}
