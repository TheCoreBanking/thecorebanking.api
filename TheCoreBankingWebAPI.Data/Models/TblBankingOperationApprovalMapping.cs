﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingOperationApprovalMapping
    {
        public int Id { get; set; }
        public string StaffId { get; set; }
        public string BrCode { get; set; }
        public string CoyCode { get; set; }
        public string ApprovingBrCode { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateCreated { get; set; }
        public string BranchName { get; set; }
    }
}
