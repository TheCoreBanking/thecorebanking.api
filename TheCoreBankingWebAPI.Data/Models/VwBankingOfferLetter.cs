﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwBankingOfferLetter
    {
        public int Id { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string CustCode { get; set; }
        public string PdType { get; set; }
        public string ProductAcctNo { get; set; }
        public int? Tenor { get; set; }
        public decimal? Rate { get; set; }
        public DateTime? EffectDate { get; set; }
        public DateTime? TerminalDate { get; set; }
        public decimal? Principal { get; set; }
        public string Name { get; set; }
        public bool? DisChargeLetter { get; set; }
        public bool? OfferLetter { get; set; }
        public int? Status { get; set; }
        public string CoyCode { get; set; }
        public string BranchId { get; set; }
        public string Currentemployer { get; set; }
        public bool? Disbursed { get; set; }
    }
}
