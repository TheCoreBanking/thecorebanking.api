﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingPayment
    {
        public int Id { get; set; }
        public string CustCode { get; set; }
        public string ProductAcctNo { get; set; }
        public string ProdCode { get; set; }
        public string ProdName { get; set; }
        public string PdtypeId { get; set; }
        public decimal? AmountPaid { get; set; }
        public decimal? OustandingPrincipal { get; set; }
        public decimal? PrincipalBalance { get; set; }
        public decimal? InterestAccrued { get; set; }
        public DateTime? DatePaid { get; set; }
        public string CoyId { get; set; }
        public string BranchId { get; set; }
        public bool? Approved { get; set; }
        public bool? Disapproved { get; set; }
        public DateTime? DateApproved { get; set; }
        public decimal? OutStandingBalance { get; set; }
        public decimal? AmountDue { get; set; }
        public DateTime? TerminalDate { get; set; }
        public bool? Active { get; set; }
        public decimal? DailyInterest { get; set; }
        public string Status { get; set; }
        public bool? PrintDealslip { get; set; }
    }
}
