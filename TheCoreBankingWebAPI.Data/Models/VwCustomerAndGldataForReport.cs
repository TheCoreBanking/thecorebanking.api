﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwCustomerAndGldataForReport
    {
        public string AccountName { get; set; }
        public string Accountnumber { get; set; }
        public int Iscurrentaccount { get; set; }
        public string Oldproductaccountnumber1 { get; set; }
        public string Oldproductaccountnumber2 { get; set; }
        public string Oldproductaccountnumber3 { get; set; }
        public int IsGl { get; set; }
    }
}
