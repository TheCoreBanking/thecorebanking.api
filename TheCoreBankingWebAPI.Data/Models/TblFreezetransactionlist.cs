﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblFreezetransactionlist
    {
        public TblFreezetransactionlist()
        {
            TblAccountfreeze = new HashSet<TblAccountfreeze>();
        }

        public int Id { get; set; }
        public string FreezeTransaction { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public virtual ICollection<TblAccountfreeze> TblAccountfreeze { get; set; }
    }
}
