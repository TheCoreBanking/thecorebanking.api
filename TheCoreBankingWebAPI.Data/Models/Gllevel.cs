﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class Gllevel
    {
        public int Id { get; set; }
        public string LevelName { get; set; }
    }
}
