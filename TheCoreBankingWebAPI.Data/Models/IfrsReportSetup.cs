﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class IfrsReportSetup
    {
        public int Id { get; set; }
        public double? ReportDivisor { get; set; }
        public string ReportDivisorDisplay { get; set; }
        public string TextHeaderColor { get; set; }
        public string TextBodyColor { get; set; }
        public string BackGroundHeaderColor { get; set; }
        public string BackGroundAlternate1Color { get; set; }
        public string SumGroupBackgroundColor { get; set; }
        public string BackGroundAlternate2Color { get; set; }
        public string OtherColor1 { get; set; }
        public string OtherColor2 { get; set; }
        public string ProfitCaptionCode { get; set; }
        public int GlLenght { get; set; }
        public string DefferedTaxAssetCaptionCode { get; set; }
        public string DefferedTaxLiabilityCaptionCode { get; set; }
    }
}
