﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwBankingCurrentGl
    {
        public int Productid { get; set; }
        public int? Principalbalancegl { get; set; }
        public int? Interestincomeexpensegl { get; set; }
        public int? Interestreceivablepayablegl { get; set; }
        public string Accountnumber { get; set; }
    }
}
