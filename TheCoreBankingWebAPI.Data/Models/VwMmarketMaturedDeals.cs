﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwMmarketMaturedDeals
    {
        public string MmDealId { get; set; }
        public int? AutoRolloverType { get; set; }
        public string CpId { get; set; }
        public decimal? DiscountedValue { get; set; }
        public decimal? MaturityAmount { get; set; }
        public decimal? PrincipalAmount { get; set; }
        public decimal? InterestAmount { get; set; }
        public string SettlementAccount { get; set; }
        public string CoyCode { get; set; }
        public string BranchCode { get; set; }
        public int? Productcategoryid { get; set; }
        public decimal? EffectiveYield { get; set; }
        public decimal? InterestRate { get; set; }
        public int? Tenor { get; set; }
        public decimal? Discount { get; set; }
        public DateTime? MaturityDate { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public short? Productclassid { get; set; }
        public decimal Availablebalance { get; set; }
        public string AccountName { get; set; }
        public int Id { get; set; }
        public string ProductCode { get; set; }
        public string DealCreatedby { get; set; }
        public string StaffNo { get; set; }
        public int? Producttypeid { get; set; }
        public string PdType { get; set; }
        public string DealId { get; set; }
        public decimal? IAccrualTodate { get; set; }
        public decimal? IPrincipal { get; set; }
        public decimal? Charge { get; set; }
        public decimal? TaxCharge { get; set; }
    }
}
