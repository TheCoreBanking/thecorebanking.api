﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMattemp
    {
        public int Id { get; set; }
        public string Customer { get; set; }
        public decimal? RepaymentAmount { get; set; }
        public string Bucket { get; set; }
        public string Coyname { get; set; }
        public string Coycode { get; set; }
        public string Brcode { get; set; }
        public string AccountNo { get; set; }
        public DateTime? Paymentdate { get; set; }
        public int? Tenor { get; set; }
        public string ProductName { get; set; }
    }
}
