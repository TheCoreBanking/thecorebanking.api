﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwTransactionRef
    {
        public string Ref { get; set; }
    }
}
