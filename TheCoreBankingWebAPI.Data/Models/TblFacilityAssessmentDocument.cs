﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblFacilityAssessmentDocument
    {
        public int CreditAssessmentId { get; set; }
        public string CreditAssessmen1 { get; set; }
        public string CreditAssessmen2 { get; set; }
        public string AccountNo { get; set; }
        public string CustomerId { get; set; }
        public bool? IsApproved { get; set; }
        public bool? IsDisapproved { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}
