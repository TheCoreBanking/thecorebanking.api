﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwBankingMmoperations
    {
        public string MDealStatus { get; set; }
        public int MDealStatusId { get; set; }
        public string Customercode { get; set; }
        public string CustomerName { get; set; }
        public string MmDealId { get; set; }
        public string DealId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public decimal? DiscountedValue { get; set; }
        public decimal? MaturityAmount { get; set; }
        public decimal? PrincipalAmount { get; set; }
        public decimal? InterestAmount { get; set; }
        public decimal? Discount { get; set; }
        public int? Tenor { get; set; }
        public decimal? InterestRate { get; set; }
        public decimal? AmtCollected { get; set; }
        public decimal? PenalAmount { get; set; }
        public decimal? AmtReInvested { get; set; }
        public decimal? EffectiveYield { get; set; }
        public decimal? NewInterestRate { get; set; }
        public string SettlementAccount { get; set; }
        public int? Expr1 { get; set; }
        public int? MDealEventId { get; set; }
        public string NewDealId { get; set; }
        public string CoyCode { get; set; }
        public string BranchCode { get; set; }
        public string DealCreatedby { get; set; }
        public bool? DealApproved { get; set; }
        public string DealApprovedby { get; set; }
        public DateTime? DateApproved { get; set; }
        public int? OperationId { get; set; }
        public bool? Reversed { get; set; }
        public string PaymentMode { get; set; }
        public string MmDealName { get; set; }
        public bool? DealDisapproved { get; set; }
    }
}
