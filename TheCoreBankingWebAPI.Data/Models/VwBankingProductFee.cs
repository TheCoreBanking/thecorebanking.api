﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwBankingProductFee
    {
        public int PdFeesId { get; set; }
        public string PdFeesName { get; set; }
        public int? Catid { get; set; }
        public int? PdTarget { get; set; }
        public string PdFeesLedgerDr { get; set; }
        public string PdFeesLedgerCr { get; set; }
        public string PdFeesInterval { get; set; }
        public DateTime PdFeedate { get; set; }
        public int? PdTypeId { get; set; }
        public decimal? RateValue { get; set; }
        public string ProductName { get; set; }
        public int? PdId { get; set; }
        public string Productcategoryname { get; set; }
    }
}
