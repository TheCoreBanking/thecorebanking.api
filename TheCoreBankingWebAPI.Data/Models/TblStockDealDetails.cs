﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblStockDealDetails
    {
        public string TradeNo { get; set; }
        public DateTime? DlDate { get; set; }
        public string CpId { get; set; }
        public string SkSymbol { get; set; }
        public int? DlId { get; set; }
        public int? DlVolume { get; set; }
        public decimal? DlUnitPrice { get; set; }
        public decimal? DlConsideration { get; set; }
        public decimal? DlBrokageComm { get; set; }
        public decimal? DlBrokVat { get; set; }
        public decimal? DlConStamp { get; set; }
        public decimal? DlCscsfees { get; set; }
        public decimal? DlCscsVat { get; set; }
        public decimal? DlSecfees { get; set; }
        public decimal? DlNsefees { get; set; }
        public int? DlNseVat { get; set; }
        public decimal? Amount { get; set; }
        public decimal? AmountPaying { get; set; }
        public decimal? AmountPaid { get; set; }
        public string MadeBy { get; set; }
    }
}
