﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblTransfercharge1
    {
        public int Transferchargeid { get; set; }
        public decimal Maxamount { get; set; }
        public decimal Minamount { get; set; }
        public decimal Amounttocharge { get; set; }
        public int? Branchid { get; set; }
        public int? Companyid { get; set; }
        public string Createdby { get; set; }
        public DateTime Datetimecreated { get; set; }
        public bool Deleted { get; set; }
        public string Deletedby { get; set; }
        public DateTime? Datetimedeleted { get; set; }
        public long Chartofaccountid { get; set; }
        public bool? Isapproved { get; set; }
        public bool? Isdisapproved { get; set; }
        public string Approvalstatus { get; set; }
        public int? Copyfileid { get; set; }
        public bool? Isnewlycreated { get; set; }
        public bool? Deleteflag { get; set; }
        public string Comment { get; set; }
        public DateTime? Dateapproved { get; set; }
    }
}
