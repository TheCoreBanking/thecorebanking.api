﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwListCancellation
    {
        public int Id { get; set; }
        public string CustName { get; set; }
        public string CustCode { get; set; }
        public string ProductName { get; set; }
        public string ProductAccNo { get; set; }
        public string LastInterestPaid { get; set; }
        public int? Tenor { get; set; }
        public string BatchRef { get; set; }
        public int? Instalment { get; set; }
        public string Principal { get; set; }
        public DateTime? DateCancelled { get; set; }
        public string Remark { get; set; }
        public DateTime? DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public decimal? OutstandingPrincipal { get; set; }
        public int? InstalmentLeft { get; set; }
        public decimal? InterestAmount { get; set; }
        public decimal? Rate { get; set; }
        public DateTime? EffectDate { get; set; }
    }
}
