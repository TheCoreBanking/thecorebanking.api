﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwBankingMmproductRpt
    {
        public string ProductId { get; set; }
        public string ProductCode { get; set; }
        public int? ProductTypeId { get; set; }
        public string ProductName { get; set; }
        public string CpId { get; set; }
        public string DealId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public int? Tenor { get; set; }
        public decimal? InterestRate { get; set; }
        public decimal? Discount { get; set; }
        public decimal? PrincipalAmount { get; set; }
        public decimal? InterestAmount { get; set; }
        public decimal? MaturityAmount { get; set; }
        public decimal? EffectiveYield { get; set; }
        public string DealCreatedby { get; set; }
        public string DealApprovedby { get; set; }
        public DateTime? MaturityDate { get; set; }
        public string CoyCode { get; set; }
        public string BranchCode { get; set; }
        public string CustomerName { get; set; }
        public bool? DealApproved { get; set; }
        public int? ProductCategoryId { get; set; }
        public string ProductCategoryName { get; set; }
        public decimal? InterestPerDay { get; set; }
        public decimal? DiscountedValue { get; set; }
        public string PdmmTypeId { get; set; }
    }
}
