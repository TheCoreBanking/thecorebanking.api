﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyFees9
    {
        public int Id { get; set; }
        public DateTime? DDate { get; set; }
        public string DealId { get; set; }
        public decimal? Dptday { get; set; }
        public decimal? Dptdate { get; set; }
        public decimal? Fee { get; set; }
        public int? Tenor { get; set; }
        public DateTime? MaturityDate { get; set; }
        public decimal? EarnedFee { get; set; }
        public decimal? Uefee { get; set; }
        public int? Days2maturity { get; set; }
        public DateTime? CurrentDate { get; set; }
        public string EmpId { get; set; }
        public bool? CollectDiscount { get; set; }
        public bool? Matured { get; set; }
    }
}
