﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwRiskFactorIndex
    {
        public int Id { get; set; }
        public int RiskFactorId { get; set; }
        public string RiskFactorsDescription { get; set; }
        public int? RiskWeight { get; set; }
        public string ProductCode { get; set; }
        public string FactorIndexDescription { get; set; }
        public int? Weight { get; set; }
    }
}
