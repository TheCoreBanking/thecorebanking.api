﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblRegion
    {
        public TblRegion()
        {
            TblCustomer = new HashSet<TblCustomer>();
            TblInsertcustomerprofile = new HashSet<TblInsertcustomerprofile>();
            TblState = new HashSet<TblState>();
        }

        public int Regionid { get; set; }
        public int Countryid { get; set; }
        public string Regionname { get; set; }

        public virtual TblCountry Country { get; set; }
        public virtual ICollection<TblCustomer> TblCustomer { get; set; }
        public virtual ICollection<TblInsertcustomerprofile> TblInsertcustomerprofile { get; set; }
        public virtual ICollection<TblState> TblState { get; set; }
    }
}
