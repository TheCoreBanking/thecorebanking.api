﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyInterestWh
    {
        public int Id { get; set; }
        public string DealId1 { get; set; }
        public DateTime? Date { get; set; }
        public decimal? Iptday { get; set; }
    }
}
