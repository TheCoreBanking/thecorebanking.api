﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCustomeraccountbankingservice
    {
        public int Id { get; set; }
        public int Bankingserviceid { get; set; }
        public int Customeraccountserviceid { get; set; }

        public virtual TblAccountbankingservice Bankingservice { get; set; }
        public virtual TblCustomeraccountservice Customeraccountservice { get; set; }
    }
}
