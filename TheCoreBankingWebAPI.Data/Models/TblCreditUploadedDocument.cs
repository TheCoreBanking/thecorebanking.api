﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCreditUploadedDocument
    {
        public int Id { get; set; }
        public string AccountNo { get; set; }
        public string CustCode { get; set; }
        public string BatcheRef { get; set; }
        public string ShortCode { get; set; }
        public byte[] UploadedDocument { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public string DocumentName { get; set; }
        public bool Approved { get; set; }
        public bool IsDelete { get; set; }
        public string FileExtension { get; set; }
    }
}
