﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblAccountstmntmedium
    {
        public TblAccountstmntmedium()
        {
            TblCustomeraccountstmntmedium = new HashSet<TblCustomeraccountstmntmedium>();
        }

        public int Id { get; set; }
        public string Statementmedium { get; set; }
        public bool? Active { get; set; }

        public virtual ICollection<TblCustomeraccountstmntmedium> TblCustomeraccountstmntmedium { get; set; }
    }
}
