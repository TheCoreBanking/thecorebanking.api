﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheCoreBankingWebAPI.Data.Models
{
    public class GlobalbalanceVM
    {
        public int ID { get; set; }
        public string CustCode { get; set; }
        public string AccountNo { get; set; }
        public string ProductCode { get; set; }
        public string BalanceType { get; set; }
        public decimal? Balances { get; set; }
        public string ProductName { get; set; }
        public decimal? PendingDr { get; set; }
        public decimal? PendingCr { get; set; }
        public decimal UnclearedChq { get; set; }
        public string CustName { get; set; }
        public decimal BlockedBalance { get; set; }
        public decimal TotalDebitAmt { get; set; }
        public decimal TotalCreditAmt { get; set; }
        public decimal? AccountBalance { get; set; }
        public string BalSign { get; set; }
    }
}
