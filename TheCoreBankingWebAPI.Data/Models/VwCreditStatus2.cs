﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwCreditStatus2
    {
        public string ProductAcctNo { get; set; }
        public decimal? InterestPayment { get; set; }
        public decimal? PrincipalRepayment { get; set; }
        public decimal? TotalRepayment { get; set; }
    }
}
