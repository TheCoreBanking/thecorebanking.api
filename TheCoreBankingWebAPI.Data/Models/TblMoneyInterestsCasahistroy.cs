﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyInterestsCasahistroy
    {
        public int Id { get; set; }
        public DateTime? DDate { get; set; }
        public string AccountNumber { get; set; }
        public decimal? AveBalance { get; set; }
        public decimal? InterestRate { get; set; }
        public decimal? Iptday { get; set; }
        public decimal? IAccrualTodate { get; set; }
        public decimal? Iptdate { get; set; }
        public DateTime? LastEod { get; set; }
        public DateTime? Som { get; set; }
    }
}
