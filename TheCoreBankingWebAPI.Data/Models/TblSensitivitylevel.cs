﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblSensitivitylevel
    {
        public TblSensitivitylevel()
        {
            TblCustomer = new HashSet<TblCustomer>();
        }

        public int Id { get; set; }
        public string Sensitivitylevel { get; set; }
        public bool? Active { get; set; }

        public virtual ICollection<TblCustomer> TblCustomer { get; set; }
    }
}
