﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyDealOperationHistory
    {
        public int Id { get; set; }
        public string DealId { get; set; }
        public string MmDealId { get; set; }
        public string CpId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public int? Tenor { get; set; }
        public decimal? InterestRate { get; set; }
        public string NewDealId { get; set; }
        public string CoyCode { get; set; }
        public string BranchCode { get; set; }
        public string DealCreatedby { get; set; }
        public bool? DealApproved { get; set; }
        public string DealApprovedby { get; set; }
        public string Operation { get; set; }
        public DateTime? MaturityDate { get; set; }
        public int? PdTypeId { get; set; }
        public int? OperationId { get; set; }
        public DateTime? TransDate { get; set; }
        public decimal? Amount { get; set; }
        public decimal? PrincipalAmount { get; set; }
        public decimal? DiscountedVal { get; set; }
        public decimal? Discount { get; set; }
        public decimal? InterestAmt { get; set; }
        public decimal? MaturityAmt { get; set; }
    }
}
