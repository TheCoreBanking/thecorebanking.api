﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwListTermination
    {
        public int Id { get; set; }
        public string CustName { get; set; }
        public string CustCode { get; set; }
        public string ProductName { get; set; }
        public string ProductAccNo { get; set; }
        public int? Instalment { get; set; }
        public decimal? OutstandingPrincipal { get; set; }
        public string Remark { get; set; }
        public DateTime? DateTerminated { get; set; }
        public decimal? Rate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? Tenor { get; set; }
        public decimal? Principal { get; set; }
        public string Ref { get; set; }
    }
}
