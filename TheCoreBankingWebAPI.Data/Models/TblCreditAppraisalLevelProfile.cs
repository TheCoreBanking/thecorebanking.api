﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCreditAppraisalLevelProfile
    {
        public int LevelProfileId { get; set; }
        public int LevelId { get; set; }
        public int ProfileId { get; set; }
    }
}
