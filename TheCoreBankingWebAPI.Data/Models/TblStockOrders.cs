﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblStockOrders
    {
        public string SoId { get; set; }
        public int? DlId { get; set; }
        public DateTime? SoDate { get; set; }
        public decimal? SoConsideration { get; set; }
        public decimal? SoEstCommFees { get; set; }
        public decimal? SoTotalAmount { get; set; }
        public string SoMadeBy { get; set; }
        public bool? SoApproved { get; set; }
        public string SoApprovedBy { get; set; }
        public int? EmpId { get; set; }
    }
}
