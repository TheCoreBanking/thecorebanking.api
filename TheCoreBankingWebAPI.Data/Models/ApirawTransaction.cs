﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class ApirawTransaction
    {
        public long Id { get; set; }
        public long? TransactionType { get; set; }
        public string ApplicationId { get; set; }
        public string Narration { get; set; }
        public string RefNumber { get; set; }
        public decimal? Amount { get; set; }
        public string Glaccount { get; set; }
        public string PostedBy { get; set; }
        public string ApprovedBy { get; set; }
        public string SourceBranch { get; set; }
        public string Miscode { get; set; }
        public string CustomerAccount { get; set; }
        public string CompanyCode { get; set; }
        public string CustomerCode { get; set; }
        public string ProductCode { get; set; }
        public bool? IsCustomer { get; set; }
        public string PostLegType { get; set; }
        public DateTime? ValueDate { get; set; }
        public DateTime? DateCreated { get; set; }
        public string ControlRef { get; set; }
    }
}
