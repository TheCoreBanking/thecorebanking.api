﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwIfrsglaccountMapping
    {
        public string AccountId { get; set; }
        public string AccountName { get; set; }
        public string CompanyCode { get; set; }
        public string BranchCode { get; set; }
        public long SubCaptionId { get; set; }
        public string SubCaption { get; set; }
        public string MainCaptionCode { get; set; }
        public string Caption { get; set; }
        public string FinType { get; set; }
        public string FinSubType { get; set; }
        public int? Position { get; set; }
        public int? RegistryId { get; set; }
        public int? ParentId { get; set; }
    }
}
