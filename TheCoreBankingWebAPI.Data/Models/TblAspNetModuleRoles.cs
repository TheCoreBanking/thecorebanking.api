﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblAspNetModuleRoles
    {
        public int Id { get; set; }
        public string ModuleName { get; set; }
    }
}
