﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwLedgerStatement
    {
        public DateTime? TransactionDate { get; set; }
        public string AccountName { get; set; }
        public string Ref { get; set; }
        public string Description { get; set; }
        public decimal? DebitAmt { get; set; }
        public decimal? CreditAmt { get; set; }
        public string PostingTime { get; set; }
        public int? AccountCategoryId { get; set; }
        public string PostedBy { get; set; }
        public string ApprovedBy { get; set; }
        public string AccountId { get; set; }
        public DateTime? ValueDate { get; set; }
        public string SourceBranch { get; set; }
        public string DestinationBranch { get; set; }
        public string Legtype { get; set; }
        public string BatchRef { get; set; }
        public string SCoyCode { get; set; }
        public string CasaaccountName { get; set; }
        public string ProductName { get; set; }
        public string LoanProductName { get; set; }
    }
}
