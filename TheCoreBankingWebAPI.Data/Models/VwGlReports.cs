﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwGlReports
    {
        public string AccountId { get; set; }
        public string AccountName { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string TransactionType { get; set; }
        public string Ref { get; set; }
        public string Description { get; set; }
        public decimal? DebitAmt { get; set; }
        public decimal? CreditAmt { get; set; }
        public int AccountTypeId { get; set; }
        public string Account { get; set; }
        public int AccountGroupId { get; set; }
    }
}
