﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCreditAppraisalProfile
    {
        public int ProfileId { get; set; }
        public string Profile { get; set; }
        public string ProfileDescription { get; set; }
    }
}
