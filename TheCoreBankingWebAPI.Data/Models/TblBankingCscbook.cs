﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingCscbook
    {
        public int Id { get; set; }
        public string Pdno { get; set; }
        public string CustCode { get; set; }
        public decimal? Amount { get; set; }
        public string Operation { get; set; }
        public decimal? Inflow { get; set; }
        public decimal? Outflow { get; set; }
        public DateTime? OpDate { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
    }
}
