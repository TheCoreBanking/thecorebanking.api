﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblKycdocumenttype
    {
        public int Documenttypeid { get; set; }
        public string Documenttypename { get; set; }
    }
}
