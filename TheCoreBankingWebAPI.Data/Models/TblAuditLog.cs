﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblAuditLog
    {
        public int Id { get; set; }
        public string Product { get; set; }
        public string PdAcctNo { get; set; }
        public string Operation { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string OriginalValue { get; set; }
        public string NewValue { get; set; }
        public string Reason { get; set; }
        public string ActionBy { get; set; }
        public DateTime? ActionDate { get; set; }
        public string ApprovedBy { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public string ExceptionId { get; set; }
    }
}
