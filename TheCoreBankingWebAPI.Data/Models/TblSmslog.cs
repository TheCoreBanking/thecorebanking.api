﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblSmslog
    {
        public int Id { get; set; }
        public string AccountNumber { get; set; }
        public decimal Amount { get; set; }
        public DateTime? DateSent { get; set; }
        public bool IsSent { get; set; }
        public bool IsTransactionAlert { get; set; }
        public bool IsReturnCheque { get; set; }
        public bool IsDishonoured { get; set; }
        public bool IsNewAccount { get; set; }
        public bool IsCharged { get; set; }
        public DateTime DateCreated { get; set; }
        public string AlertType { get; set; }
        public string Narration { get; set; }
        public string Apiresponse { get; set; }
        public string ApitransactionId { get; set; }
        public bool IsSmsInitiated { get; set; }
        public int SmsTypeId { get; set; }
        public int? MessageId { get; set; }
    }
}
