﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyAmountPaid
    {
        public int Id { get; set; }
        public string CpId { get; set; }
        public string CpName { get; set; }
        public string Ref { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Amtcollected { get; set; }
        public decimal? AmtLeft { get; set; }
        public decimal? TotalCollected { get; set; }
        public DateTime? DateOperated { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public string DealId { get; set; }
        public string DealName { get; set; }
        public bool? Approved { get; set; }
        public DateTime? DateApproved { get; set; }
        public string ApprovedBy { get; set; }
        public bool? Disapproved { get; set; }
    }
}
