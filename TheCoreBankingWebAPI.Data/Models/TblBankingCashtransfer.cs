﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingCashtransfer
    {
        public int Id { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string StaffId { get; set; }
        public string StaffBrId { get; set; }
        public string TransDescription { get; set; }
        public decimal? DebitAmount { get; set; }
        public string MyRef { get; set; }
        public string GivingBranch { get; set; }
        public string ReceivingBranch { get; set; }
        public string Coy { get; set; }
        public string Giving { get; set; }
        public string GivingGl { get; set; }
        public string Receiving { get; set; }
        public string ReceivingGl { get; set; }
        public bool? Approved { get; set; }
        public bool? Disapproved { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public string ApprovedBy { get; set; }
        public string TransCode { get; set; }
        public string TransTime { get; set; }
        public DateTime? ActualDate { get; set; }
        public bool? Reversed { get; set; }
        public bool? ReversalStatus { get; set; }
        public string ReversedBy { get; set; }
        public string ReversalApprovedBy { get; set; }
    }
}
