﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCotsetup
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? DateApproved { get; set; }
        public string BranchCode { get; set; }
        public string CompanyCode { get; set; }
        public string FeeName { get; set; }
        public decimal FeeAmount { get; set; }
        public decimal MinTransactionAmount { get; set; }
        public int CreditLedgerId { get; set; }
        public string Remark { get; set; }
        public bool Deleted { get; set; }
        public bool? Isapproved { get; set; }
        public bool? Isdisapproved { get; set; }
        public string Approvalstatus { get; set; }
        public int? Copyfileid { get; set; }
        public bool? Isnewlycreated { get; set; }
        public bool? Deleteflag { get; set; }
        public string Comment { get; set; }
        public string Reference { get; set; }
        public int? PdId { get; set; }
        public bool? Withdrawal { get; set; }
        public bool? Deposit { get; set; }
        public bool? Lodgement { get; set; }
        public bool? TransferSameAccount { get; set; }
        public bool? TransferDiffAccount { get; set; }
        public bool? InterBankTransferSameAccount { get; set; }
        public bool? InterBankTransferDifferentAccount { get; set; }
        public bool? IsFeeRate { get; set; }
        public int? DebitLedgerId { get; set; }
        public DateTime? CutoffDay { get; set; }
        public bool? Active { get; set; }
    }
}
