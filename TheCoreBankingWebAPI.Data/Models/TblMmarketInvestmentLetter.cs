﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMmarketInvestmentLetter
    {
        public int Id { get; set; }
        public string DealId { get; set; }
        public string CpId { get; set; }
        public string CpName { get; set; }
        public decimal? FaceVale { get; set; }
        public decimal? InterestAmount { get; set; }
        public decimal? Wht { get; set; }
        public decimal? AmountAdded { get; set; }
        public decimal? MaturityAmount { get; set; }
        public decimal? InterestRate { get; set; }
        public int? Tenor { get; set; }
        public int? OperationId { get; set; }
        public string Pccode { get; set; }
        public string UnitCode { get; set; }
        public string Unit { get; set; }
        public string AccountOfficer { get; set; }
        public string RelationshipOfficer { get; set; }
        public DateTime? DateCreated { get; set; }
        public bool? SentLetter { get; set; }
        public string RolloverType { get; set; }
        public DateTime? DatePrinted { get; set; }
        public string PrintedBy { get; set; }
        public DateTime? MaturityDate { get; set; }
        public int? PdTypeId { get; set; }
        public string PdName { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string AcctOfficerStaffNo { get; set; }
        public string RelOfficerStaffNo { get; set; }
        public decimal? GrossInterestAmount { get; set; }
        public byte[] AcctOfficerSign { get; set; }
        public byte[] RelOfficerSign { get; set; }
        public string Operation { get; set; }
        public string AcctOfficerRank { get; set; }
        public string RelOfficerRank { get; set; }
        public decimal? AddedAmountAtMaturity { get; set; }
        public decimal? TakenAmountAtMaturity { get; set; }
        public decimal? AddedAmountb4Maturity { get; set; }
        public decimal? TakenAmountb4Maturity { get; set; }
        public decimal? OldPrincipal { get; set; }
        public DateTime? TransDate { get; set; }
        public DateTime? OldMaturityDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CusType { get; set; }
    }
}
