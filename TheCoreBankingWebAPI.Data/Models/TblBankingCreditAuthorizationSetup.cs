﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingCreditAuthorizationSetup
    {
        public int Id { get; set; }
        public string CreditAuthOfficer { get; set; }
        public string Position { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CoyCode { get; set; }
        public string StaffNo { get; set; }
        public string StaffName { get; set; }
    }
}
