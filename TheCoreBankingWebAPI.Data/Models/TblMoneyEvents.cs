﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyEvents
    {
        public int MmEventsId { get; set; }
        public string MmEventName { get; set; }
    }
}
