﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBondHoldHistory
    {
        public int BondHolderHistoryId { get; set; }
        public string BondNo { get; set; }
        public string BondName { get; set; }
        public string SeriesName { get; set; }
        public decimal? FaceValue { get; set; }
        public decimal? CouponRate { get; set; }
        public string HolderName { get; set; }
        public string HolderAddress { get; set; }
        public string HolderCode { get; set; }
        public bool? Approved { get; set; }
        public DateTime? ApprovedOn { get; set; }
        public string ApprovedBy { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? AchivedOn { get; set; }
    }
}
