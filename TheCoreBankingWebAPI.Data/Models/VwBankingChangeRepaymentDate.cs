﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwBankingChangeRepaymentDate
    {
        public string CustCode { get; set; }
        public string Customercode { get; set; }
        public DateTime? OldRepaymentDate { get; set; }
        public DateTime? NewRepaymentDate { get; set; }
        public decimal? OutprincipalBal { get; set; }
        public decimal? AccruedInterest { get; set; }
        public int? OperationId { get; set; }
        public string Remark { get; set; }
        public string Name { get; set; }
        public bool? Approved { get; set; }
        public bool? Disapproved { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string CreatedBy { get; set; }
        public int? DayDiff { get; set; }
        public int Id { get; set; }
        public string ProductAcctNo { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? Tenor { get; set; }
        public decimal? Rate { get; set; }
        public int? Installments { get; set; }
        public int? InstalmentLeft { get; set; }
        public int? TenorMode { get; set; }
        public int? PdTypeId { get; set; }
        public string CurrentAcct { get; set; }
    }
}
