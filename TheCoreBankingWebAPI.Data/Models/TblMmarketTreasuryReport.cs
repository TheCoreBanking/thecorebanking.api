﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMmarketTreasuryReport
    {
        public int Id { get; set; }
        public string DealId { get; set; }
        public string DealName { get; set; }
        public string CpId { get; set; }
        public string CpName { get; set; }
        public decimal? Rate { get; set; }
        public int? Tenor { get; set; }
        public decimal? Amount { get; set; }
        public decimal? InterestAmount { get; set; }
        public decimal? CurBalance { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public DateTime? TransDate { get; set; }
        public int? NoOfDays { get; set; }
        public int? PdCategoryId { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public decimal? AccruedInterest { get; set; }
        public decimal? InterestDiff { get; set; }
        public int? PdMtype { get; set; }
        public string Sector { get; set; }
    }
}
