﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblFreezereasontype
    {
        public int Id { get; set; }
        public string Reason { get; set; }
    }
}
