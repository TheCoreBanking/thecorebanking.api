﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwMoneyMarketDeals
    {
        public string CpName { get; set; }
        public string DealId { get; set; }
        public int Id { get; set; }
        public string MmDealId { get; set; }
        public string CpId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public decimal? DiscountedValue { get; set; }
        public decimal? MaturityAmount { get; set; }
        public decimal? PrincipalAmount { get; set; }
        public decimal? InterestAmount { get; set; }
        public decimal? Discount { get; set; }
        public int? Tenor { get; set; }
        public decimal? InterestRate { get; set; }
        public decimal? EffectiveYield { get; set; }
        public string SettlementAccount { get; set; }
        public int? MDealStatusId { get; set; }
        public string MmDealName { get; set; }
        public string CoyCode { get; set; }
        public string BranchCode { get; set; }
        public string PdmmTypeId { get; set; }
        public int? TypeId { get; set; }
        public bool? Operated { get; set; }
    }
}
