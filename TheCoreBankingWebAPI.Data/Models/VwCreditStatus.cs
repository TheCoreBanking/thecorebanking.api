﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwCreditStatus
    {
        public string CustCode { get; set; }
        public string ProductAcctNo { get; set; }
        public string Name { get; set; }
        public string CustomerName { get; set; }
        public string BranchId { get; set; }
        public int? Tenor { get; set; }
        public decimal? Rate { get; set; }
        public decimal? SecurityValue { get; set; }
        public decimal? SecValue { get; set; }
        public string Productdescription { get; set; }
        public DateTime? EffectDate { get; set; }
        public string ProductName { get; set; }
        public decimal? CurrAcctBal { get; set; }
        public string CurrentAcct { get; set; }
        public string ProductCode { get; set; }
        public string CoyCode { get; set; }
        public decimal? OutstandingPrincipal { get; set; }
        public DateTime? TerminalDate { get; set; }
        public decimal? Principal { get; set; }
        public bool? Approved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? DateApproved { get; set; }
        public int? Status { get; set; }
        public bool? Disbursed { get; set; }
        public string Disburser { get; set; }
        public DateTime? DateOfDisburse { get; set; }
        public string FreqType { get; set; }
        public decimal? MaturityAmount { get; set; }
        public decimal? LoanInterestAmount { get; set; }
        public decimal? ApprovedAmount { get; set; }
        public DateTime? DateCreated { get; set; }
        public string Officer1 { get; set; }
        public string Officer2 { get; set; }
    }
}
