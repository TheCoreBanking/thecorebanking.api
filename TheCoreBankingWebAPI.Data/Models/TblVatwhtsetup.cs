﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblVatwhtsetup
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal? Rate { get; set; }
        public string PrincipalGl { get; set; }
        public bool? IsVat { get; set; }
        public string BrCode { get; set; }
        public string CoyCode { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateCreated { get; set; }
        public bool Approved { get; set; }
        public DateTime? DateApproved { get; set; }
        public string ApprovedBy { get; set; }
        public bool DisApproved { get; set; }
        public DateTime? DateDisApproved { get; set; }
        public string DisapprovedBy { get; set; }
        public bool IsDeleted { get; set; }
        public int? PdCategoryId { get; set; }
    }
}
