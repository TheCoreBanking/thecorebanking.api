﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCreditGuarantor
    {
        public int Id { get; set; }
        public string CustCode { get; set; }
        public string ProductAcctNo { get; set; }
        public string ProductName { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        public string Telephone2 { get; set; }
        public string ResidentilaAddress { get; set; }
        public string OfficialAddress { get; set; }
        public byte[] Documents { get; set; }
        public string Relationship { get; set; }
        public string RelationshipDuration { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? DateCreated { get; set; }
    }
}
