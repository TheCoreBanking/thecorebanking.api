﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblVatwhtsetupHistory1
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal? Rate { get; set; }
        public decimal? OldRate { get; set; }
        public string PrincipalGl { get; set; }
        public bool? IsVat { get; set; }
        public string BrCode { get; set; }
        public string CoyCode { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? DateModified { get; set; }
        public int? PdCategoryId { get; set; }
    }
}
