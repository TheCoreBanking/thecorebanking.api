﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBondLocalBidRate
    {
        public int BidRateId { get; set; }
        public decimal? BidRate { get; set; }
        public decimal? BuyRate { get; set; }
        public DateTime? SetDate { get; set; }
        public bool? Approved { get; set; }
        public string CoyId { get; set; }
    }
}
