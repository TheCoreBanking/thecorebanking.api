﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblFinanceAccountCategory
    {
        public long Id { get; set; }
        public string Descriptions { get; set; }
        public int AccountGroupId { get; set; }
        public string Active { get; set; }
    }
}
