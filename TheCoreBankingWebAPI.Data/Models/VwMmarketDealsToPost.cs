﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwMmarketDealsToPost
    {
        public string DealId { get; set; }
        public string PdmmTypeId { get; set; }
        public string CoyCode { get; set; }
        public string BranchCode { get; set; }
        public string MmDealId { get; set; }
        public int? Productcategoryid { get; set; }
        public string Productname { get; set; }
        public decimal? DailyInterest { get; set; }
        public int? Interestincomeexpensegl { get; set; }
        public int? Interestreceivablepayablegl { get; set; }
    }
}
