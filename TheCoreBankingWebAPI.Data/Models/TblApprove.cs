﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblApprove
    {
        public string StaffNo { get; set; }
        public int Id { get; set; }
        public string CoyId { get; set; }
        public string MisCode { get; set; }
        public string BranchId { get; set; }
        public string StaffName { get; set; }
        public string Department { get; set; }
        public string JobTitle { get; set; }
        public string Rank { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public DateTime? Age { get; set; }
        public string Gender { get; set; }
        public string NameOfNok { get; set; }
        public string PhoneOfNok { get; set; }
        public string EmailOfNok { get; set; }
        public string AddrOfNok { get; set; }
        public string GenderOfNok { get; set; }
        public string Comment { get; set; }
        public string State { get; set; }
        public string Nationality { get; set; }
        public string RelationShip { get; set; }
        public bool? Approved { get; set; }
        public bool? Disapproved { get; set; }
        public bool? Saved { get; set; }
        public string BranchLocation { get; set; }
        public string CoyName { get; set; }
        public string DeptCode { get; set; }
        public string UnitCode { get; set; }
        public string Pccode { get; set; }
        public string Unit { get; set; }
    }
}
