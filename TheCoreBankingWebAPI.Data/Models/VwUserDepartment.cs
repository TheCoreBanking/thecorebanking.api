﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwUserDepartment
    {
        public long Id { get; set; }
        public string StaffName { get; set; }
        public string Department { get; set; }
    }
}
