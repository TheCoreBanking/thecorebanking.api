﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblStockTransactionFees
    {
        public int CmCostId { get; set; }
        public string CmCostType { get; set; }
        public int? DlId { get; set; }
        public decimal? CmPercent { get; set; }
    }
}
