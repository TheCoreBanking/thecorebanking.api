﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwCommitteeApprovals
    {
        public int Id { get; set; }
        public int? UniqueOpId { get; set; }
        public string TransactionTypeId { get; set; }
        public string ApprovingStaffId { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public string Comment { get; set; }
        public string ApprovingStaffEmail { get; set; }
        public bool? Approved { get; set; }
        public string ProductAcctNo { get; set; }
        public int? ProductTypeId { get; set; }
        public string CustCode { get; set; }
        public string StaffName { get; set; }
        public string Custname { get; set; }
        public bool? Disapproved { get; set; }
        public bool? Completed { get; set; }
    }
}
