﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyDeal
    {
        public int Id { get; set; }
        public string MmDealId { get; set; }
        public string MmDealName { get; set; }
        public string DealClass { get; set; }
        public string PdtypeId { get; set; }
    }
}
