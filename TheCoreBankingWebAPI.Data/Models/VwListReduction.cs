﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwListReduction
    {
        public int Id { get; set; }
        public string CustName { get; set; }
        public string CustCode { get; set; }
        public string ProductName { get; set; }
        public string ProductAcctNo { get; set; }
        public decimal? OldPrincipal { get; set; }
        public decimal? NewPrincipal { get; set; }
        public decimal? AmountReduced { get; set; }
        public int? Instalment { get; set; }
        public decimal? OutprincipalBal { get; set; }
        public string Remark { get; set; }
        public DateTime? DateCreated { get; set; }
        public string CreatedBy { get; set; }
    }
}
