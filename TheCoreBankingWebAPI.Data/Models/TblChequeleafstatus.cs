﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblChequeleafstatus
    {
        public TblChequeleafstatus()
        {
            TblChequeleavesdetail = new HashSet<TblChequeleavesdetail>();
        }

        public int Id { get; set; }
        public string Status { get; set; }

        public virtual ICollection<TblChequeleavesdetail> TblChequeleavesdetail { get; set; }
    }
}
