﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingDisbursement
    {
        public int Id { get; set; }
        public string PdName { get; set; }
        public string PdCode { get; set; }
        public string PdId { get; set; }
        public string PdTypeId { get; set; }
        public string CustCode { get; set; }
        public string ProductAcctNo { get; set; }
        public string Customer { get; set; }
        public string DisbursedBy { get; set; }
        public DateTime? DateDisbursed { get; set; }
        public DateTime? DateCreated { get; set; }
        public string AccountId { get; set; }
        public decimal? PrincipalAmt { get; set; }
        public decimal? Rate { get; set; }
        public int? Moratorium { get; set; }
        public int? Tenor { get; set; }
        public int? Instalment { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public string MisCode { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public string CoyClass { get; set; }
        public string LoanAccount { get; set; }
        public int? Ref { get; set; }
        public string CurrentAccount { get; set; }
        public bool? Paid { get; set; }
        public decimal? AmountPaid { get; set; }
        public decimal? Balance { get; set; }
        public decimal? Frequency { get; set; }
        public int? BulletType { get; set; }
        public int? BulletFreq { get; set; }
        public string FreqType { get; set; }
        public string BulletName { get; set; }
        public string BulletFreqName { get; set; }
        public int? SchMethod { get; set; }
        public bool? Approved { get; set; }
        public bool? Disapproved { get; set; }
        public DateTime? DateApproved { get; set; }
        public string Comment { get; set; }
        public int? OperationId { get; set; }
        public bool? AddingPrincipal { get; set; }
        public decimal? OutstandingPrincipal { get; set; }
        public decimal? AccruedInterest { get; set; }
        public string ApproveRemark { get; set; }
        public string ApprovedBy { get; set; }
        public bool? PrincipalReview { get; set; }
        public bool? Financed { get; set; }
    }
}
