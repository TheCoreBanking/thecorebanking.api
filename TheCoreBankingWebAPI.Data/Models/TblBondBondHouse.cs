﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBondBondHouse
    {
        public int BondHouseId { get; set; }
        public int? BondId { get; set; }
        public int? SeriesId { get; set; }
        public string CountpartyId { get; set; }
        public int? ClassificationId { get; set; }
        public int? CatigoryId { get; set; }
        public decimal? FaceValue { get; set; }
        public double? CouponRate { get; set; }
        public decimal? SoldPrice { get; set; }
        public decimal? ParValue { get; set; }
        public DateTime? TransactionDate { get; set; }
        public DateTime? SettlementDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public int? OperationId { get; set; }
        public string MmDealId { get; set; }
        public bool? IsApproved { get; set; }
        public bool? IsDisapproved { get; set; }
        public DateTime? ActionDate { get; set; }
        public string ActionBy { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CoyCode { get; set; }
        public string BranchCode { get; set; }
        public string Comment { get; set; }
        public bool? IsSold { get; set; }
        public decimal? CleanPrice { get; set; }
        public decimal? BondYield { get; set; }
        public decimal? AccruedInterest { get; set; }
        public string SalesmanComment { get; set; }
        public int? BondSource { get; set; }
        public bool? IsLiquidated { get; set; }
        public string DealId { get; set; }
        public string SettlementAccount { get; set; }
        public double? CustodyFee { get; set; }
        public DateTime? PreviousCouponDate { get; set; }
    }
}
