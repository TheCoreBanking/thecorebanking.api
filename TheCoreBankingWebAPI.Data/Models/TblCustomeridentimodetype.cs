﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCustomeridentimodetype
    {
        public TblCustomeridentimodetype()
        {
            TblCustomeridentification = new HashSet<TblCustomeridentification>();
        }

        public int Identificationmodeid { get; set; }
        public string Identificationmode { get; set; }

        public virtual ICollection<TblCustomeridentification> TblCustomeridentification { get; set; }
    }
}
