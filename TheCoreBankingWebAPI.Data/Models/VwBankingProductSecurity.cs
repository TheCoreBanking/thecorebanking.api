﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwBankingProductSecurity
    {
        public int PdSecId { get; set; }
        public string PdSecurity { get; set; }
        public int PdId { get; set; }
        public string ProductName { get; set; }
        public decimal? HairCut { get; set; }
        public string SecType { get; set; }
        public bool? Active { get; set; }
        public string CreatedBy { get; set; }
        public string Remark { get; set; }
        public DateTime? ModDate { get; set; }
    }
}
