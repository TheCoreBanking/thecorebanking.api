﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblException
    {
        public int Id { get; set; }
        public string ExceptionCategory { get; set; }
        public DateTime? DateCreated { get; set; }
        public string ExceptionId { get; set; }
    }
}
