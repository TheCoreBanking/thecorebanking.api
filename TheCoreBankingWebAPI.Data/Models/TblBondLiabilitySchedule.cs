﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBondLiabilitySchedule
    {
        public int BondLiabilityScheduleId { get; set; }
        public int? Cnt { get; set; }
        public DateTime? CouponDate { get; set; }
        public int? NoOfDays { get; set; }
        public decimal? OpenBalance { get; set; }
        public decimal? PeriodicCoupon { get; set; }
        public decimal? PeriodicPrincipal { get; set; }
        public decimal? TotalPayment { get; set; }
        public decimal? EndBalance { get; set; }
        public string DealId { get; set; }
        public int? BondHouseId { get; set; }
    }
}
