﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwListRepayment
    {
        public int Id { get; set; }
        public string CustName { get; set; }
        public string CustCode { get; set; }
        public string ProductName { get; set; }
        public string ProductAcctNo { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? NewRepaymentDate { get; set; }
        public DateTime? DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public decimal? OutprincipalBal { get; set; }
        public decimal? AccruedInterest { get; set; }
    }
}
