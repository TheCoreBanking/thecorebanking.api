﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class IfrsTrialbalancegap
    {
        public int TrialBalanceGapid { get; set; }
        public string BranchCode { get; set; }
        public string Glcode { get; set; }
        public string Description { get; set; }
        public string GlsubHeadCode { get; set; }
        public string Currency { get; set; }
        public double? ExchangeRate { get; set; }
        public decimal? Debit { get; set; }
        public decimal? Credit { get; set; }
        public decimal? LcyDebit { get; set; }
        public decimal? LcyCredit { get; set; }
        public decimal? Balance { get; set; }
        public decimal? LcyBalance { get; set; }
        public string Gltype { get; set; }
        public decimal? RevaluationDiff { get; set; }
        public DateTime? TransDate { get; set; }
        public string CompanyCode { get; set; }
        public string SubGl { get; set; }
        public string AdjustmentCode { get; set; }
    }
}
