﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TsBankingErrorLog
    {
        public int Id { get; set; }
        public string ErrorMessage { get; set; }
        public int ErrorLine { get; set; }
        public int ErrorNumber { get; set; }
        public int ErrorState { get; set; }
        public string SpName { get; set; }
        public DateTime? DateOccurred { get; set; }
        public string TimeOccured { get; set; }
    }
}
