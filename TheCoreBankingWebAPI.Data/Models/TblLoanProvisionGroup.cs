﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblLoanProvisionGroup
    {
        public int Id { get; set; }
        public string ProdCode { get; set; }
        public string ProductName { get; set; }
        public string Category { get; set; }
    }
}
