﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblStampcharge
    {
        public int Id { get; set; }
        public decimal Charge { get; set; }
        public DateTime Datecreated { get; set; }
        public long? Chartofaccountid { get; set; }
        public bool? Isapproved { get; set; }
        public bool? Isdisapproved { get; set; }
        public string Approvalstatus { get; set; }
        public int? Copyfileid { get; set; }
        public bool? Isnewlycreated { get; set; }
        public bool? Deleteflag { get; set; }
        public string Comment { get; set; }
        public DateTime? Dateapproved { get; set; }

        public virtual TblFinanceChartOfAccount Chartofaccount { get; set; }
    }
}
