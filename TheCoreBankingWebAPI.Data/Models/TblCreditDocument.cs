﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCreditDocument
    {
        public int CreditDocumentId { get; set; }
        public string DocumentPath { get; set; }
        public string DocumentRef { get; set; }
        public string DocumentTitle { get; set; }
        public string BatchRef { get; set; }
        public string ShortCode { get; set; }
        public bool IsConverted { get; set; }
    }
}
