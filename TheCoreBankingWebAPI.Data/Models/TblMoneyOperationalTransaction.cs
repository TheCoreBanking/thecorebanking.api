﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyOperationalTransaction
    {
        public int Id { get; set; }
        public string CpId { get; set; }
        public string DealId { get; set; }
        public string MmDealId { get; set; }
        public decimal? Amount { get; set; }
        public bool? Inflow { get; set; }
        public string MmtypeId { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public decimal? InterestRate { get; set; }
        public int? Tenor { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public decimal? MaturityAmount { get; set; }
        public bool? DealSlip { get; set; }
        public bool? Letter { get; set; }
        public int? MDealEventId { get; set; }
        public DateTime? Dateprocessed { get; set; }
        public string ProcessedBy { get; set; }
        public bool? Cert { get; set; }
        public bool? Approved { get; set; }
        public bool? Disapproved { get; set; }
        public string Comment { get; set; }
        public decimal? InterestAmount { get; set; }
        public decimal? Discount { get; set; }
        public decimal? DiscountedValue { get; set; }
        public string PosterCode { get; set; }
        public string ApprovalCode { get; set; }
        public decimal? AmtCollected { get; set; }
        public string ApprovedBy { get; set; }
    }
}
