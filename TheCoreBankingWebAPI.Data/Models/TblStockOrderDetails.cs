﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblStockOrderDetails
    {
        public string SkSymbol { get; set; }
        public string CsAccNumber { get; set; }
        public string CpId { get; set; }
        public DateTime? SodDate { get; set; }
        public int? SodUnits { get; set; }
        public decimal? SodUnitPrice { get; set; }
        public decimal? SodUnitTotalAmount { get; set; }
        public string SoId { get; set; }
        public int SodId { get; set; }
        public decimal? SodUnitEstCommFees { get; set; }
        public decimal? SodUnitConsideration { get; set; }
        public bool? SodContracted { get; set; }
        public int? DlId { get; set; }
    }
}
