﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwBankingCreditProvision
    {
        public string CustCode { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string ProductAcctNo { get; set; }
        public string BranchId { get; set; }
        public int? Tenor { get; set; }
        public decimal? Rate { get; set; }
        public int? Installments { get; set; }
        public string CoyCode { get; set; }
        public decimal? Principal { get; set; }
        public string CasaproductAcctNo { get; set; }
        public int Casabalance { get; set; }
        public int OutstandingPrincipal { get; set; }
        public string CustomerName { get; set; }
        public string FreqType { get; set; }
        public int? SchMethod { get; set; }
        public DateTime? DateOfDisburse { get; set; }
        public DateTime? TerminalDate { get; set; }
        public string Officer1 { get; set; }
        public string Officer2 { get; set; }
        public decimal? InterestAmount { get; set; }
        public decimal? MaturityAmount { get; set; }
        public decimal? ApprovedAmount { get; set; }
        public DateTime? EffectDate { get; set; }
    }
}
