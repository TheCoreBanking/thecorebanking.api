﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblAspNetSubModuleRoles
    {
        public int Id { get; set; }
        public string ModuleId { get; set; }
        public string SubmoduleName { get; set; }
    }
}
