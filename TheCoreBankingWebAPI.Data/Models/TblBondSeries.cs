﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBondSeries
    {
        public int SeriesId { get; set; }
        public int BondId { get; set; }
        public string Trusty { get; set; }
        public string Registra { get; set; }
        public string SeriesName { get; set; }
        public decimal? Facevalue { get; set; }
        public decimal? CouponRate { get; set; }
        public int? BondClassificationId { get; set; }
        public double? ParValue { get; set; }
        public string PdCode { get; set; }
        public decimal? SoldPrice { get; set; }
        public bool? Valid { get; set; }
        public int? Bfrequancy { get; set; }
        public DateTime? IssurDate { get; set; }
        public DateTime? SettlementDate { get; set; }
        public DateTime? FirstCoupon { get; set; }
        public DateTime? MaturityDate { get; set; }
        public string CoyCode { get; set; }
        public string BranchCode { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public bool? IsApproved { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? ApprovedOn { get; set; }
        public string IsDisapprove { get; set; }
        public string Comment { get; set; }
        public int? ScheduleType { get; set; }
    }
}
