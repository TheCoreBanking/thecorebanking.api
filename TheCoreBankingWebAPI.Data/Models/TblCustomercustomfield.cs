﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCustomercustomfield
    {
        public int Id { get; set; }
        public int? CustomerCustomFieldListId { get; set; }
        public string OptionValue { get; set; }
        public string CustomerCustomFieldOptionId { get; set; }
        public int? InputTypeId { get; set; }
        public string InputName { get; set; }
        public int? Customerid { get; set; }
    }
}
