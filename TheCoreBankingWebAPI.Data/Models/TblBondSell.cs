﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBondSell
    {
        public int SellId { get; set; }
        public string BnSeries { get; set; }
        public decimal? CouponRate { get; set; }
        public DateTime? Tdate { get; set; }
        public decimal? Facevalues { get; set; }
        public DateTime? Lipd { get; set; }
        public decimal? AmountPaid { get; set; }
        public decimal? InterestSold { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Premium { get; set; }
        public decimal? CleanPrice { get; set; }
        public string DealClass { get; set; }
        public decimal? EffectiveYield { get; set; }
        public int? OpId { get; set; }
        public bool? Approved { get; set; }
        public bool? Disapproved { get; set; }
        public string ApprovedBy { get; set; }
        public string Userid { get; set; }
        public string Dateapproved { get; set; }
        public string DealId { get; set; }
        public string MmDealId { get; set; }
        public string DealName { get; set; }
        public string CpId { get; set; }
        public string CoyCode { get; set; }
        public string BranchCode { get; set; }
        public int? Unitseling { get; set; }
        public decimal? PricePerUnit { get; set; }
        public decimal? Currentyield { get; set; }
        public DateTime? Ncoupon { get; set; }
    }
}
