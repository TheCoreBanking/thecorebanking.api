﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblStockDealTypes
    {
        public int DlId { get; set; }
        public string DDealType { get; set; }
    }
}
