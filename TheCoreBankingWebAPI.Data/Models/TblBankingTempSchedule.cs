﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingTempSchedule
    {
        public int Id { get; set; }
        public int? Month { get; set; }
        public int? NoOfDays { get; set; }
        public double? PrincipalBalance { get; set; }
        public DateTime? NextPayDay { get; set; }
        public decimal? InterestAccrual { get; set; }
        public decimal? InterestPayment { get; set; }
        public decimal? PrincipalRepayment { get; set; }
        public decimal? TotalRepayment { get; set; }
        public decimal? EndBalance { get; set; }
        public string CustCode { get; set; }
        public string ProductAcctNo { get; set; }
        public decimal? FeeCharged { get; set; }
        public decimal? CummulativeInterest { get; set; }
        public decimal? CummulativePrinRepyt { get; set; }
    }
}
