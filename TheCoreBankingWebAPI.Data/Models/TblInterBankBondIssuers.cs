﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblInterBankBondIssuers
    {
        public int Id { get; set; }
        public string Issuer { get; set; }
        public DateTime? DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
    }
}
