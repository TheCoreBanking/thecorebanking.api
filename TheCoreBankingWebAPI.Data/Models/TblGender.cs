﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblGender
    {
        public TblGender()
        {
            TblCustomerGender = new HashSet<TblCustomer>();
            TblCustomerNokgender = new HashSet<TblCustomer>();
            TblEditedcustomer = new HashSet<TblEditedcustomer>();
            TblInsertcustomerprofile = new HashSet<TblInsertcustomerprofile>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<TblCustomer> TblCustomerGender { get; set; }
        public virtual ICollection<TblCustomer> TblCustomerNokgender { get; set; }
        public virtual ICollection<TblEditedcustomer> TblEditedcustomer { get; set; }
        public virtual ICollection<TblInsertcustomerprofile> TblInsertcustomerprofile { get; set; }
    }
}
