﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCreditAppraisalProduct
    {
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string CoyId { get; set; }
        public string BranchId { get; set; }
    }
}
