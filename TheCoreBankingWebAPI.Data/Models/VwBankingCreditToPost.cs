﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwBankingCreditToPost
    {
        public string ProductCode { get; set; }
        public string ProductAcctNo { get; set; }
        public string BranchId { get; set; }
        public string CoyCode { get; set; }
        public int? Productcategoryid { get; set; }
        public short? Productclassid { get; set; }
        public int? Interestincomeexpensegl { get; set; }
        public int? Interestreceivablepayablegl { get; set; }
        public decimal? DailyInterest { get; set; }
        public string Productname { get; set; }
        public decimal? IAccrualTodate { get; set; }
    }
}
