﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMmarketUnclearedCheques
    {
        public int Id { get; set; }
        public string CpId { get; set; }
        public string CpName { get; set; }
        public string Ref { get; set; }
        public string TellerNo { get; set; }
        public string ChequeNo { get; set; }
        public DateTime? ChequeDate { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? TellerDate { get; set; }
        public DateTime? ClearDate { get; set; }
        public DateTime? DateApproved { get; set; }
        public DateTime? DateCleared { get; set; }
        public bool? ChequeCleared { get; set; }
        public string Bank { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public int? OperationId { get; set; }
        public string ApprovedBy { get; set; }
        public string Remark { get; set; }
        public bool? Approved { get; set; }
        public bool? Disapproved { get; set; }
        public string Branch { get; set; }
        public int? ChequeValue { get; set; }
        public string ChequeType { get; set; }
        public int? NoOfCheques { get; set; }
        public string ClearingRemark { get; set; }
        public string ApprovalRemark { get; set; }
        public string ClearedBy { get; set; }
        public string Upload { get; set; }
        public bool? Lodged { get; set; }
        public int? OperationType { get; set; }
        public string BankGl { get; set; }
        public string OperationRemark { get; set; }
        public bool? Dv { get; set; }
        public int? ClearOption { get; set; }
        public string Source { get; set; }
        public string Purpose { get; set; }
        public string Sundry { get; set; }
        public string Account { get; set; }
        public string Narration { get; set; }
        public bool? FalselClearing { get; set; }
        public bool? ReturnCheque { get; set; }
        public string StaffNo { get; set; }
        public bool? Reversed { get; set; }
        public bool? ApproveReversal { get; set; }
        public bool? DisapprovedReversal { get; set; }
        public string ReversalComment { get; set; }
        public string ReversalApprovalComment { get; set; }
        public string IStatus { get; set; }
        public string DepositorCode { get; set; }
        public string Depositor { get; set; }
        public string SlipNumber { get; set; }
        public string BatchRef { get; set; }
        public bool ChargeStamp { get; set; }
        public bool? Location { get; set; }
    }
}
