﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingInitiatorsComment
    {
        public int CommentId { get; set; }
        public string Comment { get; set; }
    }
}
