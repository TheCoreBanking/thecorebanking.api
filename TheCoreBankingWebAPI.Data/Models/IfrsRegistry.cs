﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class IfrsRegistry
    {
        public int RegistryId { get; set; }
        public string Code { get; set; }
        public string Caption { get; set; }
        public int Position { get; set; }
        public string RefNote { get; set; }
        public string FinType { get; set; }
        public string FinSubType { get; set; }
        public int? ParentId { get; set; }
        public bool? IsTotalLine { get; set; }
        public string Color { get; set; }
        public int? Class { get; set; }
        public string CompanyCode { get; set; }
        public double? Multiplier { get; set; }
    }
}
