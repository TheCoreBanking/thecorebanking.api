﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblEmploymenttype
    {
        public TblEmploymenttype()
        {
            TblCustomer = new HashSet<TblCustomer>();
        }

        public int Id { get; set; }
        public string Type { get; set; }

        public virtual ICollection<TblCustomer> TblCustomer { get; set; }
    }
}
