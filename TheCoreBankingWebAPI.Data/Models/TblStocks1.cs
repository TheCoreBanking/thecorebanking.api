﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblStocks1
    {
        public string SkSymbol { get; set; }
        public string SkSecurity { get; set; }
        public decimal SkAverageCost { get; set; }
        public decimal SkUnitPrice { get; set; }
        public decimal SkNominalPrice { get; set; }
        public int SkVolume { get; set; }
        public int MkId { get; set; }
        public int? SecId { get; set; }
        public DateTime? SkYearend { get; set; }
        public bool? SkQuoted { get; set; }
    }
}
