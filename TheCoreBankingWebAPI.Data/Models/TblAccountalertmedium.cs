﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblAccountalertmedium
    {
        public TblAccountalertmedium()
        {
            TblCustomeraccountalertmedium = new HashSet<TblCustomeraccountalertmedium>();
        }

        public int Id { get; set; }
        public string Alerttype { get; set; }
        public bool? Active { get; set; }

        public virtual ICollection<TblCustomeraccountalertmedium> TblCustomeraccountalertmedium { get; set; }
    }
}
