﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCountry
    {
        public TblCountry()
        {
            TblCompany = new HashSet<TblCompany>();
            TblCustomer = new HashSet<TblCustomer>();
            TblCustomeraddress = new HashSet<TblCustomeraddress>();
            TblInsertcustomerprofile = new HashSet<TblInsertcustomerprofile>();
            TblRegion = new HashSet<TblRegion>();
            TblState = new HashSet<TblState>();
        }

        public int Countryid { get; set; }
        public string Name { get; set; }
        public string Countrycode { get; set; }

        public virtual ICollection<TblCompany> TblCompany { get; set; }
        public virtual ICollection<TblCustomer> TblCustomer { get; set; }
        public virtual ICollection<TblCustomeraddress> TblCustomeraddress { get; set; }
        public virtual ICollection<TblInsertcustomerprofile> TblInsertcustomerprofile { get; set; }
        public virtual ICollection<TblRegion> TblRegion { get; set; }
        public virtual ICollection<TblState> TblState { get; set; }
    }
}
