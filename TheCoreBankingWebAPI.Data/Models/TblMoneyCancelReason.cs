﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyCancelReason
    {
        public int Id { get; set; }
        public string CpId { get; set; }
        public string DealId { get; set; }
        public string RealReason { get; set; }
    }
}
