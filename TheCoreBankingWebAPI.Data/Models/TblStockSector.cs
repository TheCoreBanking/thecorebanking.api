﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblStockSector
    {
        public int SecId { get; set; }
        public string SecName { get; set; }
    }
}
