﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingEoy
    {
        public int Id { get; set; }
        public string AccountName { get; set; }
        public string AccountId { get; set; }
        public decimal? Balances { get; set; }
        public string TranYear { get; set; }
        public string CoyCode { get; set; }
    }
}
