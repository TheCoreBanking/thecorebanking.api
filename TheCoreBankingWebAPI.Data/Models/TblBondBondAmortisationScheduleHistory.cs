﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBondBondAmortisationScheduleHistory
    {
        public int ScheduleHistoryId { get; set; }
        public int ScheduleId { get; set; }
        public DateTime? AmortDate { get; set; }
        public int? Dtm { get; set; }
        public decimal? BeginBookVal { get; set; }
        public decimal? DlyCoup { get; set; }
        public decimal? DlyEffRate { get; set; }
        public decimal? DlyAmort { get; set; }
        public decimal? AmortRate { get; set; }
        public decimal? EndBookVal { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? SettlementDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public string BondName { get; set; }
        public string SeriesName { get; set; }
        public string DealId { get; set; }
        public int? BondHouseId { get; set; }
        public decimal? UnamortAmount { get; set; }
    }
}
