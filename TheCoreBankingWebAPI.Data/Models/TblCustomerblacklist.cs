﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCustomerblacklist
    {
        public int CustomerBlacklistid { get; set; }
        public int? Companyid { get; set; }
        public int? Customerid { get; set; }
        public DateTime? Dateblacklisted { get; set; }
        public string Reason { get; set; }
        public bool? Iscurrent { get; set; }

        public virtual TblCompany Company { get; set; }
    }
}
