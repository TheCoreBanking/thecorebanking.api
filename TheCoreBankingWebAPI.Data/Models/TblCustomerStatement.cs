﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCustomerStatement
    {
        public int Id { get; set; }
        public string DealId { get; set; }
        public string DealName { get; set; }
        public string MmDealId { get; set; }
        public string CpId { get; set; }
        public string Descr { get; set; }
        public decimal? Investment { get; set; }
        public decimal? Interest { get; set; }
        public decimal? Withdrawal { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public DateTime? TransDate { get; set; }
        public bool? Paid { get; set; }
        public string DoneBy { get; set; }
    }
}
