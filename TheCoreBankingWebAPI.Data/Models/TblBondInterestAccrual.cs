﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBondInterestAccrual
    {
        public int BondInteresId { get; set; }
        public string DealId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? NoOfDay { get; set; }
        public decimal? Principal { get; set; }
        public double? DailyInterst { get; set; }
        public double? AccrualTodate { get; set; }
        public decimal? InterestAmount { get; set; }
        public int? Daycount { get; set; }
        public bool? Matured { get; set; }
        public DateTime? CurrentDate { get; set; }
    }
}
