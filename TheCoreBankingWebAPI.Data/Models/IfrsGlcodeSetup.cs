﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class IfrsGlcodeSetup
    {
        public int Id { get; set; }
        public string Glcode { get; set; }
        public string BranchCode { get; set; }
        public string CompanyCode { get; set; }
        public string FscaptionCode { get; set; }
    }
}
