﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblSector
    {
        public TblSector()
        {
            TblCustomer = new HashSet<TblCustomer>();
            TblIndustry = new HashSet<TblIndustry>();
        }

        public int Sectorid { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool Isdeleted { get; set; }

        public virtual ICollection<TblCustomer> TblCustomer { get; set; }
        public virtual ICollection<TblIndustry> TblIndustry { get; set; }
    }
}
