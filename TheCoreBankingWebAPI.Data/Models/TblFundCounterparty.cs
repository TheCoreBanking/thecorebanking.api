﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblFundCounterparty
    {
        public int Id { get; set; }
        public string CpId { get; set; }
        public string CpName { get; set; }
        public string CpAddress { get; set; }
        public string CpPhone { get; set; }
        public string CpMobile { get; set; }
        public string CpEmail { get; set; }
        public string CpFax { get; set; }
        public string CpContactPerson { get; set; }
        public string CpContactPhone { get; set; }
        public string CpContactMobile { get; set; }
        public string CpContactEmail { get; set; }
        public decimal? CpAgreedComm { get; set; }
        public string CpType { get; set; }
        public string EmpId { get; set; }
        public string MisempId { get; set; }
        public string CreatedBy { get; set; }
        public bool? OverridePlacement { get; set; }
        public bool? OverrideTier { get; set; }
        public string Industry { get; set; }
        public string Sector { get; set; }
        public string Tier { get; set; }
        public string CoyCode { get; set; }
        public string ShortCode { get; set; }
        public string CustomerCategory { get; set; }
        public string CustomerType { get; set; }
        public bool? InterBank { get; set; }
    }
}
