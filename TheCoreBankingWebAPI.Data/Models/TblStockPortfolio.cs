﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblStockPortfolio
    {
        public int Id { get; set; }
        public string Portfolio { get; set; }
        public string Description { get; set; }
        public string Createdby { get; set; }
        public DateTime Createddate { get; set; }
    }
}
