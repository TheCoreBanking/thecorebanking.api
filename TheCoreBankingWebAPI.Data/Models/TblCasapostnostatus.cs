﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCasapostnostatus
    {
        public int Postnostatusid { get; set; }
        public string Postnostatusname { get; set; }
    }
}
