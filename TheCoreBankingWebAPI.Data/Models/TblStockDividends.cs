﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblStockDividends
    {
        public int DividendId { get; set; }
        public DateTime? EntryDate { get; set; }
        public string Symbol { get; set; }
        public string Security { get; set; }
        public decimal? DividendAmount { get; set; }
        public string DividendType { get; set; }
        public DateTime? ClosureDate { get; set; }
        public string Year { get; set; }
        public bool PostingFlag { get; set; }
        public string MadeBy { get; set; }
    }
}
