﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblChequebooktype
    {
        public TblChequebooktype()
        {
            TblChequebookdetail = new HashSet<TblChequebookdetail>();
        }

        public int Id { get; set; }
        public string Chequetype { get; set; }
        public decimal? Charge { get; set; }
        public int Leavesno { get; set; }
        public string Remark { get; set; }
        public string Creditledger { get; set; }
        public bool Isdeleted { get; set; }
        public int? Noissued { get; set; }
        public int? Lastissuedrange { get; set; }
        public bool Isapproved { get; set; }
        public bool Isdisapproved { get; set; }
        public DateTime? Dateapproved { get; set; }
        public string Approvalstatus { get; set; }
        public int? Copyfileid { get; set; }
        public bool Isnewlycreated { get; set; }
        public bool Deleteflag { get; set; }

        public virtual ICollection<TblChequebookdetail> TblChequebookdetail { get; set; }
    }
}
