﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblStockSectorPriceHistory
    {
        public int SpHid { get; set; }
        public DateTime SpDayDate { get; set; }
        public string SpSymbol { get; set; }
        public decimal SpPrice { get; set; }
        public int SpTrade { get; set; }
        public int SpVolume { get; set; }
        public int? SpSector { get; set; }
    }
}
