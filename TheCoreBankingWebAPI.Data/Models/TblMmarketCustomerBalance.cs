﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMmarketCustomerBalance
    {
        public int Id { get; set; }
        public string DealId { get; set; }
        public string CpName { get; set; }
        public decimal? AmountBalance { get; set; }
        public decimal? IntAccrued2Date { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public string MmDealName { get; set; }
        public decimal? Rate { get; set; }
        public int? Tenor { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public decimal? MaturityAmount { get; set; }
        public string CpId { get; set; }
        public decimal? FaceValue { get; set; }
        public string MmDealId { get; set; }
        public int? NumberOfDaysRun { get; set; }
        public decimal? Wht { get; set; }
        public decimal? GrossInterestAmount { get; set; }
        public decimal? NetInterestAmount { get; set; }
        public decimal? WhtatMaturity { get; set; }
        public decimal? GrossMaturityAmount { get; set; }
        public decimal? GrossInterestAccrued { get; set; }
        public decimal? InterestInArchive { get; set; }
        public string UnitCode { get; set; }
        public decimal? PenalCharge { get; set; }
    }
}
