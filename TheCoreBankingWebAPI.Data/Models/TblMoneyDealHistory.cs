﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyDealHistory
    {
        public int Id { get; set; }
        public string DealId { get; set; }
        public string MmDealId { get; set; }
        public string CpId { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public decimal? MaturityAmount { get; set; }
        public decimal? PrincipalAmount { get; set; }
        public decimal? NewPrincipalAmount { get; set; }
        public decimal? PrincipalDifferrence { get; set; }
        public decimal? InterestAmount { get; set; }
        public decimal? Discount { get; set; }
        public int? Tenor { get; set; }
        public decimal? InterestRate { get; set; }
        public decimal? EffectiveYield { get; set; }
        public string CoyCode { get; set; }
        public string BranchCode { get; set; }
        public string Operation { get; set; }
        public bool? Approved { get; set; }
    }
}
