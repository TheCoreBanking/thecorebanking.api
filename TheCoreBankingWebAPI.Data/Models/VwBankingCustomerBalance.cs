﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwBankingCustomerBalance
    {
        public string AccountName { get; set; }
        public string Accountnumber { get; set; }
        public int Iscurrentaccount { get; set; }
        public string Oldproductaccountnumber1 { get; set; }
        public string Oldproductaccountnumber2 { get; set; }
        public string Oldproductaccountnumber3 { get; set; }
        public int IsGl { get; set; }
        public int? Branchid { get; set; }
        public string Customercode { get; set; }
        public decimal? Rate { get; set; }
        public DateTime? Effectivedate { get; set; }
        public DateTime? Terminaldate { get; set; }
        public string Productcode { get; set; }
    }
}
