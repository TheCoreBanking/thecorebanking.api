﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblCustomeraddress
    {
        public int Addressid { get; set; }
        public int Customerid { get; set; }
        public int Stateid { get; set; }
        public string City { get; set; }
        public int Countryid { get; set; }
        public short Addresstypeid { get; set; }
        public string Address { get; set; }
        public string Hometown { get; set; }
        public string Pobox { get; set; }
        public string Nearestlandmark { get; set; }
        public string Electricmeternumber { get; set; }
        public bool? Active { get; set; }

        public virtual TblCustomeraddresstype Addresstype { get; set; }
        public virtual TblCountry Country { get; set; }
        public virtual TblState State { get; set; }
    }
}
