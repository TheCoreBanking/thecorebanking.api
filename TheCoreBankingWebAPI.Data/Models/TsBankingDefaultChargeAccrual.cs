﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TsBankingDefaultChargeAccrual
    {
        public int Id { get; set; }
        public string ProductAcctNo { get; set; }
        public string CustCode { get; set; }
        public decimal? Principal { get; set; }
        public string AccountNo { get; set; }
        public decimal? IaccrualToDate { get; set; }
        public decimal? Iptday { get; set; }
        public decimal? Iptdate { get; set; }
        public int? DefaultDays { get; set; }
        public DateTime? DefaultDate { get; set; }
        public decimal? DefaulRate { get; set; }
        public DateTime? LastEod { get; set; }
        public bool? Active { get; set; }
    }
}
