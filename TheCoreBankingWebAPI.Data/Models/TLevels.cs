﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TLevels
    {
        public int LevelsId { get; set; }
        public int? GroupsId { get; set; }
        public string LevelName { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsEditable { get; set; }
        public string CoyId { get; set; }
        public string Branch { get; set; }
        public DateTime? LastUpdate { get; set; }
        public bool? IsRest { get; set; }
        public bool? IsAdjustment { get; set; }
        public bool? IsRisk { get; set; }
        public bool DoChecklist { get; set; }
        public int Sla { get; set; }
    }
}
