﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwMmarketMaturity
    {
        public string MmDealId { get; set; }
        public string CpId { get; set; }
        public decimal? DiscountedValue { get; set; }
        public decimal? MaturityAmt { get; set; }
        public decimal? Wht { get; set; }
        public decimal? PrincipalAmount { get; set; }
        public decimal? Iprincipal { get; set; }
        public decimal? InterestAmount { get; set; }
        public string SettlementAccount { get; set; }
        public string CoyCode { get; set; }
        public string BranchCode { get; set; }
        public int? Productcategoryid { get; set; }
        public string MmDealName { get; set; }
        public decimal? EffectiveYield { get; set; }
        public decimal? InterestRate { get; set; }
        public int? Tenor { get; set; }
        public decimal? Discount { get; set; }
        public DateTime? MaturityDate { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public short? Productclassid { get; set; }
        public string CpName { get; set; }
        public int Id { get; set; }
        public string Customercode { get; set; }
        public string DealCreatedby { get; set; }
        public string StaffNo { get; set; }
        public int? Producttypeid { get; set; }
        public string PdType { get; set; }
        public string UnitCode { get; set; }
        public string DealId { get; set; }
        public decimal? IaccrualTodate { get; set; }
        public decimal? MaturityAmount { get; set; }
        public decimal? GrossInterest { get; set; }
        public decimal? SuspendedInterestAmount { get; set; }
        public decimal? TaxCharge { get; set; }
        public decimal? PenalCharge { get; set; }
        public decimal? WhtonInterestArchived { get; set; }
        public string TerminateAcctNo { get; set; }
        public bool? AutoRollover { get; set; }
        public int? DaysForRollover { get; set; }
        public int? Principalbalancegl { get; set; }
        public int? Interestincomeexpensegl { get; set; }
        public int? Interestreceivablepayablegl { get; set; }
        public int Customeraccounttypeid { get; set; }
        public string PrincipalGl { get; set; }
        public decimal? NewInterestAmount { get; set; }
    }
}
