﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblOperationsLevel
    {
        public int OperationId { get; set; }
        public string Operations { get; set; }
        public int? ApprovalLevels { get; set; }
        public bool? SetupStatus { get; set; }
    }
}
