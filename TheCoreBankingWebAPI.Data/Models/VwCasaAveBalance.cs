﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class VwCasaAveBalance
    {
        public string Accountnumber { get; set; }
        public decimal? Rate { get; set; }
        public bool Tod { get; set; }
        public bool Iscurrentaccount { get; set; }
        public DateTime? Overdraftexpirydate { get; set; }
        public string AccountName { get; set; }
        public int? Principalbalancegl { get; set; }
        public int? Interestincomeexpensegl { get; set; }
        public int? Interestreceivablepayablegl { get; set; }
        public string Customercode { get; set; }
        public string ProductCode { get; set; }
        public int? BranchId { get; set; }
        public int? Companyid { get; set; }
        public string Productname { get; set; }
    }
}
