﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblMoneyAmountCollected
    {
        public string DealId { get; set; }
        public decimal? Amount { get; set; }
        public string UserName { get; set; }
        public string CoyCode { get; set; }
        public string BrCode { get; set; }
        public string CpId { get; set; }
        public string CrossCheque { get; set; }
        public string Bank { get; set; }
        public string Instruction { get; set; }
        public decimal? Rtgscharge { get; set; }
        public string AccountNo { get; set; }
    }
}
