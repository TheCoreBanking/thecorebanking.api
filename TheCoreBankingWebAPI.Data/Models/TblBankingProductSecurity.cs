﻿using System;
using System.Collections.Generic;

namespace TheCoreBankingWebAPI.Data.Models
{
    public partial class TblBankingProductSecurity
    {
        public int PdSecId { get; set; }
        public string PdSecurity { get; set; }
        public decimal? HairCut { get; set; }
        public DateTime? ModDate { get; set; }
        public bool? Active { get; set; }
        public string Remark { get; set; }
        public string CreatedBy { get; set; }
        public string SecType { get; set; }
        public string PdCode { get; set; }
        public decimal? SecurityCoverage { get; set; }
        public bool? Approved { get; set; }
        public bool? DisApproved { get; set; }
        public string Comment { get; set; }
        public string ApprovedBy { get; set; }
    }
}
