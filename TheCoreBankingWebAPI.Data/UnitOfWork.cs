﻿using System;
using TheCoreBankingWebAPI.Data.Contracts;
using TheCoreBankingWebAPI.Data.Helpers;
using TheCoreBankingWebAPI.Data.Models;
using TheCoreBankingWebAPI.Data.Repository;

namespace TheCoreBankingWebAPI.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TheCoreBankingAzureContext DbContext = new TheCoreBankingAzureContext();

        private IRepositoryProvider RepositoryProvider { get; }

        public UnitOfWork(IRepositoryProvider repositoryProvider)
        {
            repositoryProvider.DbContext = DbContext;
            RepositoryProvider = repositoryProvider;
        }

        #region Repository definition
        public IAdminRepository Admin { get { return GetEntityRepository<IAdminRepository>(); } }

        public ICreditRepository Credit { get    { return GetEntityRepository<ICreditRepository > (); } }
        public ICustomerRepository Customer { get { return GetEntityRepository<ICustomerRepository>(); } }

        public IRetailRepository Retail { get { return GetEntityRepository<IRetailRepository>(); } }

        public IApprovalRepository  Approval { get { return GetEntityRepository<IApprovalRepository>(); } }

        public ITreasuryRepository Treasury { get { return GetEntityRepository<ITreasuryRepository>(); } }
        public IFinanceRepository Finance  { get { return GetEntityRepository<IFinanceRepository>(); } }

        public ISetupRepository Setup { get { return GetEntityRepository<ISetupRepository>(); } }

        public IProductRepository Product { get { return GetEntityRepository<IProductRepository>(); } }

        #endregion

        public void Commit()
        {
            DbContext.SaveChangesAsync();
        }

        private IRepository<T> GetStandardRepository<T>() where T : class
        {
            return RepositoryProvider.GetRepositoryForEntityType<T>();
        }

        private T GetEntityRepository<T>() where T : class
        {
            return RepositoryProvider.GetRepository<T>();
        }

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (DbContext != null)
                {
                    DbContext.Dispose();
                }
            }
        }

        #endregion
    }
}

