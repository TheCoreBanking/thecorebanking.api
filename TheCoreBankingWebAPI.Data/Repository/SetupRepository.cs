﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheCoreBankingWebAPI.Data.Contracts;
using TheCoreBankingWebAPI.Data.Models;
using TheCoreBankingWebAPI.Data.ViewModels;

namespace TheCoreBankingWebAPI.Data.Repository
{
    class SetupRepository: EFRepository<TblCasa>, ISetupRepository
    {
        private readonly TheCoreBankingAzureContext Db;
        public SetupRepository(TheCoreBankingAzureContext context) : base(context) {
            Db = context;
        
        }

        public async Task<TblCompanyInformation> GetcompanyById(int Id)
        {

            return await Db.TblCompanyInformation.FindAsync(Id);
        }

        public async Task<TblDirectorInformation> GetDirectorByID(int Id)
        {

            return await Db.TblDirectorInformation.FindAsync(Id);
        }

        public async Task<TblBranchInformation> GeBranchByid(string BranchId)
        {

            return await Db.TblBranchInformation.FindAsync(BranchId);
        }


        public async Task<List<TblCustomer>> GetAllCustomers()
        {

           return await Db.TblCustomer.Take(20).ToListAsync();

        }

        public async Task<List<TblCompanyInformation>> Listcompanys()
        {

            return await Db.TblCompanyInformation.Take(20).ToListAsync();
        }


        public async Task<List<TblBranchInformation>> ListBranch()
        {

            return await Db.TblBranchInformation.Take(20).ToListAsync();
        }

        public async Task<bool> VerifycompanyCode(string CompanyCode)
        {

           var Result = await Db.TblCompanyInformation.Where(x => x.CoyCode == CompanyCode).SingleOrDefaultAsync();

            if(Result == null) { return false; }else { return true; }
        }


        public async Task<bool> VerifyBranchCode( string BranchID)
        {

         var Result =  await Db.TblBranchInformation.Where(x => x.BrId == BranchID).SingleOrDefaultAsync();

            if(Result == null) { return false; } else {return true; }
        }

        public async Task<List<TblDirectorInformation>> ListDirector()
        {

            return await Db.TblDirectorInformation.Take(20).ToListAsync();
        }    
        
        public async Task<List<TblDepartment>> ListDepartment()
        {

            return await Db.TblDepartment.Take(20).ToListAsync();
        } 
        public async Task<List<TblUnit>> ListUnit()
        {

            return await Db.TblUnit.Take(20).ToListAsync();
        }

        public async Task<List<GeneralSetupTblOperationComment>> ListNaration()
        {

            return await Db.GeneralSetupTblOperationComment.Take(20).ToListAsync();
        }

        public async Task<bool> AddDirector(AddDirectorVm model)
        {
            TblDirectorInformation Director = new TblDirectorInformation { 
            
                Bvn = model.Bvn,
                CompanyId =model.CompanyId,
                FullName = model.FullName,
                PercentageShare = model.PercentageShare,
                Position = model.Position,
                
            
            };
           await Db.TblDirectorInformation.AddAsync(Director);

           if( await Db.SaveChangesAsync()> 0)
            { return true; }
            else { return false; }
        }

        public async Task<bool> UpdateDirector( int Id, UpdateDirectorVm model)
        {
           TblDirectorInformation DirectortoUpdate = await Db.TblDirectorInformation.FindAsync(Id);

            if(DirectortoUpdate == null) { return false; }

            try
            {


                DirectortoUpdate.Bvn = model.Bvn;
                DirectortoUpdate.CompanyId = model.CompanyId;
                DirectortoUpdate.FullName = model.FullName;
                DirectortoUpdate.PercentageShare = model.PercentageShare;
                DirectortoUpdate.Position = model.Position;


                await Db.SaveChangesAsync();
                return true;

            }
            catch (DbUpdateConcurrencyException)
            {

                return false;
            }


        }

        public async  Task<bool> Addcompany(AddCompanyVm model) {

            TblCompanyInformation companyInformation = new TblCompanyInformation {
                AccountingStandard = model.AccountingStandard,
                AccountStand = model.AccountStand,
                Address = model.Address,
                Approved = model.Approved,
                AuthorisedShareCapital = model.AuthorisedShareCapital,
                Comment = model.Comment,
                CompanyClass = model.CompanyClass,
                CompanyType = model.CompanyType,
                CoyClass = model.CoyClass,
                CoyCode = model.CoyCode,
                CoyId = model.CoyId,
                CoyName = model.CoyName,
                CoyRegisteredBy = model.CoyRegisteredBy,
                DateOfCommencement = model.DateOfCommencement,
                DateOfIncorporation = model.DateOfIncorporation,
                DateOfRenewalOfRegistration = model.DateOfRenewalOfRegistration,
                Deleted = model.Deleted,
                Disapproved = model.Disapproved,
                Email = model.Email,
                EoyprofitAndLossGl = model.EoyprofitAndLossGl,
                Fax = model.Fax,
                FormerManagersTrustees = model.FormerManagersTrustees,
                FunctionsRegistered = model.FunctionsRegistered,
                InitialFloatation = model.InitialFloatation,
                InvestmentObjective = model.InvestmentObjective,
                InitialSubscription = model.InitialSubscription,
                ManagementType = model.ManagementType,
                Manager = model.Manager,
                MgtType = model.MgtType,
                NameOfRegistrar = model.NameOfRegistrar,
                NameOfScheme = model.NameOfScheme,
                NameOfTrustees = model.NameOfTrustees,
                NatureOfBusiness = model.NatureOfBusiness,
                Telephone = model.Telephone,
                TrusteesAddress = model.TrusteesAddress,
                Webbsite = model.Webbsite,

               
            
                

            };
            await Db.TblCompanyInformation.AddAsync(companyInformation);

            if (await Db.SaveChangesAsync() > 0)
            { return true; }
            else { return false; }

        }
        public async Task<bool> Updatecompany(int Id, UpdateCompanyVm model) {

            TblCompanyInformation CompanytoUpdate = await Db.TblCompanyInformation.FindAsync(Id);

            if (CompanytoUpdate == null) { return false; }

            try
            {

                CompanytoUpdate.AccountingStandard = model.AccountingStandard;
                CompanytoUpdate.AccountStand = model.AccountStand;
                CompanytoUpdate.Address = model.Address;
                CompanytoUpdate.Approved = model.Approved;
                CompanytoUpdate.AuthorisedShareCapital = model.AuthorisedShareCapital;
                CompanytoUpdate.Comment = model.Comment;
               CompanytoUpdate.CompanyClass = model.CompanyClass;
                CompanytoUpdate.CompanyType = model.CompanyType;
                CompanytoUpdate.CoyClass = model.CoyClass;
                CompanytoUpdate.CoyCode = model.CoyCode;
                CompanytoUpdate.CoyId = model.CoyId;
                CompanytoUpdate.CoyName = model.CoyName;
                CompanytoUpdate.CoyRegisteredBy = model.CoyRegisteredBy;
                CompanytoUpdate.DateOfCommencement = model.DateOfCommencement;
                CompanytoUpdate.DateOfIncorporation = model.DateOfIncorporation;
                CompanytoUpdate.DateOfRenewalOfRegistration = model.DateOfRenewalOfRegistration;
                CompanytoUpdate.Deleted = model.Deleted;
                CompanytoUpdate.Disapproved = model.Disapproved;
                CompanytoUpdate.Email = model.Email;
                CompanytoUpdate.EoyprofitAndLossGl = model.EoyprofitAndLossGl;
                CompanytoUpdate.Fax = model.Fax;
                CompanytoUpdate.FormerManagersTrustees = model.FormerManagersTrustees;
                CompanytoUpdate.FunctionsRegistered = model.FunctionsRegistered;
                CompanytoUpdate.InitialFloatation = model.InitialFloatation;
                CompanytoUpdate.InvestmentObjective = model.InvestmentObjective;
                CompanytoUpdate.InitialSubscription = model.InitialSubscription;
               CompanytoUpdate.ManagementType = model.ManagementType;
                CompanytoUpdate.Manager = model.Manager;
                CompanytoUpdate.MgtType = model.MgtType;
                CompanytoUpdate.NameOfRegistrar = model.NameOfRegistrar;
                CompanytoUpdate.NameOfScheme = model.NameOfScheme;
                CompanytoUpdate.NameOfTrustees = model.NameOfTrustees;
                CompanytoUpdate.NatureOfBusiness = model.NatureOfBusiness;
                CompanytoUpdate.Telephone = model.Telephone;
                CompanytoUpdate.TrusteesAddress = model.TrusteesAddress;
                CompanytoUpdate.Webbsite = model.Webbsite;
                


                await Db.SaveChangesAsync();
                return true;

            }
            catch (DbUpdateConcurrencyException)
            {

                return false;
            }
        }

        public async Task<TblDirectorInformation> GetDirectorByBvn(string Bvn)
        {
            return await Db.TblDirectorInformation.Where(x => x.Bvn == Bvn).SingleOrDefaultAsync();

        }

        public async Task<bool> DeleteNaration(int Id)
        {
            GeneralSetupTblOperationComment Naration = await Db.GeneralSetupTblOperationComment.Where(x => x.Id == Id).SingleOrDefaultAsync();
            if(Naration == null) { return false; }

            try {

                 Db.GeneralSetupTblOperationComment.Remove(Naration);
                await  Db.SaveChangesAsync();
                return true;
               
            
            }
            catch (Exception)
            {
                return false;
            }

        }

        public async Task<bool> AddNaration(NarationVm model)
        {

            GeneralSetupTblOperationComment Naration = new GeneralSetupTblOperationComment { 
            
                 BrCode = model.BrCode,
                  Comment = model.Comment,
                   CoyCode = model.CoyCode,
                    CreatedBy = model.CreatedBy,
                     
            
            };

             await Db.GeneralSetupTblOperationComment.AddAsync(Naration);
            if (await Db.SaveChangesAsync() > 0)
            { return true; }
            else { return false; }

        }

        public async Task<bool> AddMis(AddMisVm model)
        {

           TblMisinformation misinformation = new TblMisinformation
            {
                CompanyCode = model.CompanyCode,
                 DateCreated = model.DateCreated,
                  Deleted = model.Deleted,
                   MisCode = model.MisCode,
                    MisName = model.MisName,
                     MisTypeId = model.MisTypeId,
                      ParentMisCode = model.ParentMisCode,
                      

            };

            await Db.TblMisinformation.AddAsync(misinformation);
            if (await Db.SaveChangesAsync() > 0)
            { return true; }
            else { return false; }

        }

        public async Task<bool> UpdateMis(int Id, UpdateMisVm model)
        {

            TblMisinformation MistoUpdate = await Db.TblMisinformation.FindAsync(Id);

            if (MistoUpdate == null) { return false; }

            try
            {
                MistoUpdate.CompanyCode = model.CompanyCode;
                MistoUpdate.DateCreated = model.DateCreated;
                MistoUpdate.Deleted = model.Deleted;
                MistoUpdate.MisCode = model.MisCode;
                MistoUpdate.MisName = model.MisName;
                MistoUpdate.MisTypeId = model.MisTypeId;
                MistoUpdate.ParentMisCode = model.ParentMisCode;
               



                await Db.SaveChangesAsync();
                return true;

            }
            catch (DbUpdateConcurrencyException)
            {

                return false;
            }
        }

        public async Task<TblMisinformation> GetMisById(int Id)
        {

            return await Db.TblMisinformation.FindAsync(Id);
        }

        public async Task<bool> AddBranch(AddbranchVm model)
        {

           TblBranchInformation  branchInformation = new TblBranchInformation
            {
                BrAddress = model.BrAddress,
                  BrLocation = model.BrLocation,
                   BrManager = model.BrManager,
                    BrName = model.BrName,
                     BrState = model.BrState,
                      CoyId = model.CoyId,
                       Deleted = model.Deleted,
                        BrId = model.BrId
             



            };

            await Db.TblBranchInformation.AddAsync(branchInformation);
            if (await Db.SaveChangesAsync() > 0)
            { return true; }
            else { return false; }

        }

        public async Task<bool> UpdateBranch(int Id, UpdatebranchVm model)
        {

            TblBranchInformation BranchtoUpdate = await Db.TblBranchInformation.FindAsync(Id);

            if (BranchtoUpdate == null) { return false; }

            try
            {

                BranchtoUpdate.BrAddress = model.BrAddress;
                BranchtoUpdate.BrLocation = model.BrLocation;
                BranchtoUpdate.BrManager = model.BrManager;
                BranchtoUpdate.BrName = model.BrName;
                     BranchtoUpdate.BrState = model.BrState;
                BranchtoUpdate.CoyId = model.CoyId;
                BranchtoUpdate.Deleted = model.Deleted;
                BranchtoUpdate.BrId = model.BrId;


                await Db.SaveChangesAsync();
                return true;

            }
            catch (DbUpdateConcurrencyException)
            {

                return false;
            }
        }

        public async Task<TblBranchInformation> GetBranchId(int Id)
        {

            return await Db.TblBranchInformation.FindAsync(Id);
        }


        public async Task<bool> AddDepartment(DepartmentVm model)
        {

          TblDepartment  department = new TblDepartment
            {
                Branch = model.Branch,
                 Branchid = model.Branchid,
                  Companyid = model.Companyid,
                   Department = model.Department,
                    Departmentcode = model.Departmentcode,
                     Unitcode = model.Unitcode,
                      Unitname = model.Unitname



            };

            await Db.TblDepartment.AddAsync(department);
            if (await Db.SaveChangesAsync() > 0)
            { return true; }
            else { return false; }

        }

        public async Task<bool> AddDesignation(DesignationVm model)
        {

           TblDesignation  designation = new TblDesignation
            {
                Designation = model.Designation,
                 DesignationCode = model.DesignationCode,
                  
            




            };

            await Db.TblDesignation.AddAsync(designation);
            if (await Db.SaveChangesAsync() > 0)
            { return true; }
            else { return false; }

        }

        public async Task<bool> AddUnit(AddUnitVm model)
        {

           TblUnit  unit = new TblUnit
            {
                
                BrCode = model.BrCode,
                 CoyCode = model.CoyCode,
                  CreatedBy = model.CreatedBy,
                   DateCreated = model.DateCreated,
                    IsDeleted = model.IsDeleted,
                     Remark = model.Remark,
                      UnitCode = model.UnitCode,
                       UnitName = model.UnitName,
                        




            };

            await Db.TblUnit.AddAsync(unit);
            if (await Db.SaveChangesAsync() > 0)
            { return true; }
            else { return false; }

        }

        public async Task<bool> UpdateUnit(int Id, UpdateUnitVm model)
        {

            TblUnit unittoUpdate = await Db.TblUnit.FindAsync(Id);

            if (unittoUpdate == null) { return false; }

            try
            {


                unittoUpdate.BrCode = model.BrCode;
                unittoUpdate.CoyCode = model.CoyCode;
                  unittoUpdate.CreatedBy = model.CreatedBy;
                   unittoUpdate.DateCreated = model.DateCreated;
                    unittoUpdate.IsDeleted = model.IsDeleted;
                     unittoUpdate.Remark = model.Remark;
                      unittoUpdate.UnitCode = model.UnitCode;
                       unittoUpdate.UnitName = model.UnitName;


                await Db.SaveChangesAsync();
                return true;

            }
            catch (DbUpdateConcurrencyException)
            {

                return false;
            }
        }


        public async Task<TblUnit> GetUnitById(int Id)
        {

            return await Db.TblUnit.FindAsync(Id);
        }

    }
}
