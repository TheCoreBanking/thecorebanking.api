﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheCoreBankingWebAPI.Data.Contracts;
using TheCoreBankingWebAPI.Data.Models;
using TheCoreBankingWebAPI.Data.ViewModels;

namespace TheCoreBankingWebAPI.Data.Repository
{
    class CreditRepository: EFRepository<TblCasa>, ICreditRepository
    {
        private TheCoreBankingAzureContext Db;
        public CreditRepository(TheCoreBankingAzureContext context) : base(context) {
            Db = context;
        
        }

        public async Task<List<TblCustomer>> GetAllCustomers()
        {

           return await Db.TblCustomer.Take(20).ToListAsync();

        }

        public async Task<List<TblBankingOperationSetup>> ListOperation()
        {

            return await Db.TblBankingOperationSetup.Take(20).ToListAsync();

        }

        public async Task<List<TblProduct>> CreditProduct()
        {

            return await Db.TblProduct.Where(x=>x.Producttypeid == 701 || x.Producttypeid == 708).Take(20).ToListAsync();
        }

        public async Task<List<TblBankingProductType>> LoanProductType()
        {

            return await Db.TblBankingProductType.Take(20).ToListAsync();
        }

        public async Task<List<TblBankingProductSecurity>> ProductSecurity()
        {

            return await Db.TblBankingProductSecurity.Take(20).ToListAsync();
        }

        public async Task<List<TblProductCategory>> LoanProductCatergory()
        {

            return await Db.TblProductCategory.Take(20).ToListAsync();
        }

        public async Task<List<TblBankingLoanApplication>> ApprovedLoanApplication()
        {

            return await Db.TblBankingLoanApplication.Where(o=>o.Approved == true && o.Booked == false).Take(20).ToListAsync();
        }

        public async Task<TblCasa> ProductAccountNumber(string ProductCode, int CustomerId)
        {
            var Customer = await Db.TblCasa.Where(x => x.Productcode == ProductCode && x.Customerid == CustomerId).SingleOrDefaultAsync();
            return Customer;
        }
        
        
       public async Task<int> GenerateLoanAppSchedule(LoanApplicationVM  loanApplication) 
        {


           try
           {

         
               using (var context = new TheCoreBankingAzureContext())
               {

                   SqlParameter _ProductAccNo = new SqlParameter("@ProductAcctNo", loanApplication.ProductAcctNo);
                    SqlParameter _ProductTyeId = new SqlParameter("@pdTypeID", loanApplication.PdTypeId);
                    SqlParameter _Product_Name = new SqlParameter("@PrdName", loanApplication.ProductName);
                    SqlParameter _ProductCode = new SqlParameter("@ProdCode", loanApplication.ProductCode);
                    SqlParameter _Name = new SqlParameter("@Name", loanApplication.CustName);
                    SqlParameter _Ref = new SqlParameter("@Ref", loanApplication.Ref);
                    SqlParameter _CurrentAccount = new SqlParameter("@CurrentAcct", loanApplication.CurrentAcct);

                    var Result = await context.Database.ExecuteSqlRawAsync(
                        "dbo.sp_Banking_CreditGlobalBalance @ProductAcctNo,@pdTypeID,@PrdName,@ProdCode,@Name,@Ref,@CurrentAcct",
                        _ProductAccNo ,_ProductTyeId, _Product_Name, _ProductCode, _Name, _Ref,_CurrentAccount);
                    return Result;

               }
           }
           catch (System.Exception ex)
           {

                return 0;
           }

       
    }

        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loanapplication"></param>
        /// <returns></returns>
        public async  Task<bool> AddLoanApplication(TblBankingLoanApplication model)
        {
            /*
            TblBankingLoanApplication loanApplication = new TblBankingLoanApplication {
                CustCode = model.CustCode,
                CustName = model.CustName,
                ProductCode = model.ProductCode,
                ProductAcctNo = model.ProductAcctNo,
                Ref = model.Ref,
                DateCreated = this.getSystemDate(),
                EffectiveDate = model.EffectiveDate,
                Email = model.Email,
                FeeCharge = model.FeeCharge,
                FeeFrequency = model.FeeFrequency,
                Principal = model.Principal,
                PrincipalFreqType = model.PrincipalFreqType,
                PrincipalFrequency = model.PrincipalFrequency,
                RelationshipManager = model.RelationshipManager,
                RelationshipManagerDept = model.RelationshipManagerDept,
                RelationshipOfficer = model.RelationshipOfficer,
                RelationshipOfficerDept = model.RelationshipOfficerDept,
                LegalReceived = model.LegalReceived,
                AgreeLoan = model.AgreeLoan,
                AmountPaid = model.AmountPaid,
                ApprovedAmount = model.ApprovedAmount,
                Status = 0,
                ApprovalComment = model.ApprovalComment,
                Approved = false,
                ApprovedBy = "Sys",
                DateApproved = model.DateApproved,
                CurrentAcct = model.CurrentAcct,
                Cam = model.Cam,
                CategoryId = model.CategoryId,
                CanDisburse = model.CanDisburse,
                Currency = model.Currency,
                CoyCode = model.CoyCode,
                CurrRate = model.CurrRate,
                DisChargeLetter = model.DisChargeLetter,
                Frequency = model.Frequency,



            
            };


            */
          await  Db.AddAsync(model);
            if (await Db.SaveChangesAsync() > 0) { return true; } else { return false; }

          
        }

        private  DateTime getSystemDate()
        {
            //   return db.ts_FinanceCurrentDate.Select(x => x.CurrentDate).FirstOrDefault();

              return Db.TblFinanceCurrentDate.Select(x => x.CurrentDate).FirstOrDefault();
        }

        public async Task<TblBankingLoanApplication> FindLoanApplication(int Id)
        {


            TblBankingLoanApplication loanApplication = await Db.TblBankingLoanApplication.FindAsync(Id);

            return loanApplication;
        }

        public async Task<TblBankingLoanLease> FindLoanBooking(int Id)
        {


          TblBankingLoanLease loanApplication = await Db.TblBankingLoanLease.FindAsync(Id);

            return loanApplication;
        }
        public async  Task<bool> UpdateLoanApplication(int Id, TblBankingLoanApplication model) {



            TblBankingLoanApplication loanApplication = await Db.TblBankingLoanApplication.FindAsync(Id);
            if (loanApplication == null) { return false; }
            else
            {
           
                if(Id != model.Id) { return false; }
                else
                {
                    try {

                       Db.Entry(model).State = EntityState.Modified;

                        await Db.SaveChangesAsync();
                        return true;
                    
                    }
                    catch (DbUpdateConcurrencyException) {

                        return false;
                    }

                }


            }

        }
        public async Task<bool> RemoveLoanApplication(int Id) {

            TblBankingLoanApplication loanApplicationtoremove = await Db.TblBankingLoanApplication.FindAsync(Id);
            if (loanApplicationtoremove == null) { return false; }
            else
            {
                Db.TblBankingLoanApplication.Remove(loanApplicationtoremove);


                if (await Db.SaveChangesAsync() > 0) { return true; } else { return false; }
            }


        }

        public async Task<bool> AddLoanProduct(LoanProductVM loanproduct) {

            TblProduct product = new TblProduct() {
            
                Productcode = loanproduct.Productcode,
                Productname = loanproduct.Productname,
                Producttypeid = loanproduct.Producttypeid,
                Premiumdiscountgl = loanproduct.Premiumdiscountgl,
                PrimeLending = loanproduct.PrimeLending,
                Principalbalancegl = loanproduct.Principalbalancegl,
                ProductBehaviourid = loanproduct.ProductBehaviourid,
                Productcategoryid = loanproduct.Productcategoryid,
                Productclassid = loanproduct.Productclassid,
                Productdescription = loanproduct.Productdescription,
                Productgroupid = loanproduct.Productgroupid,
                Productid = loanproduct.Productid,
                Productpriceindexid = loanproduct.Productpriceindexid,
                Productpriceindexspread = loanproduct.Productpriceindexspread,
                InterestPayment =loanproduct.InterestPayment,
                AccountType = loanproduct.AccountType,
                Allowcustomeraccountforcedebit = loanproduct.Allowcustomeraccountforcedebit,
                Allowmoratorium = loanproduct.Allowmoratorium,
                Allowoverdrawn = loanproduct.Allowoverdrawn,
                Allowrate = loanproduct.Allowrate,
                Allowscheduletypeoverride = loanproduct.Allowscheduletypeoverride,
                Allowtenor = loanproduct.Allowtenor,
                Companyid = loanproduct.Companyid,
                Cleanupperiod =loanproduct.Cleanupperiod,
                Datetimecreated = getSystemDate(),
                Dealtypeid = loanproduct.Dealtypeid,
                Dormantgl = loanproduct.Dormantgl,
                Expiryperiod = loanproduct.Expiryperiod,
                Equitycontribution = loanproduct.Equitycontribution,
                Maximumdrawdownduration = loanproduct.Maximumdrawdownduration,
                Maximumrate = loanproduct.Maximumrate,
                Maximumtenor = loanproduct.Maximumtenor,
                Minimumbalance = loanproduct.Minimumbalance,
                Minimumrate = loanproduct.Minimumrate,
                Minimumtenor = loanproduct.Minimumtenor,
                Approvedby = 001,
                Daycountconventionid = loanproduct.Daycountconventionid,
                Dealclassificationid = loanproduct.Dealclassificationid,
                Interestincomeexpensegl = loanproduct.Interestincomeexpensegl,
                Interestreceivablepayablegl = loanproduct.Interestreceivablepayablegl,
                Ismultiplecurency = loanproduct.Ismultiplecurency,
                Overdrawngl = loanproduct.Overdrawngl,
                Scheduletypeid = loanproduct.Scheduletypeid,
                SpreadRate = loanproduct.SpreadRate,
                Settlementdate = loanproduct.Settlementdate,
                Treasuryproducttypeid = loanproduct.Treasuryproducttypeid,
                Defaultgraceperiod = loanproduct.Defaultgraceperiod,
                Createdby = loanproduct.Createdby,
                Completed = loanproduct.Completed,
                
              };
            await Db.AddAsync(product);
            if (await Db.SaveChangesAsync() > 0) { return true; } else { return false; }


           }
        public async Task<bool> AddLoanBooking(TblBankingLoanLease model) {

        
            await Db.AddAsync(model);
            if (await Db.SaveChangesAsync() > 0) { return true; } else { return false; }


        }

        public async Task<bool> UpdateLoanBooking(int Id, TblBankingLoanLease loan) {

        TblBankingLoanLease loanlease = await Db.TblBankingLoanLease.FindAsync(Id);
            if (loanlease == null) { return false; }
            else
            {
                if(Id != loan.Id) { return false; }
                else
                {
                    try
                    {

                        Db.Entry(loan).State = EntityState.Modified;

                        await Db.SaveChangesAsync();
                        return true;

                    }
                    catch (DbUpdateConcurrencyException)
                    {

                        return false;
                    }
                }


               
            }
        }
        public async Task<bool> RemoveLoanBooking(int Id) {


            TblBankingLoanLease targetloan = await Db.TblBankingLoanLease.FindAsync(Id);
            if (targetloan == null) { return false; }
            else
            {
                Db.TblBankingLoanLease.Remove(targetloan);


                if (await Db.SaveChangesAsync() > 0) { return true; } else { return false; }
            }
        }
        public async Task<List<TblBankingTempSchedule>> ListGeneratedSchedule(string ProductAcctNo) {


            return await Db.TblBankingTempSchedule.Where(x => x.ProductAcctNo == ProductAcctNo).Take(20).ToListAsync(); }
        public async Task<bool> AddLoanRestructure(TblBankingRestructure model) {

      
            await Db.AddAsync(model);
            if (await Db.SaveChangesAsync() > 0) { return true; } else { return false; }


        }
        public async Task<bool> AddPrincipalReduction(TblBankingPrincipalReduction model)
        {
          
            await Db.AddAsync(model);
            if (await Db.SaveChangesAsync() > 0) { return true; } else { return false; }
        }
        public async Task<bool> AddPrincipalAddition(TblBankingPrincipalAddition model)
        {
           
            await Db.AddAsync(model);
            if (await Db.SaveChangesAsync() > 0) { return true; } else { return false; }
        }
        public async Task<bool> AddInterestSuspension( TblBankingInterestSuspension model)
        {
           
            await Db.AddAsync(model);
            if (await Db.SaveChangesAsync() > 0) { return true; } else { return false; }
        }
        public async Task<bool> AddLoanCancellation(TblBankingLoanCancelation model)
        {
          
            await Db.AddAsync(model);
            if (await Db.SaveChangesAsync() > 0) { return true; } else { return false; }
        }
        public async Task<bool> AddLoanTermination(TblBankingTermination model)
        {
          
            await Db.AddAsync(model);
            if (await Db.SaveChangesAsync() > 0) { return true; } else { return false; }
        }

    }
}
