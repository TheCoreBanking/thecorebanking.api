﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheCoreBankingWebAPI.Data.Contracts;
using TheCoreBankingWebAPI.Data.Models;
using TheCoreBankingWebAPI.Data.ViewModels;

namespace TheCoreBankingWebAPI.Data.Repository
{
    class AdminRepository: EFRepository<TblCasa>, IAdminRepository
    {
        private readonly TheCoreBankingAzureContext Db;
        public AdminRepository(TheCoreBankingAzureContext context) : base(context) {
            Db = context;
        
        }

        public async Task<List<TblCustomer>> GetAllCustomers()
        {

           return await Db.TblCustomer.Take(20).ToListAsync();

        }


        public async Task<TblCustomer> GetCustomerbyID( int id)
        {

            return await Db.TblCustomer.Where(x => x.Customerid == id).SingleOrDefaultAsync();

        }
      public async Task<List<TblAnnualincome>> LoadAnnualIncome()
        {
            return await Db.TblAnnualincome.Take(20).ToListAsync();
        }
        public async Task<List<TblCasa>> LoadAccounts()
        {

            return await dbSet.Take(20).ToListAsync();

        }
        public async Task<List<TblCustomeraccounttype>> LoadAccountType()
        {

            return await Db.TblCustomeraccounttype.Take(20).ToListAsync();

        }

      public async  Task<TblKycitem> LoadKycItem(int id)
        {

            return await Db.TblKycitem.Where(x => x.Kycitemid == id).SingleOrDefaultAsync();
        }

      public async  Task<TblCustomeraddress> LoadcustomerAddress(int id)
        {

            return await Db.TblCustomeraddress.Where(x => x.Customerid == id).SingleOrDefaultAsync();
        }

        public async Task<TblCustomerphonecontact> LoadcustomerPhone(int id)
        {

            return await Db.TblCustomerphonecontact.Where(x => x.Customerid == id).SingleOrDefaultAsync();
        }

        public async Task<List<TblSourceoffunds>> LoadFundSourcs()
        {

            return await Db.TblSourceoffunds.Take(20).ToListAsync();
        }

        public async  Task<TblCustomeremailcontact> LoadcustomerEmail(int id) {

            return await Db.TblCustomeremailcontact.Where(x => x.Customerid == id).SingleOrDefaultAsync();
        }


        public async Task<List<TblCasa>> LoadAccount() {

              return  await Db.TblCasa.Take(20).ToListAsync();
        
        }


        public async Task <List<TblCasa>> LoadDomantAccount() {

            return await Db.TblCasa.Where(x => x.Accountstatusid == 3).Take(20).ToListAsync();
        
        
        }

        public async Task<TblCasa> LoadAccountById(int id)
        {

            return await Db.TblCasa.Where(x => x.Casaaccountid == id).SingleOrDefaultAsync();

        }
        public async  Task<int> GenerateCustomerCode(long customerId)
        {


            int result = 0;
            using (var context = new TheCoreBankingAzureContext())
            {

                SqlParameter _CustomerId = new SqlParameter("@CustId", customerId);

                result =  await context.Database.ExecuteSqlRawAsync("[Customer].[sp_GenerateCustCode] @CustId", _CustomerId);

                return result;
            }



        }

        public async Task<string> GetProductCasaAccountId(string accountName)
        {

          var Customer = await  dbSet.Where(x => x.Accountname == accountName).SingleOrDefaultAsync();

            return Customer.Productid.ToString();
        }

        public async Task<List<TblAccountfreeze>> LoadFrozenAccounts() {

            return await Db.TblAccountfreeze.Where(x => x.IsReversed == false && x.IsApproved == false).ToListAsync();
        
        }

        public async Task<List <TblAccountcardtype>> LoadAccountCardTypes() {

            return await Db.TblAccountcardtype.Where(x => x.Active == true).Take(20).ToListAsync();

        }

        public async Task<TblAccountfreeze> ConfirmFreezeReversalStatus(string id)
        {

            return await Db.TblAccountfreeze.Where(x => x.AccountNumber == id && x.IsReversed == true).SingleOrDefaultAsync();
        }

        public async Task<TblAccountfreeze> ConfirmFreezeStatus(string id)
        {

            return await Db.TblAccountfreeze.Where(x => x.AccountNumber == id && x.IsReversed == false).SingleOrDefaultAsync();
        }

        public async Task<List<TblAccountclosure>> ListClosedAndApprovedAccounts() {

            return await Db.TblAccountclosure.Where(x => x.Approved == true).Take(20).ToListAsync();
        }

        public async Task<List<GlobalbalanceVM>> GlobalBalance(string customercode)
        {
            List<GlobalbalanceVM> global = new List<GlobalbalanceVM>();
            try
            {


                using (var context = new TheCoreBankingAzureContext())
                {

                    SqlParameter _CustCode = new SqlParameter("@CustCode", customercode);

                    return await context.GlobalbalanceVM.FromSqlRaw("dbo.sp_Banking_CreditGlobalBalance @CustCode", _CustCode).ToListAsync();
                    

                }
            }
            catch (Exception ex)
            {

                return global;
            }


        }
       

        public async  Task<List<TblFreezetransactionlist>> Loadfreezetransaction()
        {
            return await Db.TblFreezetransactionlist.ToListAsync();
        }

        public async Task<bool> AddCustomer(CustomerViewModel customer) {

            TblCustomer CustomertoAdd = new TblCustomer {

                Surname = customer.surname,
                Firstname = customer.firstname,
                Placeofbirth = customer.placeOfBirth,
                Branchcode = customer.branchcode,
                Branchid = customer.BranchId,
                Customeraccounttypeid = customer.customerType,
                Approvalstatus = 0,
                Actedonby = "Api",
                
         

               
               //  Maritalstatus = customer.maritalStatus,
            
            };

             await  Db.TblCustomer.AddAsync(CustomertoAdd);

           int result =  await  Db.SaveChangesAsync();

            if(result > 0) { return true; } else { return false;  }
        }

        public async Task<bool> UpdateCustomer( CustomerViewModel customer) {

         TblCustomer CustomertoUpdate = await Db.TblCustomer.Where(x => x.Customercode == customer.customerCode).SingleOrDefaultAsync();
            if(CustomertoUpdate == null) { return false; }
            else {

                CustomertoUpdate.Surname = customer.surname;
                CustomertoUpdate.Firstname = customer.firstname;
                int result = await Db.SaveChangesAsync();

                if (result > 0) { return true; } else { return false; }

            }


            

        }
    }
}
