﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheCoreBankingWebAPI.Data.Contracts;
using TheCoreBankingWebAPI.Data.Models;
using TheCoreBankingWebAPI.Data.ViewModels;

namespace TheCoreBankingWebAPI.Data.Repository
{
    class ProductRepository : EFRepository<TblCasa>, IProductRepository
    {
        private readonly TheCoreBankingAzureContext Db;
        public ProductRepository(TheCoreBankingAzureContext context) : base(context)
        {
            Db = context;

        }

        public async Task<List<TblCustomer>> GetAllCustomers()
        {

            return await Db.TblCustomer.Take(20).ToListAsync();

        }


        public async Task<List<TblProduct>> LoadProduct()
        {

            return await Db.TblProduct.Take(20).ToListAsync();

        }

        public async Task<bool> CreateProduct(AddProductVm model)
        {

            TblProduct product = new TblProduct
            {
                AccountType = model.AccountType,
                Allowcustomeraccountforcedebit = model.Allowcustomeraccountforcedebit,
                Allowmoratorium = model.Allowmoratorium,
                Allowoverdrawn = model.Allowoverdrawn,
                Allowrate = model.Allowrate,
                Allowscheduletypeoverride = model.Allowscheduletypeoverride,
                Allowtenor = model.Allowtenor,
                Approved = model.Approved,
                Approvedby = model.Approvedby,
                Cleanupperiod = model.Cleanupperiod,
                Companyid = model.Companyid,
                Completed = model.Completed,
                Createdby = model.Createdby,
                Datetimecreated = model.Datetimecreated,
                Datetimedeleted = model.Datetimedeleted,
                Datetimeupdated = model.Datetimeupdated,
                Daycountconventionid = model.Daycountconventionid,
                Dealclassificationid = model.Dealclassificationid,
                Dealtypeid = model.Dealtypeid,
                Defaultgraceperiod = model.Defaultgraceperiod,
                Deleted = model.Deleted,
                Deletedby = model.Deletedby,
                Dormantgl = model.Dormantgl,
                Equitycontribution = model.Equitycontribution,
                Expiryperiod = model.Expiryperiod,
                Interestincomeexpensegl = model.Interestincomeexpensegl,
                InterestPayment = model.InterestPayment,
                Interestreceivablepayablegl = model.Interestreceivablepayablegl,
                Ismultiplecurency = model.Ismultiplecurency,
                Lastupdatedby = model.Lastupdatedby,
                Maximumdrawdownduration = model.Maximumdrawdownduration,
                Maximumrate = model.Maximumrate,
                Maximumtenor = model.Maximumtenor,
                Minimumbalance = model.Minimumbalance,
                Minimumrate = model.Minimumrate,
                Minimumtenor = model.Minimumtenor,
                Overdrawngl = model.Overdrawngl,
                Premiumdiscountgl = model.Premiumdiscountgl,
                PrimeLending = model.PrimeLending,
                Principalbalancegl = model.Principalbalancegl,
                ProductBehaviourid = model.ProductBehaviourid,
                Productcategoryid = model.Productcategoryid,
                Productclassid = model.Productclassid,
                Productcode = model.Productcode,
                Productdescription = model.Productdescription,
                Productgroupid = model.Productgroupid,
                Productid = model.Productid,
                Productname = model.Productname,
                Productpriceindexid = model.Productpriceindexid,
                Productpriceindexspread = model.Productpriceindexspread,
                Producttypeid = model.Producttypeid,
                Scheduletypeid = model.Scheduletypeid,
                Settlementdate = model.Settlementdate,
                SpreadRate = model.SpreadRate,
                Treasuryproducttypeid = model.Treasuryproducttypeid,





            };

            await Db.AddAsync(product);
            if (await Db.SaveChangesAsync() > 0) { return true; } else { return false; }


        }

        public async Task<bool> CreateProductGroup(AddProductGroupVm model)
        {

            TblProductGroup product = new TblProductGroup()
            {
                Createdby = model.Createdby,
                Datetimecreated = model.Datetimecreated,
                Datetimedeleted = model.Datetimedeleted,
                Deleted = model.Deleted,
                Productgroupcode = model.Productgroupcode,
                Productgroupid = model.Productgroupid,
                Productgroupname = model.Productgroupname,

            };

            await Db.AddAsync(product);
            if (await Db.SaveChangesAsync() > 0) { return true; } else { return false; }
        }
        public async Task<bool> UpdateProductGroup(UpdateProductGroupVm model, int id)
        {

            TblProductGroup ProductToUpdate = await Db.TblProductGroup.FindAsync(id);

            if (ProductToUpdate == null) { return false; }
            else
            {
                ProductToUpdate.Createdby = model.Createdby;
                ProductToUpdate.Datetimecreated = model.Datetimecreated;
                ProductToUpdate.Datetimedeleted = model.Datetimedeleted;
                ProductToUpdate.Deleted = model.Deleted;
                ProductToUpdate.Productgroupcode = model.Productgroupcode;
                ProductToUpdate.Productgroupid = model.Productgroupid;
                ProductToUpdate.Productgroupname = model.Productgroupname;



                if (await Db.SaveChangesAsync() > 0) { return true; } else { return false; }

            }
        }

        public async Task<bool> CreateProductCatergory(AddProductCatergoryVm model)
        {

            TblProductCategory product = new TblProductCategory()
            {
                Productcategoryid = model.Productcategoryid,
                Productcategoryname = model.Productcategoryname,


            };
            await Db.AddAsync(product);
            if (await Db.SaveChangesAsync() > 0) { return true; } else { return false; }
        }
        public async Task<bool> UpdateProductCatergory(UpdateProductCatergoryVm model, int id)
        {

            TblProductCategory productCategoryToUpdate = await Db.TblProductCategory.FindAsync(id);
            if (productCategoryToUpdate == null) { return false; }
            else
            {
                productCategoryToUpdate.Productcategoryid = model.Productcategoryid;
                productCategoryToUpdate.Productcategoryname = model.Productcategoryname;

                if (await Db.SaveChangesAsync() > 0) { return true; } else { return false; }
            }
        }

        public async Task<List<CasaProductVm>> ListCasaProduct()
        {

            var CasaProduct = await (from a in Db.TblProduct

                                     join b in Db.TblBankingProductType on a.Producttypeid equals b.PdTypeId

                                     select new CasaProductVm { Product = a, ProductType = b }).Take(30).ToListAsync();
            return CasaProduct;



        }


        public async Task<List<FeeVm>> GetFeeNameByProductId(string ProductID)
        {

            try {
                var FeeName = await (from a in Db.TblBankingProductFeesList
                                     join b in Db.TblBankingProductFees on a.PdFeesId equals b.PdFeesId
                                     join x in Db.TblProduct on a.PdId equals Convert.ToInt32(x.Id)
                                     where (x.Id == Convert.ToInt64(ProductID))

                                     select new FeeVm { FeeName = b.PdFeesName }).ToListAsync();
                {
                    return FeeName;
                }
            }
            catch(Exception ex) {

                return null;
            }

           
        }

        public async Task<List<FeeVm>> GetFeeNameByProductName(string ProductName)
        {

            try
            {
                var FeeName = await (from a in Db.TblBankingProductFeesList
                                     join b in Db.TblBankingProductFees on a.PdFeesId equals b.PdFeesId
                                     join x in Db.TblProduct on a.PdId equals Convert.ToInt32(x.Id)
                                     where (x.Productname == ProductName)

                                     select new FeeVm { FeeName = b.PdFeesName }).ToListAsync();
                {
                    return FeeName;
                }
            }
            catch (Exception ex)
            {

                return null;
            }


        }
    }
}
