﻿namespace TheCoreBankingWebAPI.Data.Enums
{
    public enum ChequeLeafStatusEnum
    {
        STOPPED = 1,
        USED = 2
    }
}
