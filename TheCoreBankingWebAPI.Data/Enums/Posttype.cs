﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheCoreBankingWebAPI.Data.Enums
{
    public enum Posttype
    {
        ACCOUNT = 1,
        TRANSFER = 2,
        WITHDRAWAL = 3,
        REVISION = 4,
        SUSPENSION = 5,
        TERMINATION = 6,
        CANCELATION = 7,

        CASA = 8,
        CASADEPOSIT = 11,
        CASAWITHDRAWAL = 13,
        CASAFREEZE = 15,
        CASACLOSE = 17,
        CASATRANSFER = 19,
        LOANDISBURSEMENT = 20,
        LEASEDISBURSEMENT = 21,
        LOANRESTRUCTURE = 22,
        LEASERESTRUCTURE = 23,
        LOANTERMINATION = 24,
        LEASETERMINATION = 25,
        LOANREPAYMENT = 26,
        LEASEREPAYMENT = 27,
        LOANCHEQUELODGE = 28,
        LEASECHEQUELODGE = 29,
        LOANCHEQUERETURN = 30,
        LEASECHEQUERETURN = 31,
        LOANMONTHLYREPAYMENT = 32,
        LEASEMONTHLYREPAYMENT = 33,
        DEALREDISCOUNTING = 34,
        DEALINTSUSPENSION = 35,
        INTERBRANCHVAULT = 36,
        TILLTOVAULT = 37,
        VAULTTOTILL = 38,
        REMITTANCE = 39,
        CASACHEQUELODGMENT = 40,
        MMarket = 41,
        REDISCOUNT = 42,
        COT = 43,
        CASAREACTIVATEACCOUNT = 44,
        CASHTOVAULT = 45,
        JOURNAL = 46,
        SERVICEREQUEST = 47,
        CASAINTERESTACCURAL = 48,
        WHT = 49,
        VAT = 50,
        FHRMS = 51,
        ERMS = 52,
        VAULTTOCASH = 53,
        TITHEANDREMITTANCE = 129,

    }
}
