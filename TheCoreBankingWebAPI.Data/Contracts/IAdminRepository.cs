﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheCoreBankingWebAPI.Data.Models;
using TheCoreBankingWebAPI.Data.ViewModels;

namespace TheCoreBankingWebAPI.Data.Contracts
{
    public interface IAdminRepository
    {
        Task<List<TblCustomer>> GetAllCustomers();
        Task<List<TblCustomeraccounttype>> LoadAccountType();

        Task<List<TblCasa>> LoadAccounts();
        Task<List<TblSourceoffunds>> LoadFundSourcs();
        Task<List<TblAnnualincome>> LoadAnnualIncome();
        Task<TblCustomer> GetCustomerbyID(int id);
        Task<TblKycitem> LoadKycItem(int id);
        Task<TblCustomeraddress> LoadcustomerAddress(int id);
        Task<TblCustomerphonecontact> LoadcustomerPhone(int id);
        Task<TblCustomeremailcontact> LoadcustomerEmail(int id);
        Task<int> GenerateCustomerCode(long customerId);
        Task<TblCasa> LoadAccountById(int id);

        Task<List<TblCasa>> LoadDomantAccount();

        Task<string> GetProductCasaAccountId(string accountName);

        Task<List<TblAccountfreeze>> LoadFrozenAccounts();

        Task<List<TblAccountcardtype>> LoadAccountCardTypes();

        Task<TblAccountfreeze> ConfirmFreezeStatus(string id);

        Task<TblAccountfreeze> ConfirmFreezeReversalStatus(string id);

        Task<List<TblAccountclosure>> ListClosedAndApprovedAccounts();

        Task<List<GlobalbalanceVM>> GlobalBalance(string customercode);

        Task<List<TblFreezetransactionlist>> Loadfreezetransaction();
        Task<bool> AddCustomer( CustomerViewModel customer);
        Task<bool> UpdateCustomer(CustomerViewModel customer);
    }
        
}
