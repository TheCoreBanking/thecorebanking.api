﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheCoreBankingWebAPI.Data.Models;
using TheCoreBankingWebAPI.Data.ViewModels;

namespace TheCoreBankingWebAPI.Data.Contracts
{
    public interface IProductRepository
    {
        Task<List<TblCustomer>> GetAllCustomers();
     

        Task<List<TblProduct>> LoadProduct();

        Task<bool> CreateProduct( AddProductVm model);

        Task<bool> CreateProductGroup(AddProductGroupVm model);
        Task<bool> UpdateProductGroup(UpdateProductGroupVm model, int id );

        Task<bool> CreateProductCatergory(AddProductCatergoryVm model);
        Task<bool> UpdateProductCatergory(UpdateProductCatergoryVm model, int id);

        Task<List<CasaProductVm>> ListCasaProduct();
        Task<List<FeeVm>> GetFeeNameByProductId(string ProductID);

        Task<List<FeeVm>> GetFeeNameByProductName(string ProductName);
    }
        
}
