﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheCoreBankingWebAPI.Data.Models;
using TheCoreBankingWebAPI.Data.ViewModels;

namespace TheCoreBankingWebAPI.Data.Contracts
{
    public interface ICreditRepository
    {
        Task<List<TblCustomer>> GetAllCustomers();


       Task<List<TblBankingOperationSetup>> ListOperation();


       Task<List<TblProduct>> CreditProduct();


        Task<List<TblBankingProductType>> LoanProductType();


         Task<List<TblBankingProductSecurity>> ProductSecurity();


        Task<List<TblProductCategory>> LoanProductCatergory();

       Task<List<TblBankingLoanApplication>> ApprovedLoanApplication();

         Task<TblCasa> ProductAccountNumber(string ProductCode, int CustomerId);

        Task<int> GenerateLoanAppSchedule(LoanApplicationVM loanApplication);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="loanapplication"></param>
        /// <returns></returns>
        Task<bool> AddLoanApplication(TblBankingLoanApplication loanapplication);

        Task<bool> UpdateLoanApplication(int Id, TblBankingLoanApplication loanapplication);
        Task<bool> RemoveLoanApplication(int Id);
        Task<TblBankingLoanApplication> FindLoanApplication(int Id);
        Task<bool> AddLoanProduct(LoanProductVM loanproduct);
        Task<bool> AddLoanBooking(TblBankingLoanLease model);

        Task<bool> UpdateLoanBooking(int Id, TblBankingLoanLease loan);
        Task<bool> RemoveLoanBooking(int Id);
        Task<List<TblBankingTempSchedule>> ListGeneratedSchedule(string ProductAcctNo);
        Task<bool> AddLoanRestructure(TblBankingRestructure loanrestructure);
        Task<bool> AddPrincipalReduction(TblBankingPrincipalReduction model);
        Task<bool> AddPrincipalAddition(TblBankingPrincipalAddition model);
        Task<bool> AddInterestSuspension(TblBankingInterestSuspension model );
        Task<bool> AddLoanCancellation(TblBankingLoanCancelation model );
        Task<bool> AddLoanTermination(TblBankingTermination model);

    }

}
