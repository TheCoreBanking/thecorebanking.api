﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheCoreBankingWebAPI.Data.Models;
using TheCoreBankingWebAPI.Data.ViewModels;

namespace TheCoreBankingWebAPI.Data.Contracts
{
    public interface ISetupRepository
    {
        Task<TblCompanyInformation> GetcompanyById(int Id);
        Task<TblUnit> GetUnitById(int Id);
        Task<TblBranchInformation> GetBranchId(int Id);
        Task<TblMisinformation> GetMisById(int Id);
        Task<TblDirectorInformation> GetDirectorByID(int Id);
        Task<List<TblCustomer>> GetAllCustomers();
        Task<List<TblCompanyInformation>> Listcompanys();
        Task<List<TblBranchInformation>> ListBranch();
        Task<bool> DeleteNaration(int Id);
        Task<bool> VerifycompanyCode(string CompanyCode);
        Task<bool> VerifyBranchCode( string BranchID);
        Task<List<TblDirectorInformation>> ListDirector();
        Task<List<TblDepartment>> ListDepartment(); 
        Task<List<TblUnit>> ListUnit();
        Task<List<GeneralSetupTblOperationComment>> ListNaration();

        Task<bool> AddDirector(AddDirectorVm model);
        Task<bool> UpdateDirector(int Id, UpdateDirectorVm model);

        Task<bool> Addcompany( AddCompanyVm model);
        Task<bool> Updatecompany( int Id, UpdateCompanyVm model);

        Task<TblDirectorInformation> GetDirectorByBvn(string Bvn);

        Task<bool> AddNaration(NarationVm model);
        Task<bool> AddMis(AddMisVm model);
        Task<bool> UpdateMis(int Id, UpdateMisVm model);

        Task<bool> AddBranch(AddbranchVm model);
        Task<bool> UpdateBranch(int Id, UpdatebranchVm model);
        Task<bool> UpdateUnit(int Id, UpdateUnitVm model);
        Task<bool> AddUnit(AddUnitVm model);
        Task<bool> AddDepartment(DepartmentVm model);
        Task<bool> AddDesignation(DesignationVm model);
    }
        
}
