﻿using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBankingWebAPI.Data.Contracts;
using TheCoreBankingWebAPI.Data.Helpers;
using TheCoreBankingWebAPI.Data.Models;
using TheCoreBankingWebAPI.Data.Repository;
namespace TheCoreBankingWebAPI.Data
{
    public interface IUnitOfWork
    {
        void Commit();
        IAdminRepository Admin { get; }
        ICustomerRepository Customer { get; }
        ICreditRepository Credit { get; }
        IRetailRepository Retail { get; }

        IFinanceRepository Finance { get; }
        ITreasuryRepository Treasury { get; }
        ISetupRepository Setup { get; }
       IProductRepository Product { get; }
        IApprovalRepository Approval { get; }
        

    }
}
