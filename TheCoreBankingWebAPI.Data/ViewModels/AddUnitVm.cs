﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
    public class AddUnitVm
    {
        [Required]
        public string UnitName { get; set; }
        [Required]
        public string UnitCode { get; set; }
        [Required]
        public DateTime? DateCreated { get; set; }
        [Required]
        public string Remark { get; set; }
        [Required]
        public string CreatedBy { get; set; }
        [Required]
        public string BrCode { get; set; }
        [Required]
        public string CoyCode { get; set; }
        [Required]
        public bool? IsDeleted { get; set; }
    }
}
