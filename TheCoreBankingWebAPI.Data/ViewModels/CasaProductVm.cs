﻿using System;
using System.Collections.Generic;
using System.Text;
using TheCoreBankingWebAPI.Data.Models;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
   public class CasaProductVm
    {
      
    public TblProduct Product { get; set; }
    public   TblBankingProductType ProductType { get; set; }

    }
}
