﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
    public class TransactionsModel
    {
        public string transactionType { get; set; }
        public string narration { get; set; }
        public string refNumber { get; set; }
        public decimal amount { get; set; }
        public string glAccount { get; set; } 
        public string postedBy { get; set; }
        public string approvedBy { get; set; }
        public string sourceBranch { get; set; }
        public string misCode { get; set; }
        public string customerAccount { get; set; }
        public string companyCode { get; set; }
        public string customerCode { get; set; }
        public string prroductCode { get; set; } 
        public string postingType { get; set; }
        public string postLegType { get; set; }
        public string valueDate { get; set; }
        public string productCode { get; set; }
        public bool isCustomer { get; set; }
    }
}
