﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
  public   class UpdateProductGroupVm
    {
        public long Id { get; set; }
        public string Productgroupid { get; set; }
        public string Productgroupcode { get; set; }
        public string Productgroupname { get; set; }
        public int? Createdby { get; set; }
        public DateTime? Datetimecreated { get; set; }
        public bool? Deleted { get; set; }
        public DateTime? Datetimedeleted { get; set; }
    }
}
