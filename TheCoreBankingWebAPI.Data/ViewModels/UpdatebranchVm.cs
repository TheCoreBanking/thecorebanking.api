﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
    public class UpdatebranchVm
    {
        public string BrId { get; set; }
        public string CoyId { get; set; }
        public string BrAddress { get; set; }
        public string BrLocation { get; set; }
        public string BrState { get; set; }
        public string BrManager { get; set; }
        public string BrName { get; set; }
        public bool Deleted { get; set; }
    }
}
