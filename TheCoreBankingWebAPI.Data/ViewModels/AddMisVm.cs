﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
    public  class AddMisVm
    {
        [Required]
        public string MisCode { get; set; }
        [Required]
        public string MisName { get; set; }
        [Required]
        public string MisTypeId { get; set; }
        [Required]
        public string ParentMisCode { get; set; }
        [Required]
        public string CompanyCode { get; set; }
        [Required]
        public bool? Deleted { get; set; }
        [Required]
        public DateTime? DateCreated { get; set; }
    }
}
