﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
    public class CustomerViewModel
    {

        [Required]

        public int CustomerId { get; set; }
        [Required]
        public string customerCode { get; set; }
        [Required]
        public int customerType { get; set; }
        [Required]
        public string surname { get; set; }

        public string othername { get; set; }
        [Required]
        public string firstname { get; set; }
        public int title { get; set; }
        [Required]
        public string dateOfBirth { get; set; }
        public string placeOfBirth { get; set; }
        public string CMStatus { get; set; }
        [Required]
        public int gender { get; set; }
        [Required]
        public string mobilePhone { get; set; }
        [Required]
        public int modeOfID { get; set; }
        [Required]
        public string email { get; set; }
        [Required]
        public string nationality { get; set; }
        [Required]
        public string residence { get; set; }
        [Required]
        public string occupation { get; set; }
        [Required]
        public string branchcode { get; set; }
        [Required]
        public int BranchId { get; set; }
        [Required]
        public string company { get; set; }
        public string motherName { get; set; }
        public int maritalStatus { get; set; }
        public int sector { get; set; }
        [Required]
        public int customerCategory { get; set; }
    }
}
