﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
    public class TransactionViewModel
    {
        
        public string transactionType { get; set; }

        public string crNarration { get; set; }

        public string drNarration { get; set; }

        public string refNumber { get; set; }

        public decimal amount { get; set; }

        public string drGLAccount { get; set; } //AccountId

        public string crGLAccount { get; set; }//AccountId

        public string postedBy { get; set; }

        public string approvedBy { get; set; }

        public string sourceBranch { get; set; }

        public string destinationBranch { get; set; }

        public string misCode { get; set; }

        public string crCustomerAccount { get; set; } ///legType
        public string drCustomerAccount { get; set; } ///legType

        public string companyCode { get; set; }

        public string chequeNumber { get; set; }

        public string crCustomerCode { get; set; } //customerCode
        public string drCustomerCode { get; set; } //customerCode

        public string crProductCode { get; set; } //productCode
        public string drProductCode { get; set; } //productCode

        public string postingType { get; set; }
        public string valueDate { get; set; }
    }
}
