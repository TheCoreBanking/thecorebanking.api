﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
  public   class AddProductGroupVm
    {
       [Required]
        public string Productgroupid { get; set; }
        [Required]
        public string Productgroupcode { get; set; }
        [Required]
        public string Productgroupname { get; set; }
        [Required]
        public int? Createdby { get; set; }
        [Required]
        public DateTime? Datetimecreated { get; set; }
        [Required]
        public bool? Deleted { get; set; }
        [Required]
        public DateTime? Datetimedeleted { get; set; }
    }
}
