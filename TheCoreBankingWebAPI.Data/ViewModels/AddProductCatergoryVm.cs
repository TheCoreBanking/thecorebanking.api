﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
  public   class AddProductCatergoryVm
    {

        [Required]
        public int? Productcategoryid { get; set; }
        [Required]
        public string Productcategoryname { get; set; }
    }
}
