﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
   public class UpdateDirectorVm
    {
       
        public string Bvn { get; set; }

        public decimal? PercentageShare { get; set; }

        public string Position { get; set; }

        public string CompanyId { get; set; }

        public string FullName { get; set; }
    }
}
