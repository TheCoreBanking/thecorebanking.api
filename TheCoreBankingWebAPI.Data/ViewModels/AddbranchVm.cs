﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
    public class AddbranchVm
    {

        [Required]
        public string BrId { get; set; }
        [Required]
        public string CoyId { get; set; }
        [Required]
        public string BrAddress { get; set; }
        [Required]
        public string BrLocation { get; set; }
        [Required]
        public string BrState { get; set; }
        [Required]
        public string BrManager { get; set; }
        [Required]
        public string BrName { get; set; }
        [Required]
        public bool Deleted { get; set; }
    }
}
