﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
   public class LoanApplicationVM
    {

        [Required]
        public string CustName { get; set; }
        [Required]
        public string CustCode { get; set; }
        [Required]
        public string ProductCode { get; set; }
        [Required]
        public string ProductName { get; set; }
        [Required]
        public string ProductAcctNo { get; set; }
        [Required]
        public string CurrentAcct { get; set; }
        [Required]
        public int? PdTypeId { get; set; }
        [Required]
        public string Ref { get; set; }
    }
}
