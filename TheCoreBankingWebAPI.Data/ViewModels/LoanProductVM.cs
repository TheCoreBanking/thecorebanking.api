﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
   public  class LoanProductVM
    {

        [Required]
        public string Productid { get; set; }
        [Required]
        public int? Companyid { get; set; }
        [Required]
        public int? Producttypeid { get; set; }
        [Required]
        public int? Productcategoryid { get; set; }
        [Required]
        public bool? Ismultiplecurency { get; set; }
        [Required]
        public short? Productclassid { get; set; }
        [Required]
        public string Productcode { get; set; }
        [Required]
        public string Productname { get; set; }
        [Required]
        public string Productdescription { get; set; }
        [Required]
        public int? Principalbalancegl { get; set; }
        [Required]
        public int? Interestincomeexpensegl { get; set; }
        [Required]
        public int? Interestreceivablepayablegl { get; set; }
        [Required]

        public int? Dormantgl { get; set; }
        [Required]
        public int? Premiumdiscountgl { get; set; }
        [Required]
        public short? Dealtypeid { get; set; }
        [Required]
        public short? Dealclassificationid { get; set; }
        [Required]
        public short? Daycountconventionid { get; set; }
        [Required]
        public short? Scheduletypeid { get; set; }
        [Required]
        public bool? Allowscheduletypeoverride { get; set; }
        [Required]
        public int? Maximumtenor { get; set; }
        [Required]
        public int? Minimumtenor { get; set; }
        [Required]
        public decimal? Maximumrate { get; set; }
        [Required]
        public decimal? Minimumrate { get; set; }
        [Required]
        public decimal? Minimumbalance { get; set; }
        [Required]
        public short? Productpriceindexid { get; set; }
        [Required]
        public double? Productpriceindexspread { get; set; }
        [Required]
        public short? ProductBehaviourid { get; set; }
        [Required]
        public bool? Allowoverdrawn { get; set; }
        [Required]
        public int? Overdrawngl { get; set; }
        [Required]
        public bool? Allowrate { get; set; }
        [Required]
        public bool? Allowtenor { get; set; }
        [Required]
        public bool? Allowmoratorium { get; set; }
        [Required]
        public bool? Allowcustomeraccountforcedebit { get; set; }
        [Required]
        public int? Defaultgraceperiod { get; set; }
        [Required]
        public int? Cleanupperiod { get; set; }
        [Required]
        public int? Expiryperiod { get; set; }
        [Required]
        public double? Equitycontribution { get; set; }
        [Required]
        public int? Maximumdrawdownduration { get; set; }
        [Required]
        public int? Approvedby { get; set; }
        [Required]
        public bool? Completed { get; set; }
        [Required]
        public bool? Approved { get; set; }
        [Required]
        public int? Createdby { get; set; }

        public int? Lastupdatedby { get; set; }
        [Required]
        public DateTime? Datetimecreated { get; set; }
    
        public DateTime? Datetimeupdated { get; set; }
        [Required]
        public bool? Deleted { get; set; }
   
        public int? Deletedby { get; set; }
     
        public DateTime? Datetimedeleted { get; set; }
        [Required]
        public int? Productgroupid { get; set; }
        [Required]
        public bool? InterestPayment { get; set; }
        [Required]
        public string AccountType { get; set; }
        [Required]
        public string SpreadRate { get; set; }
        [Required]
        public string PrimeLending { get; set; }
        [Required]
        public string Settlementdate { get; set; }
        [Required]
        public string Treasuryproducttypeid { get; set; }
      

    }
}
