﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
    public class UpdateUnitVm
    {

        public string UnitName { get; set; }
        public string UnitCode { get; set; }
        public DateTime? DateCreated { get; set; }
        public string Remark { get; set; }
        public string CreatedBy { get; set; }
        public string BrCode { get; set; }
        public string CoyCode { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
