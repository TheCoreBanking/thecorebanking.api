﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
   public class DesignationVm
    {
        [Required]
        public string Designation { get; set; }
        [Required]
        public string DesignationCode { get; set; }
    }
}
