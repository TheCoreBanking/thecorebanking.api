﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
    public class LoanApprovalModel
    {
        public int loanId { get; set; }

        public int approvalType { get; set; }

        public string approvalComment { get; set; }

        public string approvedBy { get; set; }
    }
}
