﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
 public   class AddDirectorVm
    {
        [Required]
        public string Bvn { get; set; }
        [Required]
        public decimal? PercentageShare { get; set; }
        [Required]
        public string Position { get; set; }
        [Required]
        public string CompanyId { get; set; }
        [Required]
        public string FullName { get; set; }
    }
}
