﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TheCoreBankingWebAPI.Data.Models;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
   public class DepartmentVm
    {
        [Required]
        public int? Companyid { get; set; }
        [Required]
        public int? Branchid { get; set; }
        [Required]
        public string Department { get; set; }
        [Required]
        public string Departmentcode { get; set; }
        [Required]
        public string Unitcode { get; set; }
        [Required]
        public string Unitname { get; set; }
        [Required]

        public virtual TblBranch Branch { get; set; }
    }
}
