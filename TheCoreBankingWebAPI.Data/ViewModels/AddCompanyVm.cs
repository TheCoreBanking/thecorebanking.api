﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
   public  class AddCompanyVm
    {
       [Required]
        public string CoyId { get; set; }
        [Required]
        public string CoyName { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Telephone { get; set; }
        [Required]
        public string Fax { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public DateTime? DateOfIncorporation { get; set; }
        [Required]
        public string Manager { get; set; }
        [Required]
        public string NatureOfBusiness { get; set; }
        [Required]
        public string NameOfScheme { get; set; }
        [Required]
        public string FunctionsRegistered { get; set; }
        [Required]
        public decimal? AuthorisedShareCapital { get; set; }
        [Required]
        public string NameOfRegistrar { get; set; }
        [Required]
        public string NameOfTrustees { get; set; }
        [Required]
        public string FormerManagersTrustees { get; set; }
        [Required]
        public DateTime? DateOfRenewalOfRegistration { get; set; }
        [Required]
        public DateTime? DateOfCommencement { get; set; }
        [Required]
        public int? InitialFloatation { get; set; }
        [Required]
        public int? InitialSubscription { get; set; }
        [Required]
        public string CoyRegisteredBy { get; set; }
        [Required]
        public string TrusteesAddress { get; set; }
        [Required]
        public string InvestmentObjective { get; set; }
        [Required]
        public string CompanyClass { get; set; }
        [Required]
        public string CompanyType { get; set; }
        [Required]
        public int? AccountingStandard { get; set; }
        [Required]
        public int? MgtType { get; set; }
        [Required]
        public string Webbsite { get; set; }
        [Required]
        public string CoyClass { get; set; }
        [Required]
        public string AccountStand { get; set; }
        [Required]
        public string ManagementType { get; set; }
        [Required]
        public string EoyprofitAndLossGl { get; set; }
        [Required]
        public bool? Approved { get; set; }
        [Required]
        public bool? Disapproved { get; set; }
        [Required]
        public string Comment { get; set; }
        [Required]
        public string CoyCode { get; set; }
        [Required]
        public bool? Deleted { get; set; }
    }
}
