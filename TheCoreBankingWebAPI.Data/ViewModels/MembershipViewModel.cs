﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
    public class MembershipViewModel
    {
        public string userId { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public string passwordSalt { get; set; }
    }
}
