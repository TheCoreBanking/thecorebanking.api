﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TheCoreBankingWebAPI.Data.ViewModels
{
   public class NarationVm
    {
        [Required]
        public string Comment { get; set; }
        [Required]
        public string CreatedBy { get; set; }
        [Required]
        public string BrCode { get; set; }
        [Required]
        public string CoyCode { get; set; }
    }
}
